There are 3 databases in use of which one is maintained and the others are synchronized automatically via pg_dumpall:
1. stdw => used by lexicographers internally (dws, fkw)
2. stdw-ws => publicly available, used by wurdian app, gridline spelling
3. foarkarswurdlist-ejb-ws in languageapi => used by frysker.nl, several apps

##Updating applications:

1. stdw: deploy new war files, see [README](docker/README.md)
2. stdw-ws: deploy new war file, see [README](docker-ws/README.md)
3. foarkarswurdlist-ejb-ws: change dependency in language api project, build and deploy new war file, see [languageapi](../../languageapi/docker/README.md)

##Updating databases

###First the master database...

- stop application: `docker service rm stdw_stdw`
- backup db: run [cronreportsstdw](docker/cronreportsstdw)
- stop db: `docker stack rm stdw`
- move old db: `mv ${VOLUMEROOT}/stdw/pgdata/data xxx`
- create empty db dir `mkdir ${VOLUMEROOT}/stdw/pgdata/data`
- `chown postgres ${VOLUMEROOT}/stdw/pgdata/data`
- cd into stack directory
- adapt database version docker-compose.yml
- start db: `docker stack deploy --compose-file docker-compose.yml stdw`
- copy latest backup into container: `docker cp ${VOLUMEROOT}/stdw/reports/backupxxx.sql <containerid>:/tmp`
- import db:
  - `docker exec -it -upostgres <containerid> bash`
  - `psql < /tmp/backupxxx.sql`
  - `psql standertwurdlist`
    - `set search_path to standertwurdlist`
    - if needed set password for connection user
    - if needed run ddl to update database
- over time remove moved data directory
- backup db again: run [cronreportsstdw](docker/cronreportsstdw)

###...then the other two databases

(see docker-compose.yml for naming)
- stop db: `docker stack rm xxxx`
- remove old db: `rm -rf ${VOLUMEROOT}/xxx/pgdata/data/`
- create empty db dir `mkdir ${VOLUMEROOT}/xxx/pgdata/data`
- `chown postgres ${VOLUMEROOT}/xxx/pgdata/data`
- cd into stack directory
- adapt database version docker-compose.yml
- start db: `docker stack deploy --compose-file docker-compose.yml xxx`
- copy latest backup into container: `docker cp ${VOLUMEROOT}/${APPNAME}/reports/backupxxx.sql <containerid>:/tmp`
- import db:
    - `docker exec -it -upostgres <containerid> bash`
    - `psql < /tmp/backupxxx.sql`