-- GENERATED (xsltproc OddToSql.xsl ../customization/v2.0/corpora_linguistics.odd > linguistics.sql), ADAPT AS NEEDED

create table linguistic_category (id INT(10) NOT NULL auto_increment, version INT(10) NOT NULL, category varchar(16) not null, description varchar(256) not null,
                                  primary key(id),
                                  unique index category_unique (category));
create table linguistic_value (id INT(10) NOT NULL auto_increment, version INT(10) NOT NULL, category_id INT(10) NOT NULL, value varchar(16) not null, description varchar(256) not null,
                               primary key(id),
                               unique index value_category_unique (category_id, value),
                               constraint group_link foreign key (category_id) references linguistic_category (id));
insert into linguistic_category (id, version, category, description) values(0,0, 'islemma', 'https://universaldependencies.org/u/overview/morphology.html Boolean, is this a base form');
insert into linguistic_value (id, version, category_id, value, description) values(0,0, 0, 'yes', '');
insert into linguistic_category (id, version, category, description) values(1,0, 'abbr', 'http://universaldependencies.org/u/feat/Abbr.html Boolean feature. Is this an abbreviation?');
insert into linguistic_value (id, version, category_id, value, description) values(1,0, 1, 'yes', '');
insert into linguistic_category (id, version, category, description) values(2,0, 'poss', 'http://universaldependencies.org/u/feat/Poss.html Boolean feature. Is this word possessive?');
insert into linguistic_value (id, version, category_id, value, description) values(2,0, 2, 'yes', '');
insert into linguistic_category (id, version, category, description) values(3,0, 'reflex', 'http://universaldependencies.org/u/feat/Reflex.html Boolean feature, typically of pronouns or determiners. It tells whether the word is reflexive, i.e. refers to the subject of its clause.?');
insert into linguistic_value (id, version, category_id, value, description) values(3,0, 3, 'yes', '');
insert into linguistic_category (id, version, category, description) values(4,0, 'prefix', 'https://universaldependencies.org/u/feat/Prefix.html Boolean feature, Is this a prefix word in a compound, that usually cannot stand on its own?');
insert into linguistic_value (id, version, category_id, value, description) values(4,0, 4, 'yes', '');
insert into linguistic_category (id, version, category, description) values(5,0, 'suffix', 'not in universaldependencies Boolean feature, Is this a suffix word in a compound, that usually cannot stand on its own?');
insert into linguistic_value (id, version, category_id, value, description) values(5,0, 5, 'yes', '');
insert into linguistic_category (id, version, category, description) values(6,0, 'prontype', 'http://universaldependencies.org/u/feat/PronType.html This feature typically applies to pronouns, pronominal adjectives (determiners), pronominal numerals (quantifiers) and pronominal adverbs.');
insert into linguistic_value (id, version, category_id, value, description) values(6,0, 6, 'prs', 'personal pronoun or determiner');
insert into linguistic_value (id, version, category_id, value, description) values(7,0, 6, 'rcp', 'reciprocal pronoun');
insert into linguistic_value (id, version, category_id, value, description) values(8,0, 6, 'art', 'Article is a special case of determiner that bears the feature of definiteness');
insert into linguistic_value (id, version, category_id, value, description) values(9,0, 6, 'int', 'interrogative pronoun, determiner, numeral or adverb');
insert into linguistic_value (id, version, category_id, value, description) values(10,0, 6, 'rel', 'relative pronoun, determiner, numeral or adverb');
insert into linguistic_value (id, version, category_id, value, description) values(11,0, 6, 'ind', 'indefinite pronoun, determiner, numeral or adverb');
insert into linguistic_value (id, version, category_id, value, description) values(12,0, 6, 'emp', 'Emphatic pro-adjectives (determiners) emphasize the nominal they depend on.');
insert into linguistic_value (id, version, category_id, value, description) values(13,0, 6, 'exc', 'exclamative determiner');
insert into linguistic_value (id, version, category_id, value, description) values(14,0, 6, 'dem', 'Demonstrative pronouns are often parallel to interrogatives.');
insert into linguistic_category (id, version, category, description) values(7,0, 'case', 'http://universaldependencies.org/u/feat/Case.html Case is usually an inflectional feature of nouns.');
insert into linguistic_value (id, version, category_id, value, description) values(15,0, 7, 'nom', 'nominative');
insert into linguistic_value (id, version, category_id, value, description) values(16,0, 7, 'acc', 'accusative');
insert into linguistic_value (id, version, category_id, value, description) values(17,0, 7, 'dat', 'dative');
insert into linguistic_value (id, version, category_id, value, description) values(18,0, 7, 'gen', 'genitive');
insert into linguistic_value (id, version, category_id, value, description) values(19,0, 7, 'ins', 'instrumental / instructive');
insert into linguistic_value (id, version, category_id, value, description) values(20,0, 7, 'par', 'partitive');
insert into linguistic_category (id, version, category, description) values(8,0, 'tense', 'http://universaldependencies.org/u/feat/Tense.html Tense is typically a feature of verbs.');
insert into linguistic_value (id, version, category_id, value, description) values(21,0, 8, 'past', 'past tense');
insert into linguistic_value (id, version, category_id, value, description) values(22,0, 8, 'pres', 'present tense');
insert into linguistic_value (id, version, category_id, value, description) values(23,0, 8, 'fut', 'future tense');
insert into linguistic_category (id, version, category, description) values(9,0, 'voice', 'http://universaldependencies.org/u/feat/Voice.html Voice is typically a feature of verbs.');
insert into linguistic_value (id, version, category_id, value, description) values(24,0, 9, 'act', 'The subject of the verb is the doer of the action (agent).');
insert into linguistic_value (id, version, category_id, value, description) values(25,0, 9, 'pass', 'The subject of the verb is affected by the action (patient).');
insert into linguistic_category (id, version, category, description) values(10,0, 'number', 'http://universaldependencies.org/u/feat/Number.html Number is usually an inflectional feature of nouns.');
insert into linguistic_value (id, version, category_id, value, description) values(26,0, 10, 'sing', 'A singular noun denotes one person, animal or thing.');
insert into linguistic_value (id, version, category_id, value, description) values(27,0, 10, 'plur', 'A plural noun denotes several persons, animals or things.');
insert into linguistic_value (id, version, category_id, value, description) values(28,0, 10, 'ptan', 'Plurale tantum, some nouns appear only in the plural form even though they denote one thing.');
insert into linguistic_value (id, version, category_id, value, description) values(29,0, 10, 'coll', 'Collective or mass or singulare tantum applies to words that use grammatical singular to describe sets of objects.');
insert into linguistic_category (id, version, category, description) values(11,0, 'person', 'http://universaldependencies.org/u/feat/Person.html Person is typically feature of personal and possessive pronouns / determiners, and of verbs.');
insert into linguistic_value (id, version, category_id, value, description) values(30,0, 11, 'first', 'The first person refers just to the speaker / author and in plural one or more additional persons.');
insert into linguistic_value (id, version, category_id, value, description) values(31,0, 11, 'second', 'The second person refers to the addressee(s).');
insert into linguistic_value (id, version, category_id, value, description) values(32,0, 11, 'third', 'The third person refers to one or more persons that are neither speakers nor addressees.');
insert into linguistic_category (id, version, category, description) values(12,0, 'verbtype', 'http://universaldependencies.org/u/feat/VerbType.html distinctions on top of verb and aux.');
insert into linguistic_value (id, version, category_id, value, description) values(33,0, 12, 'mod', 'Verbs that take infinitive of another verb as argument and add various modes of possibility, necessity etc.');
insert into linguistic_value (id, version, category_id, value, description) values(34,0, 12, 'tense', 'Verb used to create periphrastic verb forms (tenses, passives etc.).');
insert into linguistic_category (id, version, category, description) values(13,0, 'verbform', 'http://universaldependencies.org/u/feat/VerbForm.html form of verb or deverbative.');
insert into linguistic_value (id, version, category_id, value, description) values(35,0, 13, 'inf', 'Infinitive is the citation form of verbs in many languages.');
insert into linguistic_value (id, version, category_id, value, description) values(36,0, 13, 'part', 'Participle is a non-finite verb form that shares properties of verbs and adjectives.');
insert into linguistic_value (id, version, category_id, value, description) values(37,0, 13, 'ger', 'Gerund is a non-finite verb form that shares properties of verbs and nouns.');
insert into linguistic_value (id, version, category_id, value, description) values(38,0, 13, 'conv', 'The converb, also called adverbial participle or transgressive, is a non-finite verb form that shares properties of verbs and adverbs.');
insert into linguistic_category (id, version, category, description) values(14,0, 'polite', 'https://universaldependencies.org/u/feat/Polite.html Various languages have various means to express politeness or respect.');
insert into linguistic_value (id, version, category_id, value, description) values(39,0, 14, 'infm', 'usually meant for communication with family members and close friends.');
insert into linguistic_value (id, version, category_id, value, description) values(40,0, 14, 'form', 'usually meant for communication with strangers and people of higher social status.');
insert into linguistic_category (id, version, category, description) values(15,0, 'numtype', 'http://universaldependencies.org/u/feat/NumType.html numeral type.');
insert into linguistic_value (id, version, category_id, value, description) values(41,0, 15, 'ord', 'ordinal number (first, second,..)');
insert into linguistic_value (id, version, category_id, value, description) values(42,0, 15, 'card', 'cardinal number (one, two, many,....)');
insert into linguistic_category (id, version, category, description) values(16,0, 'degree', 'http://universaldependencies.org/u/feat/Degree.html Degree of comparison is typically an inflectional feature of some adjectives and adverbs.');
insert into linguistic_value (id, version, category_id, value, description) values(43,0, 16, 'cmp', 'comparative, second degree');
insert into linguistic_value (id, version, category_id, value, description) values(44,0, 16, 'sup', 'superlative, third degree');
insert into linguistic_category (id, version, category, description) values(17,0, 'mood', 'http://universaldependencies.org/u/feat/Mood.html Mood is a feature that expresses modality and subclassifies finite verb forms.');
insert into linguistic_value (id, version, category_id, value, description) values(45,0, 17, 'imp', 'The speaker uses imperative to order or ask the addressee to do the action of the verb.');
insert into linguistic_value (id, version, category_id, value, description) values(46,0, 17, 'sub', 'The subjunctive mood is used under certain circumstances in subordinate clauses, typically for actions that are subjective or otherwise uncertain.');
insert into linguistic_value (id, version, category_id, value, description) values(47,0, 17, 'ind', 'A verb in indicative merely states that something happens, has happened or will happen.');
insert into linguistic_category (id, version, category, description) values(18,0, 'gender', 'http://universaldependencies.org/u/feat/Gender.html gender.');
insert into linguistic_value (id, version, category_id, value, description) values(48,0, 18, 'masc', 'masculine gender');
insert into linguistic_value (id, version, category_id, value, description) values(49,0, 18, 'fem', 'feminine gender');
insert into linguistic_value (id, version, category_id, value, description) values(50,0, 18, 'neut', 'neuter gender');
insert into linguistic_value (id, version, category_id, value, description) values(51,0, 18, 'com', 'Some languages do not distinguish masculine/feminine but they do distinguish neuter vs. non-neuter. The non-neuter is called common gender.');
insert into linguistic_category (id, version, category, description) values(19,0, 'pronoun', 'Not in universaldependencies. pronoun drop or clitic');
insert into linguistic_value (id, version, category_id, value, description) values(52,0, 19, 'drop', 'Not in universaldependencies. pronoun drop, omission of pronouns because they can be infered');
insert into linguistic_value (id, version, category_id, value, description) values(53,0, 19, 'clitic', 'Not in universaldependencies. pronoun clitic, most personal pronouns have a clitic form, which is the result of either vowel deletion, vowel reduction, monophthongization or schwa deletion, while there are also cases of suppletion.');
insert into linguistic_category (id, version, category, description) values(20,0, 'diminutive', 'Not in universaldependencies. Diminutive.');
insert into linguistic_value (id, version, category_id, value, description) values(54,0, 20, 'dim', 'Not in universaldependencies. diminutive');
insert into linguistic_category (id, version, category, description) values(21,0, 'inflection', 'Not in universaldependencies. The modification of a word to express different grammatical categories such as tense, case, voice, aspect, person.');
insert into linguistic_value (id, version, category_id, value, description) values(55,0, 21, 'infl', 'Not in universaldependencies. inflected');
insert into linguistic_value (id, version, category_id, value, description) values(56,0, 21, 'uninf', 'Not in universaldependencies. uninflected');
insert into linguistic_category (id, version, category, description) values(22,0, 'valency', 'Not in universaldependencies. Verb valency or valence is the number of arguments controlled by a verbal predicate.');
insert into linguistic_value (id, version, category_id, value, description) values(57,0, 22, 'mtran', 'Not in universaldependencies. a monotransitive verb takes two arguments (of which one object)');
insert into linguistic_value (id, version, category_id, value, description) values(58,0, 22, 'tran', 'Not in universaldependencies. a transitive verb requires one or more objects');
insert into linguistic_value (id, version, category_id, value, description) values(59,0, 22, 'intran', 'Not in universaldependencies. an intransitive verb takes one argument (no object)');
insert into linguistic_value (id, version, category_id, value, description) values(60,0, 22, 'ditran', 'Not in universaldependencies. a ditransitive verb takes three arguments (of which a direct and an indirect object)');
insert into linguistic_category (id, version, category, description) values(23,0, 'construction', 'Not in universaldependencies. Construction.');
insert into linguistic_value (id, version, category_id, value, description) values(61,0, 23, 'attr', 'Not in universaldependencies. attributive');
insert into linguistic_category (id, version, category, description) values(24,0, 'convertedfrom', 'Not in universaldependencies. words belonging to one part of speach category used as another category.');
insert into linguistic_value (id, version, category_id, value, description) values(62,0, 24, 'adj', 'Not in universaldependencies. adjective used as another category');
insert into linguistic_value (id, version, category_id, value, description) values(63,0, 24, 'adv', 'Not in universaldependencies. adverb used as another category');
insert into linguistic_value (id, version, category_id, value, description) values(64,0, 24, 'ver', 'Not in universaldependencies. verb used as another category');
insert into linguistic_value (id, version, category_id, value, description) values(65,0, 24, 'num', 'Not in universaldependencies. numeral used as another category');
insert into linguistic_value (id, version, category_id, value, description) values(66,0, 24, 'pro', 'Not in universaldependencies. pronomen used as another category');
insert into linguistic_value (id, version, category_id, value, description) values(67,0, 24, 'part', 'Not in universaldependencies. verbform part used as another category');
insert into linguistic_category (id, version, category, description) values(25,0, 'predicate', 'Not in universaldependencies. Predicate.');
insert into linguistic_value (id, version, category_id, value, description) values(68,0, 25, 'pred', 'Not in universaldependencies. statement about the subject');
insert into linguistic_category (id, version, category, description) values(26,0, 'pos', 'http://universaldependencies.org/u/pos/index.html These tags mark the core part-of-speech categories.');
insert into linguistic_value (id, version, category_id, value, description) values(69,0, 26, 'adj', 'Adjectives are words that typically modify nouns and specify their properties or attributes.');
insert into linguistic_value (id, version, category_id, value, description) values(70,0, 26, 'adp', 'Adposition is a cover term for prepositions and postpositions.');
insert into linguistic_value (id, version, category_id, value, description) values(71,0, 26, 'adv', 'Adverbs are words that typically modify verbs for such categories as time, place, direction or manner.');
insert into linguistic_value (id, version, category_id, value, description) values(72,0, 26, 'aux', 'An auxiliary is a function word that accompanies the lexical verb of a verb phrase and expresses grammatical distinctions not carried by the lexical verb, such as person, number, tense, mood, aspect, voice or evidentiality.');
insert into linguistic_value (id, version, category_id, value, description) values(73,0, 26, 'cconj', 'A coordinating conjunction is a word that links words or larger constituents without syntactically subordinating one to the other and expresses a semantic relationship between them.');
insert into linguistic_value (id, version, category_id, value, description) values(74,0, 26, 'det', 'Determiners are words that modify nouns or noun phrases and express the reference of the noun phrase in context.');
insert into linguistic_value (id, version, category_id, value, description) values(75,0, 26, 'intj', 'An interjection is a word that is used most often as an exclamation or part of an exclamation.');
insert into linguistic_value (id, version, category_id, value, description) values(76,0, 26, 'noun', 'Nouns are a part of speech typically denoting a person, place, thing, animal or idea.');
insert into linguistic_value (id, version, category_id, value, description) values(77,0, 26, 'num', 'A numeral is a word, functioning most typically as a determiner, adjective or pronoun, that expresses a number and a relation to the number, such as quantity, sequence, frequency or fraction.');
insert into linguistic_value (id, version, category_id, value, description) values(78,0, 26, 'part', 'Particles are function words that must be associated with another word or phrase to impart meaning and that do not satisfy definitions of other universal parts of speech.');
insert into linguistic_value (id, version, category_id, value, description) values(79,0, 26, 'pron', 'Pronouns are words that substitute for nouns or noun phrases, whose meaning is recoverable from the linguistic or extralinguistic context.');
insert into linguistic_value (id, version, category_id, value, description) values(80,0, 26, 'propn', 'A proper noun is a noun (or nominal content word) that is the name (or part of the name) of a specific individual, place, or object.');
insert into linguistic_value (id, version, category_id, value, description) values(81,0, 26, 'punct', 'Punctuation marks are non-alphabetical characters and character groups used in many languages to delimit linguistic units in printed text.');
insert into linguistic_value (id, version, category_id, value, description) values(82,0, 26, 'sconj', 'A subordinating conjunction is a conjunction that links constructions by making one of them a constituent of the other.');
insert into linguistic_value (id, version, category_id, value, description) values(83,0, 26, 'sym', 'A symbol is a word-like entity that differs from ordinary words by form, function, or both.');
insert into linguistic_value (id, version, category_id, value, description) values(84,0, 26, 'verb', 'A verb is a member of the syntactic class of words that typically signal events and actions.');
insert into linguistic_value (id, version, category_id, value, description) values(85,0, 26, 'x', 'The tag X is used for words that for some reason cannot be assigned a real part-of-speech category.');