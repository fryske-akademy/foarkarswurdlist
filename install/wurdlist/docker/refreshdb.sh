#!/bin/sh
# refresh database with new data
d=`date +%w`
n=`expr $d - 1`
[ $n -eq -1 ] && n=6
echo "drop schema if exists standertwurdlist cascade"|psql standertwurdlist
psql < /reports/backup-${n}.sql
