# docker setup for foarkarswurdlist

## in the build directory

### create file 'changepwd' to change master and admin password:
```
AS_ADMIN_PASSWORD=admin
AS_ADMIN_NEWPASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=changeit
AS_ADMIN_NEWMASTERPASSWORD=xxxxxxx
```
### create file 'pwdprd' to use new passwords and for dbcon:
```
AS_ADMIN_PASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=xxxxxxx
AS_ADMIN_ALIASPASSWORD=xxxxxxx
```
### create file 'ldap' to use for ldap connection:
```
AS_ADMIN_PASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=xxxxxxx
AS_ADMIN_ALIASPASSWORD=xxxxxxx
```
### DON'T COMMIT THE PASSWORD FILES AND REMOVE AFTER USE!!!

### copy "postgres jdbc jar" to this directory

### copy wars and prepare deploy.txt in wars directory

### environment
```bash
export APPNAME=wurdlist
export VOLUMEROOT=${HOME}/volumes
export VERSION=xxx
```
### build image:
```
export DOCKER_BUILDKIT=1
export PGJDBC=postgresql-42.2.5.jar
docker build --secret id=changepwd,src=changepwd --secret id=pwdprd,src=pwdprd --secret id=payarainit,src=payarainit -t ${APPNAME}:${VERSION} .
rm changepwd pwdprd
```

## optionally push to registry
```
docker tag ${APPNAME}:${VERSION} localhost:5000/${APPNAME}:${VERSION}
docker run -d -p 5000:5000 --restart=always --name registry registry:2
docker push localhost:5000/${APPNAME}
```

## in the stack directory

### prepare and run stack
**NOTE**: create the secrets in /bin/sh, without history  
**NOTE**: look carefully at volumes you may not want to overwrite
```
sh
docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
echo "AS_ADMIN_MASTERPASSWORD=xxxxxxx"|docker secret create master -
echo "xxxxxxxxx"|docker secret create postgres-passwd -
```
Prepare the volumes in ${VOLUMEROOT}/${APPNAME}, see comment in compose file, **PERMISSIONS!**
```
mkdir -p ${VOLUMEROOT}/${APPNAME}/pgdata/data
mkdir ${VOLUMEROOT}/${APPNAME}/refresh
sudo cp refreshdb.sh ${VOLUMEROOT}/${APPNAME}/refresh
sudo chown -R postgres ${VOLUMEROOT}/${APPNAME}/refresh
sudo chown -R postgres ${VOLUMEROOT}/${APPNAME}/pgdata
sudo chmod +x ${VOLUMEROOT}/${APPNAME}/refresh/refreshdb.sh
```
```
docker stack deploy -c docker-compose.yml ${APPNAME}
```
## crontab entry voor refresh / sync
```
sudo cp cronrefresh${APPNAME} /etc/cron.daily
sudo chmod +x /etc/cron.daily/cronrefresh${APPNAME}
```
### usefull commands

```
docker container|service|image|stack ls
docker service logs ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```

### after first stack deploy

- read and apply comment in docker-compose.yml
