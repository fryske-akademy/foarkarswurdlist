set search_path to standertwurdlist;

alter table standertwurdlist.Lemma add column standert tinyint(1) not null default 0;
alter table standertwurdlist.Lemma_AUD add column standert tinyint(1);
update standertwurdlist.Lemma set standert=1 where ynfhwbofjur=1 and stdlem_id is null;
alter table standertwurdlist.Lemma add column hatfariant tinyint(1) not null default 0;
alter table standertwurdlist.Lemma_AUD add column hatfariant tinyint(1);
update standertwurdlist.Lemma set hatfariant=voorkeur;
alter table standertwurdlist.Lemma drop column voorkeur;

ALTER TABLE Lemma_AUD MODIFY standert tinyint(1) NULL DEFAULT 0;
ALTER TABLE Lemma_AUD MODIFY hatfariant tinyint(1) NULL DEFAULT 0;

ALTER TABLE standertwurdlist.NotFound 
CHANGE COLUMN foarm foarm VARCHAR(255) CHARACTER set utf8 collate utf8_bin NOT NULL ;

alter table standertwurdlist.Lemma add column prefix VARCHAR(20) character set utf8 collate utf8_bin;
alter table standertwurdlist.Lemma_AUD add column prefix VARCHAR(20) character set utf8 collate utf8_bin;

update Lemma l set l.prefix = (
    select substring_index(foarm,' ',-1) as pref
    from Paradigma where lemma_id = l.id and foarm like '% %' group by pref having
    count(pref) = 1 or count(pref) = 2)
where l.soart = 'haadtw.' and l.prefix is null;

update Lemma l set l.prefix = (
    select substring_index(foarm,' ',-1) as pref
    from Paradigma where lemma_id = l.id and foarm like '% %' group by pref having
    count(pref) = 3 or count(pref) = 4 or
    count(pref) = 5 or count(pref) = 6 or
    count(pref) = 7 or count(pref) = 8 or
    count(pref) = 9 or count(pref) = 10 or
    count(pref) = 11)
where l.soart = 'haadtw.' and l.prefix is null;

delete from Paradigma_soarten where paradigma_id in
(select id from Paradigma where foarm like '% %' and lemma_id in
(select id from Lemma where soart= 'haadtw.'));

delete from Paradigma where foarm like '% %' and lemma_id in
(select id from Lemma where soart= 'haadtw.');

alter table Paradigma add column linguistics VARCHAR(512) character set utf8 collate utf8_bin;
alter table Paradigma_AUD add column linguistics VARCHAR(512) character set utf8 collate utf8_bin;

update Paradigma p set linguistics =
  concat('[',
    (
        select group_concat(c.universalcode) from code_convert c
        where c.facode in (
            select soartlabel from Soartlist where id in (select soartlist_id from Paradigma_soarten where paradigma_id=p.id))
    )
  ,']');

update Paradigma set linguistics = '[]' where linguistics is null;

ALTER TABLE Lemma
ADD INDEX neisjoen (neisjoen ASC);

update code_convert set universalcode = 'Pos.DET PronType.Art' where universalcode='PronType.Art';

alter table Lemma add column pos VARCHAR(10) character set utf8 collate utf8_bin not null;
alter table Lemma_AUD add column pos VARCHAR(10) character set utf8 collate utf8_bin;

update Lemma l set pos = (
select 
case
    when position(' ' in c.universalcode) = 0 then c.universalcode
    else substring(c.universalcode from 1 for position(' ' in c.universalcode) - 1)
end
from code_convert c
where c.facode=l.soart);

ALTER TABLE Lemma 
ADD INDEX soartind (soart),
ADD INDEX posind (pos ASC);

ALTER TABLE Lemma
ADD INDEX standert (standert ASC);

ALTER TABLE Paradigma
ADD INDEX std (std ASC);

create index linguistics on standertwurdlist.paradigma (linguistics);

create view SpellChecker as
select p.foarm, p.ofbrekking from Paradigma p
inner join Lemma L on p.lemma_id = L.id and L.neisjoen=true;

grant all on table spellchecker to stdw;


----------------------------
--createdb standertwurdlist
--pgloader mysql://user:pwd@194.171.192.173/standertwurdlist postgresql://user:pw@localhost/standertwurdlist
SET search_path TO standertwurdlist;

CREATE SEQUENCE standertwurdlist.hibernate_sequence;

ALTER SEQUENCE standertwurdlist.hibernate_sequence
    OWNER TO postgres;

GRANT ALL ON SEQUENCE standertwurdlist.hibernate_sequence TO postgres;

GRANT ALL ON SEQUENCE standertwurdlist.hibernate_sequence TO stdw;

select max(id) from standertwurdlist.revisioninfo ;
alter sequence standertwurdlist.hibernate_sequence restart with ...;

insert into standertwurdlist.Soartlist (soartlabel, version) values ('subj',0);
insert into standertwurdlist.Soartlist (soartlabel, version) values ('obj',0);
insert into standertwurdlist.Soartlist (soartlabel, version) values ('polite-nt',0);
insert into standertwurdlist.Soartlist (soartlabel, version) values ('polite-dt',0);

insert into standertwurdlist.paradigma_soarten (paradigma_id, soartlist_id)
select paradigma_id, (select id from
    standertwurdlist.soartlist where soartlabel='polite-nt')
from standertwurdlist.paradigma_soarten where soartlist_id in
(select min(id) from standertwurdlist.soartlist where soartlabel='2-mt-nt' or soartlabel='nulfoarm|=nf');

update Paradigma p set foarm=
                           concat(
                                   substr(p.foarm,length((select prefix from Lemma where id=p.lemma_id))+1),
                                   ' ',
                                   (select prefix from Lemma where id=p.lemma_id))
where p.linguistics like '%Clitic%' and p.foarm not like '% %'
  and p.lemma_id in
      (select id from Lemma where id=p.lemma_id and not (prefix is null or prefix = ''));

insert into standertwurdlist.paradigma_soarten (paradigma_id, soartlist_id)
select paradigma_id, (select id from
    standertwurdlist.soartlist where soartlabel='polite-dt')
from standertwurdlist.paradigma_soarten where soartlist_id in
                                              (select min(id) from standertwurdlist.soartlist where soartlabel='2-mt-dt');

-- verse data in code_convert and linguistic tables!!!

update Paradigma p set linguistics =
                           concat('[',
                                  (
                                      select string_agg(c.universalcode,',') from code_convert c
                                      where c.facode in (
                                          select soartlabel from Soartlist where id in (select soartlist_id from Paradigma_soarten where paradigma_id=p.id))
                                  )
                               ,']');

alter table Lemma add column aksept boolean not null default false;
alter table Lemma_AUD add column aksept boolean;
create index aksept on Lemma (aksept);


------------------------------8-7-2019

-- verse data in code_convert

drop view preferred;

create view preferred as
select l.foarm as nonstandard,
       case when l.aksept then 'true' else '' end as accepted,
       case when m.foarm is null then 'NULL' else m.foarm end as standard,
       p.foarm as form, p.linguistics from Lemma l
inner join Paradigma p on p.lemma_id = l.id
left outer join Lemma m on l.stdlem_id=m.id and not l.stdlem_id is null
where l.neisjoen=true and l.standert=false;

grant all on table preferred to stdw;

update Paradigma p set linguistics =
                           concat('[',
                                  (
                                      select string_agg(c.universalcode,',') from code_convert c
                                      where c.facode in (
                                          select soartlabel from Soartlist where id in
                                                 (select soartlist_id from Paradigma_soarten where paradigma_id=p.id))
                                      or c.facode in (select soart from lemma where p.lemma_id=id)
                                  )
                               ,']');

drop view forms;

create view forms as
select l.foarm as lemma, p.foarm as form, p.linguistics, p.std as preferred, p.pron
from Paradigma p
inner join Lemma l on p.lemma_id = l.id
where l.neisjoen=true;

grant all on table forms to stdw;

create view dutchism as
select l.foarm as lemma, d.foarm as dutchism, e.foarm as form, c.universalcode as linguistics, l.pos as pos
from Lemma l
         inner join hollemma d on d.lemma = l.id
         inner join holparadigma e on e.hollemma = d.id
         inner join soartlist s on e.soart = s.id
         inner join code_convert c on s.soartlabel=c.facode
where l.neisjoen=true;

grant all on table dutchism to stdw;

create table synoniem (
    id serial primary key,
    version bigint not null,
    lemma_id bigint not null references lemma(id),
    vorm varchar(64) not null,
    ok boolean,
    betekenis text,
    opmerking text
);
create index synvorm on synoniem(vorm);
grant all on table synoniem to stdw;

create table synoniem_AUD (
  id bigint not null,
  rev bigint not null,
  revtype smallint,
  lemma_id bigint,
  vorm varchar(64),
  ok boolean,
    betekenis text,
    opmerking text
);
grant all on table synoniem_AUD to stdw;

create view synonym as
select l.foarm as lemma, d.vorm as synonym, d.betekenis, c.universalcode as pos
from Lemma l
         inner join synoniem d on d.lemma_id = l.id
         inner join code_convert c on l.soart=c.facode
where l.neisjoen=true and d.ok=true;
grant all on table synonym to stdw;
grant all on sequence synoniem_id_seq to stdw;

alter table synoniem add column betekenis text;
alter table synoniem_AUD add column betekenis text;
update synoniem set betekenis = opmerking;
update synoniem set opmerking = null;

update lemma set aksept=true where standert=true and aksept != true;

insert into code_convert values ('it-subst.','Pos.NOUN');

create extension unaccent;

alter table lemma add column nodiacrits varchar(255) not null default '';
alter table lemma_AUD add column nodiacrits varchar(255);
alter table paradigma add column nodiacrits varchar(255) not null default '';
alter table paradigma_AUD add column nodiacrits varchar(255);
alter table hollemma add column nodiacrits varchar(255) not null default '';
alter table hollemma_AUD add column nodiacrits varchar(255);
alter table holparadigma add column nodiacrits varchar(255) not null default '';
alter table holparadigma_AUD add column nodiacrits varchar(255);
alter table synoniem add column nodiacrits varchar(255) not null default '';
alter table synoniem_AUD add column nodiacrits varchar(255);

create index lemma_nodia on lemma(nodiacrits);
create index paradigma_nodia on paradigma(nodiacrits);
create index hollemma_nodia on hollemma(nodiacrits);
create index holpar_nodia on holparadigma(nodiacrits);
create index syn_nodia on synoniem(nodiacrits);



update lemma set nodiacrits = lower(unaccent(foarm));
update paradigma set nodiacrits = lower(unaccent(foarm));
update hollemma set nodiacrits = lower(unaccent(foarm));
update holparadigma set nodiacrits = lower(unaccent(foarm));
update synoniem set nodiacrits = lower(unaccent(vorm));

alter table lemma add column article varchar(10);
alter table lemma_AUD add column article varchar(10);

update lemma set article = 'de' where soart like '%de%subst%';
update lemma set article = 'it' where soart like '%it%subst%';
update lemma set article = 'de&it' where soart like 'de&it%subst%' or soart like 'it&de%subst%';

insert into holparadigma (version, foarm, soart, hollemma,nodiacrits) select 0, d.foarm, (select id from soartlist where soartlabel='nulfoarm'), d.id, lower(unaccent(d.foarm)) from hollemma d where d.foarm not in (select foarm from holparadigma where foarm=d.foarm);

-- 10 maart 2022

drop view preferred;

create view nonpreferred as
select l.foarm as nonstandard,
       case when l.aksept then 'true' else '' end as accepted,
       case when m.foarm is null then 'NULL' else m.foarm end as standard,
       p.foarm as form, p.linguistics from Lemma l
                                               inner join Paradigma p on p.lemma_id = l.id
                                               left outer join Lemma m on l.stdlem_id=m.id and not l.stdlem_id is null
where l.neisjoen=true and l.standert=false;

grant all on table nonpreferred to stdw;

drop view forms;

create view forms as
select l.foarm as lemma, p.foarm as form, p.linguistics, l.standert as lemmaPreferred, p.std as formPreferred, p.pron, p.ofbrekking as hyph
from Paradigma p inner join Lemma l on p.lemma_id = l.id
where l.neisjoen=true;

grant all on table forms to stdw;

create table lemmapart (id  serial not null, version int not null, nodiacrits varchar(255) not null, part varchar(255) not null, pos varchar(20), lemma_id int not null, primary key (id));
create table lemmapart_AUD (id int4 not null, REV int not null, REVTYPE int, nodiacrits varchar(255), part varchar(255), pos varchar(20), lemma_id int, primary key (id, REV));
create index part_part on lemmapart(part);
create index part_nodiacrits on lemmapart(nodiacrits);
create index part_pos on lemmapart(pos);
grant all on table lemmapart to stdw;
grant all on table lemmapart_aud to stdw;
grant all on sequence lemmapart_id_seq to stdw;

alter table lemmapart add column base boolean default false;
alter table lemmapart_AUD add column base boolean;

create index base_index on lemmapart(base);

CREATE SEQUENCE standertwurdlist.revisioninfo_seq increment by 50;
grant all on sequence standertwurdlist.revisioninfo_seq TO stdw;

update paradigma set linguistics=regexp_replace(linguistics,'Person\.First','Person.1','g') where linguistics like '%Person.First%';
update paradigma set linguistics=regexp_replace(linguistics,'Person\.Second','Person.2','g') where linguistics like '%Person.Second%';
update paradigma set linguistics=regexp_replace(linguistics,'Person\.Third','Person.3','g') where linguistics like '%Person.Third%';
update paradigma set linguistics=regexp_replace(linguistics,'pronoun\.Clitic','Clitic.YES','g') where linguistics like '%pronoun.Clitic%';
update paradigma set linguistics=regexp_replace(linguistics,'pronoun\.Drop','Prodrop.YES','g') where linguistics like '%pronoun.Drop%';
update paradigma set linguistics=regexp_replace(linguistics,'Diminutive\.Dim','Degree.DIM','g') where linguistics like '%Diminutive.Dim%';
update paradigma set linguistics=regexp_replace(linguistics,'  ',' ','g') where linguistics like '%  %';
update paradigma set linguistics=regexp_replace(linguistics,'Predicate\.Pred','Predicate.Yes','g') where linguistics like '%Predicate.Pred%';

alter table paradigma add column pron varchar(64);
create index ipaidx on paradigma(pron);

alter table paradigma_aud add column pron varchar(64);

alter table paradigma alter column pron type varchar(80);
alter table paradigma_aud alter column pron type varchar(80);

alter table lemma add column publish boolean not null default false;
alter table lemma_AUD add column publish boolean;
update lemma set publish=standert;
