/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.enterprise.util.AnnotationLiteral;
import java.text.Normalizer;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;

import java.util.regex.Pattern;
import org.fa.foarkarswurdlist.ejb.IpaService;

/**
 *
 * @author eduard
 */
public abstract class CrudAware {
    public static final Pattern DEL_DIACRITS = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    
    public static String removeDiacrits(String s) {
        return DEL_DIACRITS.matcher(Normalizer.normalize(s,Normalizer.Form.NFKD)).replaceAll("");
    }

    protected IpaService getIpaService() {
        return CDI.current().select(IpaService.class,STDW_ANNOTATION).get();
    }

    static class StdwAnnotation extends AnnotationLiteral<StdwCrudBean> implements StdwCrudBean {}

    public static final StdwAnnotation STDW_ANNOTATION = new StdwAnnotation();
}
