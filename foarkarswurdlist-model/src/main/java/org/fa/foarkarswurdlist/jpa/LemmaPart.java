/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.fa.foarkarswurdlist.jpa.listeners.LemmaPartListener;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name = "lemmapart")
@Access(AccessType.FIELD)
@EntityListeners(LemmaPartListener.class)
public class LemmaPart extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String part;
    @Column(length = 20)
    @Enumerated(EnumType.STRING)
    private Pc.Pos pos;
    @NotNull
    @NotEmpty
    @Column(nullable = false)
    private String nodiacrits;
    @Column
    private Boolean base;
    @JoinColumn(name = "lemma_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Lemma lemma;

    public LemmaPart() {
    }
    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }
    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemmaId) {
        this.lemma = lemmaId;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public Pc.Pos getPos() {
        return pos;
    }

    public void setPos(Pc.Pos pos) {
        this.pos = pos;
    }

    public Boolean getBase() {
        return base;
    }

    public void setBase(Boolean base) {
        this.base = base;
    }

    @Override
    public String toString() {
        return "LemmaParts{" + "part=" + part + ", id=" + getId() + '}';
    }

    
}
