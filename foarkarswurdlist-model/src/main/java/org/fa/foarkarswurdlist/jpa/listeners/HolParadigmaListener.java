/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import org.fa.foarkarswurdlist.jpa.HolParadigma;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.Auditing;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.util.Optional;

/**
 *
 * @author eduard
 */
public class HolParadigmaListener extends CrudAware {

    @PreUpdate
    @PrePersist
    public void check(HolParadigma toStore) {
        toStore.setNodiacrits(removeDiacrits(toStore.getFoarm()));
        Auditing auditing = getIpaService();
        Optional<HolParadigma> dup = auditing.find("HolParadigma.findByFoarmAndHolLemmaAndSoart",
                new Param.Builder().add("foarm", toStore.getFoarm()).
                        add("hollemma", toStore.getHollemma()).add("soart", toStore.getSoart()).build()
                , 0, 1, HolParadigma.class).stream().findFirst();
        if (dup.isPresent()) {
            if (toStore.isTransient() || !dup.get().equals(toStore)) {
                throw new PersistenceException(toStore + " exists: " + dup.get());
            }
        }
    }
    
}
