/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.util.List;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "linguistic_category")
@NamedQuery(name = "LinguisticCategory.byCategory",
        query = "select lc from LinguisticCategory lc where category = :category")
@Access(AccessType.FIELD)
public class LinguisticCategory extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @NotNull
    @Column(name = "category")
    private String category;
    @NotNull
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId", fetch = FetchType.EAGER)
    private List<LinguisticValue> linguisticValues;

    public LinguisticCategory() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LinguisticValue> getLinguisticValues() {
        return linguisticValues;
    }

    public void setLinguisticValues(List<LinguisticValue> linguisticValues) {
        this.linguisticValues = linguisticValues;
    }

    @Override
    public String toString() {
        return "LinguisticCategory[ " + category + " ]";
    }
    
}
