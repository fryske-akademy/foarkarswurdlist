package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "NonPreferred")
@IdClass(NonPreferred.class)
public class NonPreferred implements Serializable {

    @Id
    private String nonstandard;
    @Id
    private String standard;
    @Id
    private String form;
    @Id
    private String linguistics;

    public String getNonstandard() {
        return nonstandard;
    }

    public void setNonstandard(String nonstandard) {
        this.nonstandard = nonstandard;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getLinguistics() {
        return linguistics;
    }

    public void setLinguistics(String linguistics) {
        this.linguistics = linguistics;
    }
}
