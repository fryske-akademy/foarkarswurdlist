package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;

import java.io.Serializable;

@IdClass(FormId.class)
public class FormId implements Serializable {

    private String lemma;
    private String form;
    private String linguistics;

    public FormId(String lemma, String form, String linguistics) {
        this.lemma = lemma;
        this.form = form;
        this.linguistics = linguistics;
    }
}
