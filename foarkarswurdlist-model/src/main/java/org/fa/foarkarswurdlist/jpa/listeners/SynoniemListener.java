/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import org.fa.foarkarswurdlist.jpa.Synoniem;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.Auditing;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.util.Optional;

/**
 *
 * @author eduard
 */
public class SynoniemListener extends CrudAware {
    
    @PreUpdate
    @PrePersist
    public void check(Synoniem toStore) {
        toStore.setNodiacrits(removeDiacrits(toStore.getVorm()));
        Auditing auditing = getIpaService();
        Optional<Synoniem> dup = auditing.find("Synoniem.findByVormAndLemma",
                new Param.Builder().add("vorm", toStore.getVorm()).add("lemma", toStore.getLemma()).build()
                , 0, 1, Synoniem.class).stream().findFirst();
        if (dup.isPresent()) {
            if (toStore.isTransient() || !dup.get().equals(toStore)) {
                throw new PersistenceException(toStore + " exists: " + dup.get());
            }
        }
    }    
    
}
