/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Takes care of translation of internal Fryske Akademy linguistic codes to
 * universaldependency codes.
 *
 * @author eduard
 */
@Named("universaldep")
@ApplicationScoped
public class FACodeToUniversal {

    private static final Map<String, String> CODE_TO_UNIVERSAL;

    public static final String UNKNOWN_CATEGORY = "unknown.";

    static {
        try (
                BufferedReader br = new BufferedReader(new InputStreamReader(Soartlist.class.getResourceAsStream("/vertaling.txt"), StandardCharsets.UTF_8))) {
            Map<String, String> m = new HashMap<>(95);
            br.lines().forEach((e) -> {
                String[] s = e.split(":");
                m.put(s[0], s.length == 2 ? s[1].trim() : UNKNOWN_CATEGORY + s[0]);
            });
            CODE_TO_UNIVERSAL = Collections.unmodifiableMap(m);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * return universaldependency equivalent codes for a FA linguistic code,
     * separated by spaces, see {@link #getTranslations() }.
     *
     * @param faCode
     * @return
     */
    public static String universalCodes(String faCode) {
        if (faCode==null|| faCode.isEmpty()) return "";
        if (CODE_TO_UNIVERSAL.containsKey(faCode)) {
            return CODE_TO_UNIVERSAL.get(faCode);
        } else {
            throw new IllegalArgumentException(String.format("illegal linguistic code: %s", faCode));
        }
    }
    
    public static String faPos(String universalpos) {
        return switch (universalpos.toUpperCase()) {
            case "ADP" -> "preposysje";
            case "PROPN" -> "eigennamme";
            case "VERB" -> "haadtw.";
            case "ADJ" -> "adjektyf";
            case "INTJ" -> "interjeksje";
            case "ADV" -> "adverbium";
            case "NUM" -> "numerale";
            case "PRON" -> "pronomen";
            case "CCONJ", "SCONJ" -> "konj.";
            case "NOUN" -> "subst.";
            case "AUX" -> "keppeltw.";
            case "DET" -> "artikel";
            default -> throw new IllegalArgumentException(String.format("cannot translate %s", universalpos));
        };
    }

    private static final Pattern short1 = Pattern.compile("([a-zA-Z]+)[.]yes");
    private static final Pattern short2 = Pattern.compile("[a-zA-Z]+[.]");
    private static final Pattern short3 = Pattern.compile("([a-zA-Z]+)%[.]yes");

    public static String shortUniversalCode(String code) {
        return short3.matcher(
                short2.matcher(
                        short1.matcher(code).replaceAll("$1%.yes")
                ).replaceAll("")
        ).replaceAll("$1.yes");
    }

    /**
     * return the universal pos code in lower case after converting the faCode to
     * universal codes, if there is no pos code return the empty String.
     *
     * @param faCode
     * @return
     */
    public static String universalPosCode(String faCode) {
        String us = universalCodes(faCode);
        if (us.toLowerCase().contains("pos.")) {
            int pos = us.toLowerCase().indexOf("pos.");
            int space = us.indexOf(" ", pos);
            return space == -1 ? us.substring(pos) : us.substring(pos, space);
        } else {
            return "";
        }
    }

    public static Map<String, String> getTranslations() {
        return CODE_TO_UNIVERSAL;
    }

    private static final List<String> universalCodes = new ArrayList<>(100);

    public static List<String> universalCodes() {
        synchronized (universalCodes) {
            if (universalCodes.isEmpty()) {
                Set<String> unique = new HashSet<>();
                CODE_TO_UNIVERSAL.forEach((key, value) -> new Scanner(value.trim()).forEachRemaining(unique::add));
                universalCodes.addAll(unique);
                Collections.sort(universalCodes);
            }
        }
        return universalCodes;
    }

    /**
     * Assumes a String in the form of {@link Collection#toString() }, returns a
     * collection of strings. Each string contains zero or more space separated
     * universal codes, together denoting the linguistics of a word(form).
     *
     * @param linguistics
     * @return
     */
    public static Collection<String> parseLinguistics(String linguistics) {
        Set<String> rv = new HashSet<>();
        if (linguistics != null && linguistics.startsWith("[") && linguistics.endsWith("]")) {
            rv.addAll(Arrays.asList(linguistics.substring(1, linguistics.length() - 1).split(" ?, ?")));
        }
        return rv;
    }


}
