/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "synoniem")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = Synonym.BYFORM,
                query = "select p.lemma.synonyms " +
                        "from Paradigm p where p.nodiacrits like :form  and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.synonyms is not empty and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Synonym.BY_FORM_SENSITIVE,
                query = "select p.lemma.synonyms " +
                        "from Paradigm p where p.form like :form  and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.synonyms is not empty and (p.lemma.checked=true or :checked=false)")
})
public class Synonym extends AbstractEntity {

    public static final String BYFORM = "synonym.byform";
    public static final String BY_FORM_SENSITIVE = "synonym.byform.sensitive";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "vorm", nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "betekenis")
    private String meaning;
    @JsonbTransient
    @JoinColumn
    @ManyToOne(optional = false)
    private Lemma lemma;


    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String betekenis) {
        this.meaning = betekenis;
    }

    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemma) {
        this.lemma = lemma;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String vorm) {
        this.form = vorm;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }
}
