package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "forms")
@IdClass(FormId.class)
public class Form implements Serializable {

    @Id
    private String lemma;
    @Id
    private String form;
    private String pron;
    private String hyph;
    @Id
    private String linguistics;
    private boolean lemmaPreferred;
    private boolean formPreferred;

    public String getLemma() {
        return lemma;
    }

    public String getForm() {
        return form;
    }

    public String getPron() {
        return pron;
    }

    public String getHyph() {
        return hyph;
    }

    public void setHyph(String hyph) {
        this.hyph = hyph;
    }

    public String getLinguistics() {
        return linguistics;
    }

    public boolean isLemmaPreferred() {
        return lemmaPreferred;
    }

    public boolean isFormPreferred() {
        return formPreferred;
    }

}
