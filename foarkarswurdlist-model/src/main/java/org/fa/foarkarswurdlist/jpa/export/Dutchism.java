package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "dutchism")
@IdClass(Dutchism.class)
public class Dutchism implements Serializable {

    @Id
    private String lemma;
    @Id
    private String dutchism;
    @Id
    private String form;
    @Id
    private String linguistics;
    @Id
    private String pos;

    public String getLemma() {
        return lemma;
    }

    public String getForm() {
        return form;
    }

    public String getLinguistics() {
        return linguistics;
    }

    public String getDutchism() {
        return dutchism;
    }

    public void setDutchism(String dutchism) {
        this.dutchism = dutchism;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }
}
