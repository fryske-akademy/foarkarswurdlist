/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import org.fa.foarkarswurdlist.jpa.FACodeToUniversal;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.Auditing;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreRemove;
import jakarta.persistence.PreUpdate;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 *
 * @author eduard
 */
public class LemmaListener extends CrudAware {

    @PreUpdate
    public void checkU(Lemma toStore) {
        save(toStore, false);
    }

    @PrePersist
    public void checkP(Lemma toStore) {
        save(toStore, true);
    }

    private static final Pattern DE = Pattern.compile("de.*subst");
    private static final Pattern IT = Pattern.compile("it.*subst");
    private static final Pattern DEIT = Pattern.compile("(de&it.*subst|it&de.*subst)");

    private void save(Lemma toStore, boolean isNew) {
        toStore.setPos(FACodeToUniversal.universalPosCode(toStore.getSoart()));
        toStore.setNodiacrits(
                removeDiacrits(toStore.getFoarm()).toLowerCase(Locale.ROOT));
        if (toStore.getHatfariant() && !toStore.getStandert()) {
            throw new PersistenceException(toStore + " is no standert Lemma, cannot have variants");
        }
        if ((toStore.getPrefix() != null && !toStore.getPrefix().isEmpty()) &&
                !(toStore.getSoart().equals(Lemma.SOART.haadtw.getDbValue()) || toStore.getSoart().equals(Lemma.SOART.keppeltw.getDbValue()))) {
            throw new PersistenceException("prefix is only allowed for verbs");
        }
        if (toStore.getStdlemId() != null && !toStore.getStdlemId().getStandert()) {
            throw new PersistenceException(toStore.getStdlemId() + " is no standert Lemma, cannot be used as standert");
        }
        if (toStore.getStdlemId() != null && toStore.getStandert()) {
            throw new PersistenceException(toStore + " is a variant, cannot be standert");
        }
        if (DEIT.matcher(toStore.getSoart()).find()) {
            toStore.setArticle("de&it");
        } else if (DE.matcher(toStore.getSoart()).find()) {
            toStore.setArticle("de");
        } else if (IT.matcher(toStore.getSoart()).find()) {
            toStore.setArticle("it");
        }
        Auditing auditing = getIpaService();
        if (toStore.getHatfariant()) {
            if (auditing.countDynamic(Param.one("stdlemId.foarm","stdlemId",toStore.getFoarm()),Lemma.class)==0) {
                throw new PersistenceException(toStore + " has no variants");
            }
        }
        if (!toStore.getHatfariant()) {
            if (auditing.countDynamic(Param.one("stdlemId.foarm","stdlemId",toStore.getFoarm()),Lemma.class)>0) {
                throw new PersistenceException(toStore + " has variants");
            }
        }
        Param.Builder params = new Param.Builder().add("foarm", toStore.getFoarm());
        if (toStore.getHomnum() != null) {
            params.add("sense", toStore.getHomnum());
        }
        Optional<Lemma> dup = auditing.find(toStore.getHomnum() == null ? "Lemma.byFoarm" : "Lemma.byFoarmAndSense",
                params.build(),
                 0, 1, Lemma.class).stream().findFirst();
        if (dup.isPresent()) {
            if (toStore.isTransient() || !dup.get().equals(toStore)) {
                throw new PersistenceException(toStore + " exists: " + dup.get());
            }
        }
    }

    @PreRemove
    public void checkStd(Lemma toRemove) {
        Auditing auditing = getIpaService();
        Param.Builder params = new Param.Builder().add("foarm", toRemove.getFoarm());
        if (toRemove.getHomnum() != null) {
            params.add("sense", toRemove.getHomnum());
        }
        Optional<Lemma> hasStd = auditing.find(toRemove.getHomnum() == null ? "Lemma.byStdFoarm" : "Lemma.byStdFoarmAndSense",
                params.build(),
                 0, 1, Lemma.class).stream().findFirst();

        if (hasStd.isPresent()) {
            throw new PersistenceException(toRemove + " is standert Lemma for: " + hasStd.get());
        }
    }

}
