/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fa.foarkarswurdlist.jpa.listeners.SynoniemListener;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Synoniem.findByVormAndLemma", query = "SELECT h FROM Synoniem h where h.vorm = :vorm and h.lemma = :lemma"),
        @NamedQuery(name = "Synoniem.findByVorm", query = "SELECT h FROM Synoniem h where h.vorm = :vorm")})
@EntityListeners(SynoniemListener.class)
public class Synoniem extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String vorm;
    @NotNull
    @NotEmpty
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "betekenis")
    private String betekenis;
    @Column(name = "opmerking")
    private String opmerking;
    @JoinColumn
    @ManyToOne(optional = false)
    @NotNull
    private Lemma lemma;
    @Column(name = "ok")
    private Boolean ok=false;

    public Synoniem() {
    }
    public String getOpmerking() {
        return opmerking;
    }

    public void setOpmerking(String opmerking) {
        this.opmerking = opmerking;
    }

    public String getBetekenis() {
        return betekenis;
    }

    public void setBetekenis(String betekenis) {
        this.betekenis = betekenis;
    }

    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemma) {
        this.lemma = lemma;
    }

    public String getVorm() {
        return vorm;
    }

    public void setVorm(String vorm) {
        this.vorm = vorm;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }
}
