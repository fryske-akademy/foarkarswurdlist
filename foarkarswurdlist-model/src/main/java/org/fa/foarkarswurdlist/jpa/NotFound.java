/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

/**
 *
 * @author eduard
 */
@Entity
@Access(AccessType.FIELD)
public class NotFound extends AbstractEntity {
    
    @Column(nullable = false, unique = true)
    private String foarm;

    public String getFoarm() {
        return foarm;
    }

    public NotFound setFoarm(String foarm) {
        this.foarm = foarm;
        return this;
    }
    
}
