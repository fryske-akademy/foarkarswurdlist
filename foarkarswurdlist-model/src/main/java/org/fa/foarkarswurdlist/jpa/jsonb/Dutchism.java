/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.util.List;

/**
 *
 * @author eduard
 */
@Cacheable
@Entity
@Table(name = "HolLemma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = Dutchism.BYFORM,
                query = "select p.lemma.dutchisms " +
                        "from Paradigm p where p.nodiacrits like :form and p.lemma.publish=true and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.dutchisms is not empty and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Dutchism.BY_FORM_SENSITIVE,
                query = "select p.lemma.dutchisms " +
                        "from Paradigm p where p.form like :form and p.lemma.publish=true and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.dutchisms is not empty and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Dutchism.FULL_LEMMA_BYFORM,
                query = "select distinct p.dutchism.lemma " +
                        "from DutchismParadigm p where p.nodiacrits like :form and p.dutchism.lemma.publish=true and (:pos='' or p.dutchism.lemma.pos=:pos)" +
                        " and (p.dutchism.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Dutchism.FULL_LEMMA_BY_FORM_SENSITIVE,
                query = "select distinct p.dutchism.lemma " +
                        "from DutchismParadigm p where p.form like :form and p.dutchism.lemma.publish=true and (:pos='' or p.dutchism.lemma.pos=:pos)" +
                        " and (p.dutchism.lemma.checked=true or :checked=false)")
})
public class Dutchism extends AbstractEntity {
    public static final String BYFORM = "Dutchism.byform";
    public static final String BY_FORM_SENSITIVE = "Dutchism.byform.sensitive";
    public static final String FULL_LEMMA_BYFORM = "Dutchism.fulllemmabyform";
    public static final String FULL_LEMMA_BY_FORM_SENSITIVE = "Dutchism.fulllemmabyform.sensitive";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "foarm", nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @JsonbTransient
    @JoinColumn(name = "lemma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Lemma lemma;
    @OneToMany(mappedBy = "dutchism", cascade = CascadeType.REMOVE)
    private List<DutchismParadigm> dutchismParadigms;

    public String getForm() {
        return form;
    }

    public void setForm(String foarm) {
        this.form = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }
    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemma) {
        this.lemma = lemma;
    }

    public List<DutchismParadigm> getDutchismParadigms() {
        return dutchismParadigms;
    }

    public void setDutchismParadigms(List<DutchismParadigm> holParadigmas) {
        this.dutchismParadigms = holParadigmas;
    }

    @Override
    public String toString() {
        return "HolLemma{" + "foarm=" + form + ", id=" + getId() + '}';
    }


    
}
