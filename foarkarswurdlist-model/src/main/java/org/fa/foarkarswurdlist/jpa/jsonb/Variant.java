package org.fa.foarkarswurdlist.jpa.jsonb;

public class Variant {
    private final String form, preferredForm, pos;

    public Variant(String form, String preferredForm, String pos) {
        this.form = form;
        this.preferredForm = preferredForm;
        this.pos = pos;
    }

    public String getForm() {
        return form;
    }

    public String getPreferredForm() {
        return preferredForm;
    }

    public String getPos() {
        return pos;
    }
}
