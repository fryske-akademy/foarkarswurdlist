/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fa.foarkarswurdlist.jpa.listeners.HolLemmaListener;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.util.List;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name = "HolLemma")
@NamedQueries({
    @NamedQuery(name = "HolLemma.findAll", query = "SELECT h FROM HolLemma h"),
    @NamedQuery(name = "HolLemma.findByFoarmAndLemma", query = "SELECT h FROM HolLemma h where h.foarm = :foarm and h.lemma = :lemma"),
    @NamedQuery(name = "HolLemma.likeFoarm", query = "SELECT h FROM HolLemma h where h.foarm like :foarm"),
    @NamedQuery(name = "HolLemma.findByFoarm", query = "SELECT h FROM HolLemma h where h.foarm = :foarm")})
@EntityListeners(HolLemmaListener.class)
@Access(AccessType.FIELD)
public class HolLemma extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @NotNull
    @NotEmpty
    @Column(name = "foarm", nullable = false)
    private String foarm;
    @NotNull
    @NotEmpty
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @NotNull
    @Column(name = "lemma_sels_net",nullable = false)
    private Boolean lemmaSelsNet;
    @JoinColumn(name = "lemma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Lemma lemma;
    @OneToMany(mappedBy = "hollemma", cascade = CascadeType.REMOVE)
    private List<HolParadigma> holParadigmas;

    public HolLemma() {
    }

    public String getFoarm() {
        return foarm;
    }

    public void setFoarm(String foarm) {
        this.foarm = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public Boolean getLemmaSelsNet() {
        return lemmaSelsNet;
    }

    public void setLemmaSelsNet(Boolean lemmaSelsNet) {
        this.lemmaSelsNet = lemmaSelsNet;
    }
    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemma) {
        this.lemma = lemma;
    }

    public List<HolParadigma> getHolParadigmas() {
        return holParadigmas;
    }

    public void setHolParadigmas(List<HolParadigma> holParadigmas) {
        this.holParadigmas = holParadigmas;
    }

    @Override
    public String toString() {
        return "HolLemma{" + "foarm=" + foarm + ", id=" + getId() + '}';
    }


    
}
