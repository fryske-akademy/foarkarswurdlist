/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import org.fa.foarkarswurdlist.jpa.HolLemma;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.Auditing;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.util.Optional;

/**
 *
 * @author eduard
 */
public class HolLemmaListener extends CrudAware {
    
    @PreUpdate
    @PrePersist
    public void check(HolLemma toStore) {
        toStore.setNodiacrits(removeDiacrits(toStore.getFoarm()));
        Auditing auditing = getIpaService();
        Optional<HolLemma> dup = auditing.find("HolLemma.findByFoarmAndLemma",
                new Param.Builder().add("foarm", toStore.getFoarm()).add("lemma", toStore.getLemma()).build()
                , 0, 1, HolLemma.class).stream().findFirst();
        if (dup.isPresent()) {
            if (toStore.isTransient() || !dup.get().equals(toStore)) {
                throw new PersistenceException(toStore + " exists: " + dup.get());
            }
        }
    }    
    
}
