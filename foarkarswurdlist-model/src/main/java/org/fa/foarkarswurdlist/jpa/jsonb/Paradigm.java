/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedNativeQueries;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "Paradigma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = Paradigm.FORM,
                query = "select p.form from Paradigm p where p.nodiacrits like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.FORM_SENSITIVE,
                query = "select p.form from Paradigm p where p.form like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.HYPHBYFORM,
                query = "select p.hyphenation from Paradigm p where p.nodiacrits like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.HYPHBY_FORM_SENSITIVE,
                query = "select p.ipa from Paradigm p where p.form like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.PRONBYFORM,
                query = "select p.ipa from Paradigm p where p.nodiacrits like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.PRONBY_FORM_SENSITIVE,
                query = "select p.hyphenation from Paradigm p where p.form like :form" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.FULL_LEMMA_BYFORM,
                query = "select distinct p.lemma " +
                        "from Paradigm p where p.lemma.preferred = true and p.nodiacrits like :form and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Paradigm.FULL_LEMMA_BY_FORM_SENSITIVE,
                query = "select distinct p.lemma " +
                        "from Paradigm p where p.lemma.preferred = true and p.form like :form and (:pos='' or p.lemma.pos=:pos)" +
                        " and p.lemma.publish=true and (p.lemma.checked=true or :checked=false)")
})
@NamedNativeQueries({
    @NamedNativeQuery(name = Paradigm.IPA,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos"
            + " from Paradigma p join lemma l on p.lemma_id=l.id where "
            + "p.pron like ?1 order by p.id"),
    @NamedNativeQuery(name = Paradigm.IPACOUNT,
        query = "select count(p.foarm)"
            + " from Paradigma p where "
            + "p.pron like ?1"),
    @NamedNativeQuery(name = Paradigm.IPA2,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos"
            + " from Paradigma p join lemma l on p.lemma_id=l.id where "
            + "p.pron like ?1 or p.pron like ?2 order by p.id"),
    @NamedNativeQuery(name = Paradigm.IPACOUNT2,
        query = "select count(p.foarm)"
            + " from Paradigma p where "
            + "p.pron like ?1 or p.pron like ?2"),
    @NamedNativeQuery(name = Paradigm.IPA3,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos"
            + " from Paradigma p join lemma l on p.lemma_id=l.id where "
            + "p.pron like ?1 or p.pron like ?2 or p.pron like ?3 order by p.id"),
    @NamedNativeQuery(name = Paradigm.IPACOUNT3,
        query = "select count(p.foarm)"
            + " from Paradigma p where "
            + "p.pron like ?1 or p.pron like ?2 or p.pron like ?3"),
    @NamedNativeQuery(name = Paradigm.IPA4,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos"
            + " from Paradigma p join lemma l on p.lemma_id=l.id where "
            + "p.pron like ?1 or p.pron like ?2 or p.pron like ?3 or p.pron like ?4 order by p.id"),
    @NamedNativeQuery(name = Paradigm.IPACOUNT4,
        query = "select count(p.foarm)"
            + " from Paradigma p where "
            + "p.pron like ?1 or p.pron like ?2 or p.pron like ?3 or p.pron like ?4"),
    @NamedNativeQuery(name = Paradigm.IPAVOWEL,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos" +
"          from Paradigma p join lemma l on p.lemma_id=l.id  where" +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) order by p.id;"),
    @NamedNativeQuery(name = Paradigm.IPAVOWELCOUNT,
        query = "select count(p.foarm)" +
            " from Paradigma p where " +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3));"),
    @NamedNativeQuery(name = Paradigm.IPAVOWEL2,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos" +
"          from Paradigma p join lemma l on p.lemma_id=l.id  where" +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6)) order by p.id;"),
    @NamedNativeQuery(name = Paradigm.IPAVOWELCOUNT2,
        query = "select count(p.foarm)" +
            " from Paradigma p where " +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6));"),
    @NamedNativeQuery(name = Paradigm.IPAVOWEL3,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos" +
"          from Paradigma p join lemma l on p.lemma_id=l.id where" +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6)) or" +
"          (p.pron like ?7 and (p.pron similar to ?8 or p.pron = ?9)) order by p.id;"),
    @NamedNativeQuery(name = Paradigm.IPAVOWELCOUNT3,
        query = "select count(p.foarm)" +
            " from Paradigma p where " +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6)) or" +
"          (p.pron like ?7 and (p.pron similar to ?8 or p.pron = ?9));"),
    @NamedNativeQuery(name = Paradigm.IPAVOWEL4,
        query = "select p.foarm, p.pron, p.ofbrekking, l.foarm, l.stdlem_id, l.pos" +
"          from Paradigma p join lemma l on p.lemma_id=l.id where" +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6)) or" +
"          (p.pron like ?7 and (p.pron similar to ?8 or p.pron = ?9)) or" +
"          (p.pron like ?10 and (p.pron similar to ?11 or p.pron = ?12)) order by p.id;"),
    @NamedNativeQuery(name = Paradigm.IPAVOWELCOUNT4,
        query = "select count(p.foarm)" +
            " from Paradigma p where " +
"          (p.pron like ?1 and (p.pron similar to ?2 or p.pron = ?3)) or" +
"          (p.pron like ?4 and (p.pron similar to ?5 or p.pron = ?6)) or" +
"          (p.pron like ?7 and (p.pron similar to ?8 or p.pron = ?9)) or" +
"          (p.pron like ?10 and (p.pron similar to ?11 or p.pron = ?12));")
})
public class Paradigm extends AbstractEntity {
    public static final String FORM = "Paradigm.form";
    public static final String FORM_SENSITIVE = "Paradigm.form.sensitive";
    public static final String HYPHBYFORM = "Paradigm.hyphbyform";
    public static final String HYPHBY_FORM_SENSITIVE = "Paradigm.hyphbyform.sensitive";
    public static final String PRONBYFORM = "Paradigm.pronbyform";
    public static final String PRONBY_FORM_SENSITIVE = "Paradigm.pronbyform.sensitive";
    public static final String FULL_LEMMA_BYFORM = "Paradigm.fulllemmabyform";
    public static final String FULL_LEMMA_BY_FORM_SENSITIVE = "Paradigm.fulllemmabyform.sensitive";
    public static final String IPAVOWEL = "ipavowel";
    public static final String IPAVOWELCOUNT = "ipavowelcount";
    public static final String IPA = "ipa";
    public static final String IPACOUNT = "ipacount";
    public static final String IPAVOWEL2 = "ipavowel2";
    public static final String IPAVOWELCOUNT2 = "ipavowelcount2";
    public static final String IPAVOWEL3 = "ipavowel3";
    public static final String IPAVOWELCOUNT3 = "ipavowelcount3";
    public static final String IPAVOWEL4 = "ipavowel4";
    public static final String IPAVOWELCOUNT4 = "ipavowelcount4";
    public static final String IPA2 = "ipa2";
    public static final String IPACOUNT2 = "ipacount2";
    public static final String IPA3 = "ipa3";
    public static final String IPACOUNT3 = "ipacount3";
    public static final String IPA4 = "ipa4";
    public static final String IPACOUNT4 = "ipacount4";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "foarm",nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "pron")
    private String ipa;
    @Column(name = "std")
    private Boolean preferred =false;
    @Column(name = "ofbrekking")
    private String hyphenation;
    @JsonbTransient
    @JoinColumn(name = "lemma_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Lemma lemma;
    @Column(length = 512)
    private String linguistics;

    public String getForm() {
        return form;
    }

    public void setForm(String foarm) {
        this.form = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public String getIpa() {
        return ipa;
    }

    public void setIpa(String ipa) {
        this.ipa = ipa;
    }

    public Boolean getPreferred() {
        return preferred;
    }

    public void setPreferred(Boolean std) {
        this.preferred = std;
    }

    public String getHyphenation() {
        return hyphenation;
    }

    public void setHyphenation(String ofbrekking) {
        this.hyphenation = ofbrekking;
    }

    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemmaId) {
        this.lemma = lemmaId;
    }

    public String getLinguistics() {
        return linguistics;
    }

    public void setLinguistics(String linguistics) {
        this.linguistics = linguistics;
    }

    public String getSplitfoarm() {
        return lemma==null?null: Paradigma.getSplitForm(form,lemma.getPrefix(),linguistics);
    }

    @Override
    public String toString() {
        return "Paradigma{" + "foarm=" + form + ", id=" + getId() + '}';
    }

    
}
