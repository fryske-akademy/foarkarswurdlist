/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;
import java.util.List;

import static org.fa.foarkarswurdlist.jpa.jsonb.Lemma.LASTCHANGED;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "Lemma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = Lemma.POSBYFORM,
                query = "select l.form, l.pos from Lemma l where l.publish=true and l in" +
                        " (select p.lemma from Paradigm p where p.nodiacrits like :form)" +
                        " and (l.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.POSBY_FORM_SENSITIVE,
                query = "select l.form, l.pos from Lemma l where l.publish=true and l in" +
                        " (select p.lemma from Paradigm p where p.form like :form)" +
                        " and (l.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.ARTICLE,
                query = "select l.form, l.article from Lemma l where l.publish=true and l.pos='Pos.NOUN' and l.nodiacrits like :lemma" +
                        " and (l.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.ARTICLE_SENSITIVE,
                query = "select l.form, l.article from Lemma l where l.publish=true and l.pos='Pos.NOUN' and l.form like :lemma" +
                        " and (l.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.VARBY_LEMMA,
                query = "select l.form, l.pos, " +
                        "l.preferredLemma.form from Lemma l " +
                        "where l.publish=true and (:pos='' or l.pos=:pos)" +
                        " and (l.checked=true or :checked=false) and l.preferredLemma.nodiacrits like :lemma"),
        @NamedQuery(name = Lemma.VARBY_LEMMA_SENSITIVE,
                query = "select l.form, l.pos, " +
                        "l.preferredLemma.form from Lemma l " +
                        "where l.publish=true and (:pos='' or l.pos=:pos)" +
                        " and (l.checked=true or :checked=false) and l.preferredLemma.form like :lemma"),
        @NamedQuery(name = Lemma.BY_SYN_FORM,
                query = "select s.lemma from Synonym s " +
                        "where s.lemma.publish=true and s.nodiacrits like :form and (:pos='' or s.lemma.pos=:pos) and (s.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.BY_SYN_FORM_SENSITIVE,
                query = "select s.lemma from Synonym s " +
                        "where s.lemma.publish=true and s.form like :form and (:pos='' or s.lemma.pos=:pos) and (s.lemma.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.BY_VAR_FORM,
                query = "select p.lemma.preferredLemma from Paradigm p where" +
                        " p.lemma.preferredLemma is not null and p.lemma.preferredLemma.publish=true and p.nodiacrits like :form and (:pos='' or p.lemma.pos=:pos)" +
                        " and (p.lemma.preferredLemma.checked=true or :checked=false)"),
        @NamedQuery(name = Lemma.BY_VAR_FORM_SENSITIVE,
                query = "select p.lemma.preferredLemma from Paradigm p where" +
                        " p.lemma.preferredLemma is not null and p.lemma.preferredLemma.publish=true and p.form like :form and (:pos='' or p.lemma.pos=:pos)" +
                        " and (p.lemma.preferredLemma.checked=true or :checked=false)"),
})
@NamedNativeQuery(name = LASTCHANGED,
        query = "select to_timestamp('01-01-1970 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                " + timestamp * interval '1 millisecond' as timestamp  from revisioninfo order by id desc limit 1;"
)
public class Lemma extends AbstractEntity {
    public static final String POSBYFORM = "Lemma.byform";
    public static final String POSBY_FORM_SENSITIVE = "Lemma.byform.sensitive";
    public static final String VARBY_LEMMA = "Lemma.varbylemma";
    public static final String VARBY_LEMMA_SENSITIVE = "Lemma.varbylemma.sensitive";
    public static final String BY_SYN_FORM = "Lemma.bysynform";
    public static final String BY_SYN_FORM_SENSITIVE = "Lemma.bysynform.sensitive";
    public static final String BY_VAR_FORM = "Lemma.byvarform";
    public static final String BY_VAR_FORM_SENSITIVE = "Lemma.byvarform.sensitive";
    public static final String LASTCHANGED = "lastchanged";
    public static final String ARTICLE = "Lemma.article";
    public static final String ARTICLE_SENSITIVE = "Lemma.article.sensitive";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name="neisjoen",nullable = false)
    private Boolean checked =false;
    @Column(name = "standert")
    private Boolean preferred =false;
    @NotNull
    @Column
    private Boolean publish=false;
    @Column(name = "foarm",nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "pos",nullable = false)
    private String pos;
    @Column(name = "betsj")
    private String meaning;
    @Column
    private String article;
    @NotNull
    @Column(name = "prefix",nullable = false)
    private String prefix="";
    @JoinColumn(name = "stdlem_id", referencedColumnName = "id")
    @ManyToOne
    private Lemma preferredLemma;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma")
    private List<Paradigm> paradigms;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "lemma")
    private List<Dutchism> dutchisms;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma")
    private List<Synonym> synonyms;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma")
    private List<LemmaPart> parts;

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean neisjoen) {
        this.checked = neisjoen;
    }

    public Boolean getPreferred() {
        return preferred;
    }

    public void setPreferred(Boolean standert) {
        this.preferred = standert;
    }

    public @NotNull Boolean getPublish() {
        return publish;
    }

    public void setPublish(@NotNull Boolean publish) {
        this.publish = publish;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String foarm) {
        this.form = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public Lemma getPreferredLemma() {
        return preferredLemma;
    }

    public void setPreferredLemma(Lemma stdlemId) {
        this.preferredLemma = stdlemId;
    }

    public List<Paradigm> getParadigms() {
        return paradigms;
    }

    public void setParadigms(List<Paradigm> paradigmas) {
        this.paradigms = paradigmas;
    }

    public List<Dutchism> getDutchisms() {
        return dutchisms;
    }

    public void setDutchisms(List<Dutchism> holLemmas) {
        this.dutchisms = holLemmas;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    @Override
    public String toString() {
        return "Lemma{" + "foarm=" + form + ", id=" + getId() + '}';
    }

    public List<Synonym> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<Synonym> synoniemen) {
        this.synonyms = synoniemen;
    }

    public List<LemmaPart> getParts() {
        return parts;
    }

    public void setParts(List<LemmaPart> parts) {
        this.parts = parts;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
