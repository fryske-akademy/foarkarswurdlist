/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Audited
@Table(name = "Soartlist")
@NamedQueries({
    @NamedQuery(name = "Soartlist.findAll", query = "SELECT s FROM Soartlist s"),
    @NamedQuery(name = "Soartlist.byLabel", query = "SELECT s FROM Soartlist s where s.soartlabel = :label"),
    @NamedQuery(name = "Soartlist.likeLabel", query = "SELECT s FROM Soartlist s where s.soartlabel like :label")})
@Access(AccessType.FIELD)
public class Soartlist extends AbstractEntity implements Comparable<Soartlist> {

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "soartlabel", nullable = false)
    private String soartlabel;

    public Soartlist() {
    }

    public String getSoartlabel() {
        return soartlabel;
    }

    public void setSoartlabel(String soartlabel) {
        this.soartlabel = soartlabel;
    }
    
    @Transient
    public String getUniversalString() {
        return FACodeToUniversal.shortUniversalCode(FACodeToUniversal.universalCodes(soartlabel));
    }

    @Override
    public String toString() {
        return "Soartlist{" + "soartlabel=" + soartlabel + ", id=" + getId() + '}';
    }

    @Override
    public int compareTo(Soartlist o) {
        return soartlabel.toLowerCase().compareTo(o.soartlabel.toLowerCase());
    }
}
