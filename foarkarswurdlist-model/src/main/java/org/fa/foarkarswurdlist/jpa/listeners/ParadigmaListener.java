/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import org.fa.foarkarswurdlist.jpa.FACodeToUniversal;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fryske_akademy.jpa.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.fa.foarkarswurdlist.ejb.IpaService;

/**
 *
 * @author eduard
 */
public class ParadigmaListener extends CrudAware {

    @PreUpdate
    public void checkU(Paradigma toStore) {
        check(toStore,false);
    }
    @PrePersist
    public void checkC(Paradigma toStore) {
        check(toStore,true);
    }

    private void check(Paradigma toStore, boolean create) {
        toStore.setNodiacrits(removeDiacrits(toStore.getFoarm()));
        IpaService ipaService = getIpaService();
        Param.Builder params = new Param.Builder().add("foarm", toStore.getFoarm()).add("lemma", toStore.getLemmaId());
        Optional<Paradigma> dup = ipaService.find("Paradigma.findByFoarmAndLemma",
                params.build(),
                 0, 1, Paradigma.class).stream().findFirst();
        if (dup.isPresent()) {
            if (toStore.isTransient() || !dup.get().equals(toStore)) {
                throw new PersistenceException(toStore + " exists: " + dup.get());
            }
        }
        if (toStore.isClitic() && toStore.isSplit() && !toStore.getFoarm().endsWith(" " + toStore.getLemmaId().getPrefix())) {
            toStore.setFoarm(toStore.getFoarm().substring(toStore.getLemmaId().getPrefix().length()) + " " + toStore.getLemmaId().getPrefix());
        }
        List<String> UD = toStore.getSoarten().stream()
                .map(s -> FACodeToUniversal.universalCodes(s.getSoartlabel()))
                .collect(Collectors.toCollection(ArrayList::new));
        String s = FACodeToUniversal.universalCodes(toStore.getLemmaId().getSoart()).trim();
        if (!s.isEmpty()) UD.add(s);
        toStore.setLinguistics(UD.isEmpty()?List.of("Pos.x").toString():UD.toString());
    }

}
