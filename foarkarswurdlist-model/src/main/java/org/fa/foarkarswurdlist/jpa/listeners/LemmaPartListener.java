/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.listeners;

import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import java.util.List;
import java.util.Locale;
import jakarta.persistence.PersistenceException;
import org.fa.foarkarswurdlist.jpa.LemmaPart;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;


/**
 *
 * @author eduard
 */
public class LemmaPartListener extends CrudAware {

    @PreUpdate
    public void checkU(LemmaPart toStore) {
        save(toStore, false);
    }

    @PrePersist
    public void checkP(LemmaPart toStore) {
        save(toStore, true);
    }

    private void save(LemmaPart toStore, boolean isNew) {
        toStore.setNodiacrits(
                removeDiacrits(toStore.getPart()).toLowerCase(Locale.ROOT));
        if (toStore.getBase()!=null&&toStore.getBase()) {
            Param.Builder p = new Param.Builder()
                    .add("lemma", toStore.getLemma())
                    .add("base", true);
            if (!toStore.isTransient()) {
                p.add("id", "id", OPERATOR.NE, toStore.getId(), false, false);

            }
            List<LemmaPart> base = getIpaService().findDynamic(0, 1, null, p.build(), LemmaPart.class);
            if (!base.isEmpty()) {
                throw new PersistenceException("Only one part can be the base");
            }
        }
    }

}
