package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "SpellChecker")
public class SpellChecker {
    @Id
    private String foarm;
    private String ofbrekking;

    public String getFoarm() {
        return foarm;
    }

    public void setFoarm(String foarm) {
        this.foarm = foarm;
    }

    public String getOfbrekking() {
        return ofbrekking;
    }

    public void setOfbrekking(String ofbrekking) {
        this.ofbrekking = ofbrekking;
    }

}
