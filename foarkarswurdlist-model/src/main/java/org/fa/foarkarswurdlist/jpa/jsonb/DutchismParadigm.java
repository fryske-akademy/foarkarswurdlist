/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "HolParadigma")
@Access(AccessType.FIELD)
public class DutchismParadigm extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "foarm",nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @JsonbTransient
    @JoinColumn(name = "hollemma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Dutchism dutchism;

    public String getForm() {
        return form;
    }

    public void setForm(String foarm) {
        this.form = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public Dutchism getDutchism() {
        return dutchism;
    }

    public void setDutchism(Dutchism hollemma) {
        this.dutchism = hollemma;
    }

    @Override
    public String toString() {
        return "HolParadigma{" + "foarm=" + form + ", id=" + getId() + '}';
    }

    
}
