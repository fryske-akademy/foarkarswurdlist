/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fa.foarkarswurdlist.jpa.listeners.HolParadigmaListener;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name = "HolParadigma")
@Access(AccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "HolParadigma.findAll", query = "SELECT h FROM HolParadigma h"),
    @NamedQuery(name = "HolParadigma.findByFoarmAndHolLemma",
            query = "SELECT h FROM HolParadigma h where h.foarm=:foarm and h.hollemma=:hollemma"),
    @NamedQuery(name = "HolParadigma.findByFoarmAndHolLemmaAndSoart",
            query = "SELECT h FROM HolParadigma h where h.foarm=:foarm and h.hollemma=:hollemma and soart = :soart"),
    @NamedQuery(name = "HolParadigma.likeFoarm",
            query = "SELECT h FROM HolParadigma h where h.foarm like :foarm"),
    @NamedQuery(name = "HolParadigma.findByFoarm",
            query = "SELECT h FROM HolParadigma h where h.foarm=:foarm")})
@EntityListeners(HolParadigmaListener.class)
public class HolParadigma extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "foarm",nullable = false)
    @NotNull
    @NotEmpty
    private String foarm;
    @NotNull
    @NotEmpty
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "opmerking")
    private String opmerking;
    @JoinColumn(name = "hollemma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private HolLemma hollemma;
    @JoinColumn(name = "soart", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Soartlist soart;

    public HolParadigma() {
    }

    public String getFoarm() {
        return foarm;
    }

    public void setFoarm(String foarm) {
        this.foarm = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public String getOpmerking() {
        return opmerking;
    }

    public void setOpmerking(String opmerking) {
        this.opmerking = opmerking;
    }

    public HolLemma getHollemma() {
        return hollemma;
    }

    public void setHollemma(HolLemma hollemma) {
        this.hollemma = hollemma;
    }

    public Soartlist getSoart() {
        return soart;
    }

    public void setSoart(Soartlist soart) {
        this.soart = soart;
    }

    @Override
    public String toString() {
        return "HolParadigma{" + "foarm=" + foarm + ", id=" + getId() + '}';
    }

    
}
