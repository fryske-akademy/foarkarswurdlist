/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fa.foarkarswurdlist.jpa.listeners.LemmaListener;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name = "Lemma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Lemma.expand", query = "SELECT l FROM Lemma l join fetch l.paradigmas where l.id=:id")
        ,
        @NamedQuery(name = "Lemma.expandSyns", query = "SELECT l FROM Lemma l left join fetch l.synoniemen where l=:l")
        ,
    @NamedQuery(name = "Lemma.findAll", query = "SELECT l FROM Lemma l")
    ,
    @NamedQuery(name = "Lemma.byFoarmAndSense", query = "SELECT l FROM Lemma l where l.foarm = :foarm and l.homnum = :sense")
    ,
    @NamedQuery(name = "Lemma.byStdFoarm", query = "SELECT l FROM Lemma l where l.stdlemId.foarm = :foarm and l.stdlemId.homnum is null")
    ,
    @NamedQuery(name = "Lemma.byStdFoarmAndSense", query = "SELECT l FROM Lemma l where l.stdlemId.foarm = :foarm and l.stdlemId.homnum = :sense")
    ,
    @NamedQuery(name = "Lemma.byFoarm", query = "SELECT l FROM Lemma l where l.foarm = :foarm and l.homnum is null")
    ,
    @NamedQuery(name = "Lemma.likeParadigm", query = "SELECT l FROM Lemma l where l.id in (select p.lemmaId.id from Paradigma p where p.foarm like :foarm)"),
    @NamedQuery(name = "Lemma.likeDutchism", query = "SELECT l FROM Lemma l where l.id in (select h.lemma.id from HolLemma h where h.foarm like :foarm)"),
    @NamedQuery(name = "Lemma.likeDutchismParadigm", query = "SELECT l FROM Lemma l where l.id in ("
            + "select h.lemma.id from HolLemma h where h.id in ("
                + "select p.hollemma.id from HolParadigma p where p.foarm like :foarm))"),
    @NamedQuery(name = "Lemma.unusedStandert",query = "select l from Lemma l where l.hatfariant = true and not exists ("
            + "select ll from Lemma ll where ll.stdlemId = l)"),
    @NamedQuery(name = "Lemma.likeFoarm", query = "SELECT l FROM Lemma l where l.foarm like :foarm")})
@EntityListeners(LemmaListener.class)
public class Lemma extends AbstractEntity implements Serializable {

    public enum SOART {
        de_subst("de-subst."),
        it_subst("it-subst."),
        in_subst("in-subst."),
        haadtw("haadtw."),
        adjektyf("adjektyf"),
        ôfkoarting("ôfkoarting"),
        preposysje("preposysje"),
        pl_subst("pl-subst."),
        /**
         * @see #plaknamme
         * @see #persoansnamme
         * @deprecated
         */
        @Deprecated
        eigennamme("eigennamme"),
        plaknamme("plaknamme"),
        persoansnamme("persoansnamme"),
        de_it_subst("de&it-subst."),
        interjeksje("interjeksje"),
        adverbium("adverbium"),
        numerale("numerale"),
        pronomen("pronomen"),
        konj("konj."),
        it_de_subst("it&de-subst."),
        subst("subst."),
        keppeltw("keppeltw."),
        artikel("artikel");
        private final String dbValue;
        
        private static final Map<String, SOART> m;
        static {
            SOART[] values = SOART.values();
            m = new LinkedHashMap<>(values.length);
            for (SOART s : values) {
                m.put(s.dbValue, s);
            }
        }

        SOART(String dbValue) {
            this.dbValue = dbValue;
        }

        
        @Override
        public String toString() {
            return dbValue;
        }        
        
        public static SOART fromDbValue(String dbValue) {
            return m.get(dbValue);
        }

        public String getDbValue() {
            return dbValue;
        }

        public String universalPos() {
            return FACodeToUniversal.universalPosCode(getDbValue());
        }
        
        public static final List<String> DBVALUES = List.copyOf(SOART.m.keySet());

    }

    @Serial
    private static final long serialVersionUID = 1L;
    @NotNull
    @Column(nullable = false)
    private Boolean neisjoen=false;
    @NotNull
    @Column(name = "standert")
    private Boolean standert=false;
    @NotNull
    @Column
    private Boolean publish=false;
    @NotNull
    @NotEmpty
    @Column(name = "foarm",nullable = false)
    private String foarm;
    @NotNull
    @NotEmpty
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @NotNull
    @Column(name = "prefix",nullable = false)
    private String prefix="";
    @Column
    private String superlemmaLink;
    @Column(name = "homnum")
    private Short homnum;
    @NotNull
    @NotEmpty
    @Column(name = "soart",nullable = false)
    private String soart;
    @Column
    @Size(max = 10)
    private String article;
    @Size(max = 10)
    @Column(name = "pos",nullable = false)
    private String pos;
    @Column(name = "betsj")
    private String betsj;
    @Column(name = "hatfariant")
    private Boolean hatfariant=false;
    @JoinColumn(name = "stdlem_id", referencedColumnName = "id")
    @ManyToOne
    private Lemma stdlemId;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemmaId")
    private List<Paradigma> paradigmas;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "lemma")
    private List<HolLemma> holLemmas;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma")
    private List<Synoniem> synoniemen;
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma")
    private List<LemmaPart> parts;

    public Lemma() {
    }


    public Boolean getNeisjoen() {
        return neisjoen;
    }

    public void setNeisjoen(Boolean neisjoen) {
        this.neisjoen = neisjoen;
    }

    public Boolean getStandert() {
        return standert;
    }

    public void setStandert(Boolean standert) {
        this.standert = standert;
    }

    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public String getFoarm() {
        return foarm;
    }

    public void setFoarm(String foarm) {
        this.foarm = foarm!=null?foarm.trim():null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Short getHomnum() {
        return homnum;
    }

    public void setHomnum(Short homnum) {
        this.homnum = homnum;
    }

    public String getSoart() {
        return soart;
    }

    public void setSoart(String soart) {
        this.soart = soart;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getBetsj() {
        return betsj;
    }

    public void setBetsj(String betsj) {
        this.betsj = betsj;
    }

    public Boolean getHatfariant() {
        return hatfariant;
    }

    public void setHatfariant(Boolean hatfariant) {
        this.hatfariant = hatfariant;
    }

    public Lemma getStdlemId() {
        return stdlemId;
    }

    public void setStdlemId(Lemma stdlemId) {
        this.stdlemId = stdlemId;
    }

    public List<Paradigma> getParadigmas() {
        return paradigmas;
    }

    public void setParadigmas(List<Paradigma> paradigmas) {
        this.paradigmas = paradigmas;
    }

    public List<HolLemma> getHolLemmas() {
        return holLemmas;
    }

    public void setHolLemmas(List<HolLemma> holLemmas) {
        this.holLemmas = holLemmas;
    }

    public String getSuperlemmaLink() {
        return superlemmaLink;
    }

    public void setSuperlemmaLink(String superlemmaLink) {
        this.superlemmaLink = superlemmaLink;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Lemma{" + "foarm=" + foarm + ", homnum=" + homnum + ", id=" + getId() + '}';
    }

    public List<Synoniem> getSynoniemen() {
        return synoniemen;
    }

    public void setSynoniemen(List<Synoniem> synoniemen) {
        this.synoniemen = synoniemen;
    }

    public List<LemmaPart> getParts() {
        return parts;
    }

    public void setParts(List<LemmaPart> parts) {
        this.parts = parts;
    }

}
