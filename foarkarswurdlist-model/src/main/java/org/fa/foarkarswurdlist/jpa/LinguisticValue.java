/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "linguistic_value")
@NamedQuery(name = "LinguisticValue.byCatAndValue",
        query = "select lv from LinguisticValue lv where categoryId.category = :category and value = :value")
@Access(AccessType.FIELD)
public class LinguisticValue extends AbstractEntity {

    @Serial
    private static final long serialVersionUID = 1L;
    @NotNull
    @Column(name = "value")
    private String value;
    @NotNull
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private LinguisticCategory categoryId;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinguisticCategory getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(LinguisticCategory categoryId) {
        this.categoryId = categoryId;
    }
    @Override
    public String toString() {
        return "LinguisticValue[ " + value + " ]";
    }
    
}
