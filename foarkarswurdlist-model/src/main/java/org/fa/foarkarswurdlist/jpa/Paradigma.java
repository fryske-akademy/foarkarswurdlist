/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.fa.foarkarswurdlist.jpa.listeners.ParadigmaListener;
import org.fa.tei.jaxb.facustomization.M;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author eduard
 */
@Entity
@Audited
@Table(name = "Paradigma")
@NamedQueries({
        @NamedQuery(name = "Paradigma.findAll", query = "SELECT p FROM Paradigma p"),
        @NamedQuery(name = "Paradigma.findByLemma", query = "SELECT p FROM Paradigma p where p.lemmaId=:lemma"),
        @NamedQuery(name = "Paradigma.findByFoarmAndLemma", query = "SELECT p FROM Paradigma p where p.foarm=:foarm and p.lemmaId=:lemma"),
        @NamedQuery(name = "Paradigma.findByFoarm", query = "SELECT p FROM Paradigma p where p.foarm=:foarm"),
        @NamedQuery(name = "Paradigma.likeSoart", query = "SELECT p FROM Paradigma p JOIN p.soarten so where"
                + "   so in (select s from Soartlist s where s.soartlabel like :label)"),
        @NamedQuery(name = "Paradigma.likeFoarm", query = "SELECT p FROM Paradigma p where p.foarm like :foarm"),
        @NamedQuery(
                name = Paradigma.IPAQUERY,
                query = "from Paradigma p where p.id > :id and p.lemmaId.soart!='ôfkoarting' order by p.id"
        ),
        @NamedQuery(
                name = Paradigma.IPAQUERYNULL,
                query = "from Paradigma p where p.id > :id and (p.pron is null or p.pron = '') and p.lemmaId.soart!='ôfkoarting' order by p.id"
        )
})
@EntityListeners(ParadigmaListener.class)
@Access(AccessType.FIELD)
public class Paradigma extends AbstractEntity {
    public static final String IPAQUERY = "emptyipa";
    public static final String IPAQUERYNULL = "emptyipaNull";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name = "foarm", nullable = false)
    @NotNull
    @NotEmpty
    private String foarm;
    @NotNull
    @NotEmpty
    @Column(name = "nodiacrits", nullable = false)
    private String nodiacrits;
    private String pron;
    @Column(name = "frekw")
    private Integer frekw;
    @Column(name = "opmerking")
    private String opmerking;
    @Column(name = "std")
    private Boolean std = false;
    @Column(name = "ofbrekking")
    private String ofbrekking;
    @Column(name = "ofbneisjoen")
    private Boolean ofbneisjoen = false;
    @JoinColumn(name = "lemma_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Lemma lemmaId;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(name = "Paradigma_soarten",
            joinColumns = {@JoinColumn(name = "paradigma_id")},
            inverseJoinColumns = {@JoinColumn(name = "soartlist_id")})
    private List<Soartlist> soarten = new ArrayList<>(0);
    @Column(length = 512)
    private String linguistics;


    public Paradigma() {
    }

    public String getFoarm() {
        return foarm;
    }

    public void setFoarm(String foarm) {
        this.foarm = foarm != null ? foarm.trim() : null;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public String getPron() {
        return pron;
    }

    public void setPron(String pron) {
        this.pron = pron;
    }

    public Integer getFrekw() {
        return frekw;
    }

    public void setFrekw(Integer frekw) {
        this.frekw = frekw;
    }

    public String getOpmerking() {
        return opmerking;
    }

    public void setOpmerking(String opmerking) {
        this.opmerking = opmerking;
    }

    public Boolean getStd() {
        return std;
    }

    public void setStd(Boolean std) {
        this.std = std;
    }

    public String getOfbrekking() {
        return ofbrekking;
    }

    public void setOfbrekking(String ofbrekking) {
        this.ofbrekking = ofbrekking;
    }

    public Boolean getOfbneisjoen() {
        return ofbneisjoen;
    }

    public void setOfbneisjoen(Boolean ofbneisjoen) {
        this.ofbneisjoen = ofbneisjoen;
    }

    public Lemma getLemmaId() {
        return lemmaId;
    }

    public void setLemmaId(Lemma lemmaId) {
        this.lemmaId = lemmaId;
    }

    public List<Soartlist> getSoarten() {
        return soarten;
    }

    public void setSoarten(List<Soartlist> soarten) {
        this.soarten = soarten;
    }

    public String getLinguistics() {
        return linguistics;
    }

    public void setLinguistics(String linguistics) {
        this.linguistics = linguistics;
    }

    @Transient
    public String showSoarten() {
        if (soarten == null || soarten.isEmpty()) {
            return "";
        }
        final StringBuilder srt = new StringBuilder();
        soarten.forEach((s) -> srt.append(s.getSoartlabel()).append(", "));
        return srt.substring(0, srt.length() - 2);
    }

    /**
     * Returns the split form or null
     *
     * @param form
     * @param prefix
     * @param linguistics
     * @return
     */
    @Transient
    public static String getSplitForm(String form, String prefix, String linguistics) {
        return isSplit(prefix) ?
                isClitic(linguistics) ? null :
                        isPerson(linguistics) ? form.substring(prefix.length()).replaceFirst("^-", "") + " " + prefix : null
                : null;
    }

    @Transient
    public static boolean isClitic(String linguistics) {
        return linguistics != null && linguistics.toLowerCase().contains(M.Clitic.class.getSimpleName().toLowerCase());
    }

    @Transient
    public boolean isClitic() {
        return isClitic(linguistics);
    }

    @Transient
    public static boolean isPerson(String linguistics) {
        return linguistics != null && linguistics.toLowerCase(Locale.ROOT).contains(M.Person.class.getSimpleName().toLowerCase(Locale.ROOT));
    }

    @Transient
    public static boolean isVerbNoun(String linguistics) {
        return linguistics != null && linguistics.toLowerCase(Locale.ROOT).contains(M.Verbform.class.getSimpleName().toLowerCase(Locale.ROOT) + "." + M.Verbform.ger.name());
    }

    @Transient
    public static boolean isSplit(String prefix) {
        return prefix != null && !prefix.isEmpty();
    }

    @Transient
    public boolean isSplit() {
        return isSplit(lemmaId == null ? null : lemmaId.getPrefix());
    }

    public String getSplitfoarm() {
        return lemmaId == null ? null : getSplitForm(foarm, lemmaId.getPrefix(), linguistics);
    }

    @Override
    public String toString() {
        return "Paradigma{" + "foarm=" + foarm + ", id=" + getId() + '}';
    }


}
