/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import jakarta.persistence.FetchType;
import jakarta.validation.constraints.NotNull;
import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.util.List;

import static org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm.BY_LEMMA;
import static org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm.BY_LEMMA_SENSITIVE;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "Lemma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = BY_LEMMA,
                query = " from LemmaParadigm p where p.publish=true and p.nodiacrits like :lemma and (:pos='' or p.pos=:pos)" +
                        " and (p.checked=true or :checked=false)"),
        @NamedQuery(name = BY_LEMMA_SENSITIVE,
                query = " from LemmaParadigm p where p.publish=true and p.form like :lemma and (:pos='' or p.pos=:pos)" +
                        " and (p.checked=true or :checked=false)")
})
public class LemmaParadigm extends AbstractEntity {
    public static final String BY_LEMMA = "LemmaParadigm.bylemma";
    public static final String BY_LEMMA_SENSITIVE = "LemmaParadigm.bylemma.sensitive";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(name="neisjoen",nullable = false)
    private Boolean checked =false;
    @Column(name = "standert")
    private Boolean preferred =false;
    @NotNull
    @Column
    private Boolean publish=false;
    @Column(name = "foarm",nullable = false)
    private String form;
    @Column(name = "nodiacrits",nullable = false)
    private String nodiacrits;
    @Column(name = "pos",nullable = false)
    private String pos;
    @Column(name = "betsj")
    private String meaning;
    @Column
    private String article;

    @Column(name = "prefix",nullable = false)
    private String prefix="";
    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REMOVE}, mappedBy = "lemma", fetch = FetchType.EAGER)
    private List<Paradigm> paradigms;

    public Boolean getPreferred() {
        return preferred;
    }

    public void setPreferred(Boolean standert) {
        this.preferred = standert;
    }

    public @NotNull Boolean getPublish() {
        return publish;
    }

    public void setPublish(@NotNull Boolean publish) {
        this.publish = publish;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String foarm) {
        this.form = foarm!=null?foarm.trim():null;
    }

    public List<Paradigm> getParadigms() {
        return paradigms;
    }

    public void setParadigms(List<Paradigm> paradigmas) {
        this.paradigms = paradigmas;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return "LemmaParadigm{" + "foarm=" + form + ", id=" + getId() + '}';
    }

}
