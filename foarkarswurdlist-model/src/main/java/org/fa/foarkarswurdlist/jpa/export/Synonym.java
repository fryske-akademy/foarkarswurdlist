package org.fa.foarkarswurdlist.jpa.export;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "synonym")
@IdClass(Synonym.class)
public class Synonym implements Serializable {

    @Id
    private String lemma;
    @Id
    private String synonym;
    @Id
    private String betekenis;
    @Id
    private String pos;

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getSynonym() {
        return synonym;
    }

    public void setSynonym(String synonym) {
        this.synonym = synonym;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getBetekenis() {
        return betekenis;
    }

    public void setBetekenis(String betekenis) {
        this.betekenis = betekenis;
    }
}
