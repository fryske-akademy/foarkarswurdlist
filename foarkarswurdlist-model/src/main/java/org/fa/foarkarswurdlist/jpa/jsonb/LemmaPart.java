/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jpa.jsonb;

import jakarta.json.bind.annotation.JsonbTransient;
import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import org.fryske_akademy.jpa.AbstractEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.io.Serial;

/**
 *
 * @author eduard
 */
@Entity
@Cacheable
@Table(name = "lemmapart")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = LemmaPart.BYPART,
                query = "select lp.lemma from LemmaPart lp where lp.lemma.publish=true and lp.nodiacrits like :part and (:pos='' or lp.pos=:pos)" +
                        " and (lp.lemma.checked=true or :checked=false )"
        ),
        @NamedQuery(
                name = LemmaPart.BYPARTSENSITIVE,
                query = "select lp.lemma from LemmaPart lp where lp.lemma.publish=true and lp.part like :part and (:pos='' or lp.pos=:pos)" +
                        " and (lp.lemma.checked=true or :checked=false )"
        ),
        @NamedQuery(
                name = LemmaPart.BYBASE,
                query = "select lp.lemma from LemmaPart lp where lp.lemma.publish=true and lp.base=:base and lp.nodiacrits like :part and (:pos='' or lp.pos=:pos)" +
                        " and (lp.lemma.checked=true or :checked=false )"
        ),
        @NamedQuery(
                name = LemmaPart.BYBASESENSITIVE,
                query = "select lp.lemma from LemmaPart lp where lp.lemma.publish=true and lp.base=:base and lp.part like :part and (:pos='' or lp.pos=:pos)" +
                        " and (lp.lemma.checked=true or :checked=false )"
        )
    }
)
public class LemmaPart extends AbstractEntity {
    public static final String BYPART = "byPart";
    public static final String BYPARTSENSITIVE = "byPartSensitive";
    public static final String BYBASE = "byBase";
    public static final String BYBASESENSITIVE = "byBaseSensitive";

    @Serial
    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String part;
    @Column(length = 20)
    private String pos;
    @NotNull
    @NotEmpty
    @Column(nullable = false)
    private String nodiacrits;
    @Column
    private Boolean base;
    @JsonbTransient
    @JoinColumn(name = "lemma_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Lemma lemma;

    public LemmaPart() {
    }
    public String getNodiacrits() {
        return nodiacrits;
    }

    public void setNodiacrits(String nodiacrits) {
        this.nodiacrits = nodiacrits;
    }
    public Lemma getLemma() {
        return lemma;
    }

    public void setLemma(Lemma lemmaId) {
        this.lemma = lemmaId;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public Boolean getBase() {
        return base;
    }

    public void setBase(Boolean base) {
        this.base = base;
    }

    @Override
    public String toString() {
        return "LemmaParts{" + "part=" + part + ", id=" + getId() + '}';
    }

    
}
