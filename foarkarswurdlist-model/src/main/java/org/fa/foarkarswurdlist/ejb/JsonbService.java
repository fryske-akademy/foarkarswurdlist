package org.fa.foarkarswurdlist.ejb;

import org.fa.foarkarswurdlist.jpa.export.Dutchism;
import org.fa.foarkarswurdlist.jpa.export.Form;
import org.fa.foarkarswurdlist.jpa.export.NonPreferred;
import org.fa.foarkarswurdlist.jpa.export.SpellChecker;
import org.fa.foarkarswurdlist.jpa.export.Synonym;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Variant;
import org.fa.foarkarswurdlist.paradigm.GenerateParadigm;
import org.fa.tei.jaxb.facustomization.Pc.Pos;
import org.fryske_akademy.services.CrudReadService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface JsonbService extends CrudReadService {

    class FrisianComparator<T> extends RuleBasedCollator {
        
        private final Function<T, String> converter;

        public FrisianComparator(String rules, Function<T, String> converter) throws ParseException {
            super(rules);
            this.converter=converter;
        }

        @Override
        public int compare(Object o1, Object o2) {
            return super.compare(converter.apply((T) o1), converter.apply((T) o2));
        }

    }

    String FRY_COLLATION = "< a < A < â < Â < ä < Ä < b < B < c < C < d < D < e < E < é < É < ê < Ê < ë < Ë < f < F < g < G < h < H < i,y < I,Y < ï < Ï < j < J < k < K < l < L < m < M < n < N < o < O < ô < Ô < ö < Ö < p < P < q < Q < r < R < s < S < t < T < u < U < ú < Ú < û < Û < ü < Ü < v < V < w < W < x < X < z < Z";

    String FRY_COLLATION_INSENSITIVE = "< a,A < â,Â < ä,Ä < b,B < c,C < d,D < e,E < é,É < ê,Ê < ë,Ë < f,F < g,G < h,H < i,y,I,Y < ï,Ï < j,J < k,K < l,L < m,M < n,N < o,O < ô,Ô < ö,Ö < p,P < q,Q < r,R < s,S < t,T < u,U < ú,Ú < û,Û < ü,Ü < v,V < w,W < x,X < z,Z";

    Stream<SpellChecker> exportSpelling();

    Stream<NonPreferred> exportNonPreferred();

    Stream<Form> exportForms();

    Stream<Dutchism> exportDutchisms();

    Stream<Synonym> exportSynonyms();

    /**
     * Search in paradigm, synonyms, variants and dutchisms, return lemmas
     * found. A next category is searched only when results are not found yet or when searchEverywhere is true.
     *
     * @param form
     * @param pos
     * @param sensitive
     * @param maxResults
     * @param searchEverywhere when true search in all categories, also when results are already found
     * @return
     */
    List<Lemma> findLemma(String form, Pos pos, boolean sensitive, Integer maxResults, boolean searchEverywhere);

    /**
     * Find lemmas by their part (sykje, aaisykje, ...)
     * @param lemmaPart
     * @param pos when provided only consider base lemmas with this part of speech
     * @param sensitive search case and diacrit sensitive or not
     * @param onlyBase search only in base parts (right hand side, sykje by aaisykje)
     * 
     * @return 
     */
    List<Lemma> lemmasForPart(String lemmaPart, Pos pos, boolean sensitive, Boolean onlyBase);

    List<LemmaParadigm> findParadigm(String lemma, Pos pos, boolean sensitive);

    List<org.fa.foarkarswurdlist.jpa.jsonb.Synonym> findSynonyms(String form, Pos pos, boolean sensitive);

    List<org.fa.foarkarswurdlist.jpa.jsonb.Dutchism> findDutchisms(String form, Pos pos, boolean sensitive);

    List<Variant> findVariants(String form, Pos pos, boolean sensitive);

    String getHyphenation(String form, boolean sensitive);

    String getIpa(String form, boolean sensitive);

    String getArticle(String form, boolean sensitive);

    String versionInfo() throws URISyntaxException, IOException;
    String IPCONSONANTS = "ɡŋ"+ GenerateParadigm.consonants;

    /**
     * return lemmas around the lemma argument in the sorted Frisian index, or the complete index when the argument is null or empty
     * @param lemma
     * @return
     */
    List<String> index(String lemma);

    /**
     * find forms ending with given ipa where, when the given ipa is a vowel, the character preceding the match is not a vowel.
     * @param ipa the ipa(s) to find forms for, max 4 supported
     * @param offset
     * @param max
     * @return 
     */
    IpaResults findFormsByIpa(int offset, int max, String... ipa);
    
    default <T> List<T> frisianSort(List<T> toSort, Function<T,String> converter, boolean sensitive) {
        if (toSort == null) {
            return null;
        }
        try {
            Comparator<Object> comp = sensitive ?
                    new FrisianComparator(FRY_COLLATION, converter) :
                    new FrisianComparator(FRY_COLLATION_INSENSITIVE, converter);
            return toSort.stream()
                    .sorted(comp)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (ParseException ex) {
            throw new IllegalStateException("illegal collation rules?", ex);
        }
    }

}
