/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import org.fryske_akademy.services.CrudWriteService;

/**
 *
 * @author eduard
 */
public interface NotFoundService extends CrudWriteService {

    void logNotFound(String word, String pos);

}
