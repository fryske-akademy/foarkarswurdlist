/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.NotFound;
import org.fryske_akademy.services.CrudWriteService;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.fa.foarkarswurdlist.jpa.Paradigma;

/**
 *
 * @author eduard
 */
public interface StdwCrudService extends CrudWriteService {

    List<NotFound> findNotFound();
    
    List<Lemma> findUnusedStandert();

    void copyHyphenation(Lemma lemma, String hyphenation);

    void updatePron(List<Paradigma> noPron, AtomicInteger PROGRESS, AtomicInteger LASTID, IpaService ipaService);
}
