package org.fa.foarkarswurdlist.ejb;

import java.util.List;

public record IpaResults(int offset, int max, int total, List<FormIpa> formIpas) {
}
