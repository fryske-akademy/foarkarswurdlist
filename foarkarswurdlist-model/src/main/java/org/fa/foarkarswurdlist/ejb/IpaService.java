/*
 * Copyright 2023 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.ejb;

import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.services.Auditing;

import java.util.List;

/**
 *
 * @author eduard
 */
public interface IpaService extends Auditing {

    /**
     * return a list of ipa for every input form, or an empty list when ipa cannot be generated for the input
     * @param forms
     * @return
     */
    List<String> toIpa(String... forms);

    /**
     * return a list of ipa for one form, lemma and pos should be provided because they may influence pronunciation
     * @param form
     * @param lemma
     * @param pos
     * @return
     */
    List<String> toIpa(String form, String lemma, Pc.Pos pos);

}
