/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.paradigm;

import org.fa.foarkarswurdlist.jpa.FACodeToUniversal;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Paradigma;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * @author eduard
 */
public interface GenerateParadigm {

    /**
     * NOTE! j can be a vowel in ij
     */
    // TODO deal with ij somehow
    String consonants = "bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ";

    /**
     * returns lemma without (zs)je or e for -je en -e tiidwurden.
     *
     * @param lemma
     * @return
     */
    static String getStam(String lemma) {
        if (lemma.endsWith("zje") || lemma.endsWith("sje")) {
            return lemma.substring(0, lemma.length() - 3);
        } else if (lemma.endsWith("je")) {
            return lemma.substring(0, lemma.length() - 2);
        } else if (lemma.endsWith("e")) {
            return lemma.substring(0, lemma.length() - 1);
        } else {
            throw new IllegalArgumentException(lemma + " does not end with (j)e");
        }
    }

    /**
     * returns lemma without je for -je tiidwurden, also for -zje en -sje.
     *
     * @param lemma
     * @return
     */
    static String getStamPlusZorS(String lemma) {
        if (lemma.endsWith("zje") || lemma.endsWith("sje") || lemma.endsWith("je")) {
            return lemma.substring(0, lemma.length() - 2);
        } else {
            throw new IllegalArgumentException(lemma + " does not end with (zs)je");
        }
    }

    static boolean isVocal(char c) {
        /*
         * TODO the j in ij is also a vocal
         */
        return consonants.indexOf(c) == -1;
    }

    /**
     * returns true when the character at a certain position from the end is a vocal. The position is one based, so for
     * the last character position should be 1.
     *
     * @param s
     * @param posFromEnd
     * @return
     */
    static boolean isVocal(String s, int posFromEnd) {
        return isVocal(s.charAt(s.length() - posFromEnd));
    }

    /**
     * returns true for consonant vocal consonant vocal at the end of a string or when
     * the third character from the end is an e. Wether the e is short or long depends on pronunciation.
     *
     * @param lemma
     * @return
     */
    static boolean longVocal(String lemma, boolean eIsLong) {
        final int length = lemma.length();
        boolean isLong = length > 3 &&
                !isVocal(lemma.charAt(length - 4)) &&
                isVocal(lemma.charAt(length - 3)) &&
                !isVocal(lemma.charAt(length - 2)) &&
                isVocal(lemma.charAt(length - 1));
        return isLong && lemma.charAt(length - 3) == 'e' ? eIsLong : isLong;
    }

    /**
     * generate paradigm for lemma's conforming standard spelling rules
     * @param lemma
     * @return
     */
    List<Form> generateAdjective(String lemma);

    /**
     * generate paradigm for lemma's conforming standard spelling rules
     * @param lemma
     * @return
     */
    List<Form> generateVerb(String lemma);

    /**
     * generate paradigm for lemma's conforming standard spelling rules
     * @param lemma
     * @return
     */
    List<Form> generateNoun(String lemma);

    class Form {

        private final String linguistics;
        private final String wordform;
        private final boolean standert;

        /**
         * @param wordform
         * @param linguistics a key as found in {@link FACodeToUniversal#getTranslations()
         *                    }
         * @param result
         */
        public Form(String wordform, String linguistics, List<Form> result) {
            this(wordform, linguistics, true, result);
        }

        /**
         * @param wordform
         * @param linguistics
         * @param standert
         * @param result
         */
        public Form(String wordform, String linguistics, boolean standert, List<Form> result) {
            String wf = wordform.startsWith("-") ? wordform.substring(1) : wordform;
            result.stream().filter(form -> form.getWordform().equals(wf))
                    .findFirst().ifPresent(form -> {
                throw new IllegalArgumentException(String.format("form (%s, %s) already present: %s", wf, linguistics, form));
            });

            new Scanner(linguistics).forEachRemaining((s) -> {
                if (!FACodeToUniversal.getTranslations().containsKey(s)) {
                    throw new IllegalArgumentException(String.format("%s is invalid, use one of: %s",
                            s, FACodeToUniversal.getTranslations().keySet()));
                }
            });
            this.linguistics = linguistics;
            this.wordform = wf;
            this.standert = standert;
            result.add(this);
        }

        public String getLinguistics() {
            return linguistics;
        }

        public String getWordform() {
            return wordform;
        }

        public boolean isStandert() {
            return standert;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Form form = (Form) o;
            return Objects.equals(wordform, form.wordform);
        }

        @Override
        public int hashCode() {
            return Objects.hash(wordform);
        }

        @Override
        public String toString() {
            return "Form{" +
                    "linguistics='" + linguistics + '\'' +
                    ", wordform='" + wordform + '\'' +
                    ", standert=" + standert +
                    '}';
        }
    }

    List<Paradigma> generateParadigma(Lemma lemma, List<Form> forms);

    List<Paradigma> generateParadigma(Lemma lemma, Lemma base);
}
