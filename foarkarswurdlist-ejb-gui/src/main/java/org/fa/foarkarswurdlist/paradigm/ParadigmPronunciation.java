/*
 * Copyright 2023 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.paradigm;

import jakarta.annotation.security.RolesAllowed;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.fa.foarkarswurdlist.ejb.IpaService;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;
import org.fa.foarkarswurdlist.ejb.StdwCrudService;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fryske_akademy.jpa.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eduard
 */
@Singleton
@Named
public class ParadigmPronunciation {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ParadigmPronunciation.class);
    
    @Inject
    @StdwCrudBean
    private IpaService ipaService;
    @Inject
    private transient StdwCrudService crudWriteService;    

    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        crudWriteService = (StdwCrudService) CDI.current().select(StdwCrudService.class);
    }
        
    private Future pronResult;

    private final AtomicInteger PROGRESS = new AtomicInteger();
    private final AtomicInteger LASTID = new AtomicInteger();
    private final AtomicBoolean STOP = new AtomicBoolean();

    private List<Paradigma> findNoPron(boolean onlyNew) {
        return ipaService.find(onlyNew?Paradigma.IPAQUERYNULL:Paradigma.IPAQUERY, 
                Param.one("id", LASTID.get()), 0, 1000, Paradigma.class);
    }
    
    @RolesAllowed("beheer")
    public synchronized void addPronToParadigmForm(boolean onlyNew) {
        if (pronResult==null||pronResult.isDone()) {
            pronResult = Executors.newSingleThreadExecutor().submit(() -> {
                PROGRESS.set(0);
                LASTID.set(0);
                STOP.set(false);
                List<Paradigma> noPron = ParadigmPronunciation.this.findNoPron(onlyNew);
                while (!STOP.get()&&!noPron.isEmpty()) {
                    /*
                    we could optimize here by calling toIpa with multiple forms
                    bit cumbersome though
                    */
                    crudWriteService.updatePron(noPron, PROGRESS, LASTID, ipaService);
                    noPron = ParadigmPronunciation.this.findNoPron(onlyNew);
                }
                LOGGER.info("end adding pron: %s processed %d".formatted(LocalDateTime.now(), PROGRESS.get()));
            });
            LOGGER.info("start adding pron: "+LocalDateTime.now());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("adding pronunciation started"));            
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,"already running", "already running, progress: %s".formatted(progress())));
        }
    }
    
    @RolesAllowed("beheer")
    public void stop() {
        STOP.set(true);
    }
    
    public String progress() {
        return pronResult!=null&&!pronResult.isDone() ?
                PROGRESS.get() + (STOP.get()?" stopping":"")
                : "not running";
    }
}
