/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.paradigm;

import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.ejb.IpaService;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fa.foarkarswurdlist.jpa.Soartlist;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;

/**
 * @author eduard
 */
@ApplicationScoped
public class GenerateParadigmImpl implements GenerateParadigm {
    /*
    met een query zoals deze:

    select p.foarm, string_agg(s.soartlabel,' ') from paradigma p, soartlist s
where p.lemma_id=69843 and p.std=true and s.id in (select soartlist_id from paradigma_soarten where paradigma_id=p.id) group by p.foarm;

    kun haal je ingevoerd paradigma op van bijv. onregelmatig werkwoorden
    met wat zoek en vervang prima om te zetten naar code voor bijvoorbeeld samenstellingen, zie beneden bij "*izze"
    */

    @Inject
    @StdwCrudBean
    private IpaService ipaService;

    @Override
    public List<Paradigma> generateParadigma(Lemma lemma, List<Form> forms) {
        List<Paradigma> pars = forms.stream().map(f -> toParadigma(lemma, f)
        ).collect(Collectors.toList());
        List<String> ipas = ipaService.toIpa(pars.stream().map(Paradigma::getFoarm).toArray(String[]::new));
        if (ipas.size()==forms.size()) {
            IntStream.range(0,pars.size()).forEach(i -> pars.get(i).setPron(ipas.get(i)));
        }
        return pars;
    }

    protected Paradigma toParadigma(Lemma lemma, Form form) {
        Paradigma p = new Paradigma();
        p.setLemmaId(lemma);
        String prefix = lemma.getPrefix() == null ? "" : lemma.getPrefix();
        String hyph = form.getWordform();
        if (p.isSplit()) {
            if (form.getLinguistics().contains("?PRO")) {
                p.setFoarm(form.getWordform().substring(prefix.length()) + " " + lemma.getPrefix());
                hyph = p.getFoarm();
            } else {
                p.setFoarm(form.getWordform());
                hyph = prefix + "." + p.getFoarm().substring(prefix.length());
            }
        } else {
            p.setFoarm(form.getWordform());
        }
        p.setOfbrekking(hyph);
        p.setStd(form.isStandert());
        List<Soartlist> soarten = new ArrayList<>(5);
        Scanner linguistics = new Scanner(form.getLinguistics());
        AtomicInteger i = new AtomicInteger(1);
        linguistics.forEachRemaining((label) -> soarten.addAll(ipaService.findDynamic(0, 1, null,
                new Param.Builder().add("soartlabel", "label" + i.getAndIncrement(), "=", label, false, false).build(),
                Soartlist.class)));
        p.setSoarten(soarten);
        return p;
    }

    @Override
    public List<Form> generateAdjective(String lemma) {
        boolean longVocal = GenerateParadigm.longVocal(lemma, false);
        List<Form> result = new ArrayList<>(11);
        String l = lemma.endsWith("f") ? lemma.substring(0, lemma.length() - 1) + "v" : lemma.endsWith("s") ? lemma.substring(0, lemma.length() - 1) + "z" : lemma;
        if (l.endsWith("yv")) {
            l = l.substring(0, l.length() - 2) + "iv";
        } else if (l.endsWith("ch")) {
            l = l.substring(0, l.length() - 2) + "g";
        }

        String doubleCons = doubleConsonantSingleVocal(l, longVocal);

        new Form(lemma, "nulfoarm nb-pred nb-attr bwg-nb" + (doubleCons.endsWith("e") ? " b-attr b-sels-it" : ""), result);

        if (!doubleCons.endsWith("e")) {
            new Form(doubleCons + "e", "b-attr b-sels-it", result);
        }
        new Form(doubleCons + (doubleCons.endsWith("e") ? "n" : "en"), "b-sels-mt b-sels-in emf", result);
        new Form(lemma + "s", "b-pgn", result);

        if (l.endsWith("r")) {
            l += "d";
        } else {
            l = doubleCons;
        }
        new Form(l + (l.endsWith("e") ? "r" : "er"), "komp komp-nb-pred komp-nb-attr", result);
        new Form(l + (l.endsWith("e") ? "re" : "ere"), "komp-b-attr komp-sels-it", result);
        new Form(l + (l.endsWith("e") ? "ren" : "eren"), "komp-sels-mt komp-sels-in", result);
        new Form(l + (l.endsWith("e") ? "rs" : "ers"), "komp-pgn", result);

        new Form(lemma + "st", "sup sup-nb-pred sup-nb-attr", result);
        new Form(lemma + "ste", "sup-nb-pred sup-b-attr sup-b-sels-it", result);
        new Form(lemma + "sten", "sup-b-sels-mt sup-b-sels", result);

        return result;
    }

    @Override
    public List<Form> generateVerb(String lemma) {
        List<Form> result = new ArrayList<>(15);
        boolean longVocal = GenerateParadigm.longVocal(lemma, true);
        if (lemma.endsWith("je")) {
            String stam = GenerateParadigm.getStam(lemma);
            String stamZjeSje = GenerateParadigm.getStamPlusZorS(lemma);
            String z = stamZjeSje.endsWith("z") ? "z" : "";
            stam += z;
            if (lemma.endsWith("sje") && GenerateParadigm.isVocal(stam.charAt(stam.length() - 1))) {
                stam += "s";
            }
            String doubleStam = doubleConsonantSingleVocal(stam, longVocal); // stam when followed by a vocal
            new Form(stamZjeSje + "je", "nulfoarm|=nf 1-it-nt 1-mt-nt 2-mt-nt 3-mt-nt hf", result);
            new Form(doubleStam + "est", "2-it-nt 2-it-nt|-PRO 2-it-dt 2-it-dt|-PRO", result);
            new Form(doubleStam + "esto", "2-it-nt?PRO 2-it-dt?PRO", result);
            new Form(doubleStam + "este", "2-it-nt?PRO 2-it-dt?PRO", false, result);
            new Form(doubleStam + "estû", "2-it-nt?PRO 2-it-dt?PRO", false, result);
            new Form(doubleStam + "et", "3-it-nt", result);
            new Form(doubleStam + "e", "1-it-dt 3-it-dt om", result);
            new Form(doubleStam + "en", "1-mt-dt 2-mt-dt 3-mt-dt", result);
            new Form(stamZjeSje + "jend", "fm fm-adj-nb", result);
            new Form(stamZjeSje + "jende", "fm fm-adj-b", result);
            new Form(lemma + "n", "df", result);
        } else if (lemma.endsWith("ste")) {
            String stam = lemma.substring(0, lemma.length() - 1);
            new Form(stam + "e", "nulfoarm|=nf 2-it-nt 1-mt-nt 2-mt-nt 3-mt-nt 1-it-dt 3-it-dt polite-nt", result);
            new Form(stam, "1-it-nt 2-it-nt 3-it-nt hf om 2-it-nt|-PRO", result);
            new Form(stam + "est", "2-it-dt 2-it-dt|-PRO", result);
            new Form(stam + "este", "2-it-dt", result);
            new Form(stam + "en", "1-mt-dt 2-mt-dt 3-mt-dt om df polite-dt", result);
            new Form(stam + "ene", "1-mt-dt 2-mt-dt 3-mt-dt polite-dt", result);
            new Form(stam + "end", "fm fm-adj-nb", result);
            new Form(stam + "ende", "fm fm-adj-b", result);
            new Form(stam + "o", "2-it-nt?PRO", result);
            new Form(stam + "û", "2-it-nt?PRO", result);
            new Form(stam + "esto", "2-it-dt?PRO", result);
            new Form(stam + "estû", "2-it-dt?PRO            ", result);
        } else if (lemma.endsWith("izze")) {
            String stam = lemma.substring(0, lemma.length() - 4);
            new Form(stam + "ei", "1-it-dt om 3-it-dt", result);
            new Form(stam + "ein", "om om-adj-nb", result);
            new Form(stam + "eine", "1-mt-dt 2-mt-dt 3-mt-dt om-adj-b polite-dt", result);
            new Form(stam + "eist", "2-it-nt 2-it-nt|-PRO 2-it-dt 2-it-dt|-PRO", result);
            new Form(stam + "eisto", "2-it-dt?PRO 2-it-nt?PRO", result);
            new Form(stam + "eit", "3-it-nt", result);
            new Form(stam + "is", "hf 1-it-nt", result);
            new Form(stam + "izze", "nulfoarm|=nf 1-mt-nt 2-mt-nt 3-mt-nt 3-mt-dt polite-nt", result);
            new Form(stam + "izzen", "df", result);
            new Form(stam + "izzend", "fm fm-adj-nb", result);
            new Form(stam + "izzende", "fm-adj-b fm            ", result);
        } else if (lemma.endsWith("e")) {
            boolean thirdIsSecClitIsFirstAndThirdDt = lemma.endsWith("tse");
            String stam = GenerateParadigm.getStam(lemma);
            String dt = "kgfvszchp".contains(stam.substring(stam.length() - 1)) ? "t" : "d";
            String singStam = singleConsonantDoubleVocal(stam, longVocal); // stam for first person and when followed by consonant
            String doubleStam = doubleConsonantSingleVocal(stam, longVocal); // stam when followed by a vocal
            String stamNoS = singStam.endsWith("s") ? singStam.substring(0, singStam.length() - 1) : singStam; // stam for pronoun drop and clitic
            new Form(singStam, "1-it-nt hf" + ((stam.endsWith("d") && dt.equals("d")) ? " om" : ""), result);
            new Form(stamNoS + "st", "2-it-nt 2-it-nt|-PRO" + (thirdIsSecClitIsFirstAndThirdDt ? " 3-it-nt" : ""), result);
            new Form(stamNoS + "sto", "2-it-nt?PRO", result);
            new Form(stamNoS + "ste", "2-it-nt?PRO"
                    + (thirdIsSecClitIsFirstAndThirdDt ? " 1-it-dt 3-it-dt" : ""), false, result);
            new Form(stamNoS + "stû", "2-it-nt?PRO", false, result);
            if (!thirdIsSecClitIsFirstAndThirdDt) {
                new Form(singStam + "t", "3-it-nt" + ((dt.equals("t")) ? " om" : ""), result);
            }

            new Form(doubleStam + "e", "nulfoarm|=nf 1-mt-nt 2-mt-nt 3-mt-nt"
                    + ((doubleStam + "e").equals(singStam + dt + "e") ? " 1-it-dt 3-it-dt" : ""), result);
            if (!(doubleStam + "e").equals(singStam + dt + "e")) {
                if (!thirdIsSecClitIsFirstAndThirdDt) {
                    new Form(singStam + dt + "e", "1-it-dt 3-it-dt", result);
                }
            }
            new Form(singStam + dt + "est", "2-it-dt 2-it-dt|-PRO", result);
            new Form(singStam + dt + "esto", "2-it-dt?PRO", result);
            new Form(singStam + dt + "en", "1-mt-dt 2-mt-dt 3-mt-dt"
                    + ((singStam + dt + "en").equals(lemma + "n") ? " df" : ""), result);
            if (dt.equals("d") && !stam.endsWith("d")) {
                new Form(singStam + (stam.endsWith("d") ? "" : dt), "om", result);
            }
            new Form(doubleStam + "end", "fm fm-adj-nb", result);
            new Form(doubleStam + "ende", "fm fm-adj-b", result);
            if (!(singStam + dt + "en").equals(lemma + "n")) {
                new Form(lemma + "n", "df", result);
            }
        }
        return result;
    }

    @Override
    public List<Form> generateNoun(String lemma) {
        List<Form> result = new ArrayList<>(3);
        boolean longVocal = GenerateParadigm.longVocal(lemma, false);
        new Form(lemma, "nulfoarm|=it", result);
        if (Character.isUpperCase(lemma.charAt(lemma.length() - 1))) {
            return result;
        }
        // plural
        if (lemma.endsWith("el") || lemma.endsWith("em") || lemma.endsWith("en") || lemma.endsWith("er")
                || lemma.endsWith("êl") || lemma.endsWith("êm") || lemma.endsWith("ên") || lemma.endsWith("êr")) {
            char before = lemma.charAt(lemma.length() - 3);
            if (!GenerateParadigm.isVocal(before)) {
                new Form(lemma + "s", "mt", result);
            } else {
                String l = doubleConsonantSingleVocal(lemma, longVocal);
                new Form(l + "en", "mt", result);
            }
        } else if (lemma.endsWith("a") || lemma.endsWith("o") || lemma.endsWith("u") || lemma.endsWith("tv")) {
            new Form(lemma + "'s", "mt", result);
        } else if (lemma.endsWith("je")) {
            new Form(lemma + "s", "mt", result);
        } else if (lemma.endsWith("ing")) {
            new Form(lemma + "s", "mt", false, result);
            new Form(lemma + "en", "mt", result);
        } else if (lemma.endsWith("e")) {
            new Form(lemma + "n", "mt", result);
        } else if (lemma.endsWith("i")) {
            new Form(lemma + "en", "mt", result);
        } else if (lemma.endsWith("ch") && !lemma.endsWith("sch")) {
            if (lemma.length() >= 4) {
                if (GenerateParadigm.isVocal(lemma.charAt(lemma.length() - 3))
                        && GenerateParadigm.isVocal(lemma.charAt(lemma.length() - 4))) {
                    if (lemma.charAt(lemma.length() - 4) == lemma.charAt(lemma.length() - 3)) {
                        new Form(lemma.substring(0, lemma.length() - 3) + "gen", "mt", result);
                    } else {
                        new Form(lemma.substring(0, lemma.length() - 2) + "gen", "mt", result);
                    }
                } else if (GenerateParadigm.isVocal(lemma.charAt(lemma.length() - 3))) {
                    new Form(lemma.substring(0, lemma.length() - 2) + "ggen", "mt", result);
                } else {
                    new Form(lemma.substring(0, lemma.length() - 2) + "gen", "mt", result);
                }
            }
        } else if (lemma.endsWith("heid")) {
            new Form(lemma.substring(0, lemma.length() - 2) + "den", "mt", result);
        } else if (lemma.endsWith("ee")) {
            new Form(lemma.substring(0, lemma.length() - 2) + "ën", "mt", result);
        } else if (GenerateParadigm.isVocal(lemma, 1)) {
            new Form(lemma + "n", "mt", result);
        } else {
            String l = doubleConsonantSingleVocal(lemma, longVocal);
            new Form(l + "en", "mt", result);
        }
        boolean je = lemma.endsWith("je");
        // singular
        if (lemma.endsWith("e")) {
            lemma = lemma.substring(0, lemma.length() - 1);
        }
        String singLemma = singleConsonantDoubleVocal(lemma, longVocal);
        if (lemma.endsWith("l") || lemma.endsWith("n")) {
            new Form(singLemma + "tsje", "fw-it", result);
            new Form(singLemma + "tsjes", "fw-mt", result);
        } else if (lemma.endsWith("t") || lemma.endsWith("d")) {
            new Form(singLemma + "sje", "fw-it", result);
            new Form(singLemma + "sjes", "fw-mt", result);
        } else if (lemma.endsWith("ng")) {
            new Form(lemma.substring(0, lemma.length() - 1) + "kje", "fw-it", result);
            new Form(lemma.substring(0, lemma.length() - 1) + "kjes", "fw-mt", result);
        } else if (lemma.endsWith("g")) {
            new Form(singLemma.substring(0, singLemma.length() - 1) + "chje", "fw-it", result);
            new Form(singLemma.substring(0, singLemma.length() - 1) + "chjes", "fw-mt", result);
        } else if (lemma.endsWith("k") || lemma.endsWith("ch")) {
            new Form(singLemma + "je", "fw-it", result);
            new Form(singLemma + "jes", "fw-mt", result);
        } else if (lemma.endsWith("dze")) {
            new Form(lemma.substring(0, lemma.length() - 3) + "dske", "fw-it", result);
            new Form(lemma.substring(0, lemma.length() - 3) + "dskes", "fw-mt", result);
        } else if (lemma.endsWith("tv")) {
            new Form(lemma + "'ke", "fw-it", result);
            new Form(lemma + "'kes", "fw-mt", result);
        } else if (!je) {
            new Form(singLemma + "ke", "fw-it", result);
            new Form(singLemma + "kes", "fw-mt", result);
        }

        return result;
    }

    /**
     * This method should be called when the stam (root) of a word will get a
     * suffix starting with a vocal. A last f will become v, an s will become z.
     * A before last ú will become u, the last consonant will be duplicated,
     * except when the stam ends with lik, v, z or g or when the before last
     * character is â ê ô û. Finally two equal vocals followed by a consonant at
     * the end will be deduplicated.
     *
     * @param stam the stam (root) of a lemma, used to construct forms in the
     * paradigm of that lemma.
     * @return
     */
    public String doubleConsonantSingleVocal(String stam, boolean longVocal) {
        String l = stam.endsWith("f") ? stam.substring(0, stam.length() - 1) + "v" : stam.endsWith("s") && longVocal ? stam.substring(0, stam.length() - 1) + "z" : stam;
        if (l.length() > 1) {
            char beforeLast = l.charAt(l.length() - 2);
            boolean noDoubleCons = beforeLast == 'ê' || beforeLast == 'â' || beforeLast == 'û' || beforeLast == 'ô'
                    || l.endsWith("lik") || l.endsWith("v") || l.endsWith("z") || l.endsWith("g")
                    || l.endsWith("er") || l.endsWith("el") || l.endsWith("ij");
            boolean mayDoubleCons = !longVocal && !noDoubleCons && !GenerateParadigm.isVocal(l, 1)
                    && GenerateParadigm.isVocal(l, 2)
                    && (l.length() == 2 || !GenerateParadigm.isVocal(l, 3));
            if (beforeLast == 'ú' && mayDoubleCons) {
                l = l.substring(0, l.length() - 2) + "u" + l.substring(l.length() - 1);
            } else if (beforeLast == 'y' && mayDoubleCons) {
                l = l.substring(0, l.length() - 2) + "i" + l.substring(l.length() - 1);
            } else if (mayDoubleCons) {
                // verdubbelen laatste medeklinker
                l = l + l.charAt(l.length() - 1);
            }
            if (l.length() > 2 && !GenerateParadigm.isVocal(l, 1)) {
                char voorLaatste = l.charAt(l.length() - 2);
                char tweeVoorLaatste = l.charAt(l.length() - 3);
                if (voorLaatste == tweeVoorLaatste && GenerateParadigm.isVocal(voorLaatste)) {
                    // ontdubbelen klinkers
                    l = l.substring(0, l.length() - 3) + l.substring(l.length() - 2);
                }
            }
        }
        return l;
    }

    /**
     * When the before last character is a long vocal (as denoted in the
     * argument) enclosed in consonants, it will be doubled, except for â ê ô û
     * that will never be doubled and ú that will become u and i that will
     * become y (except for "lik"). A long vocal can be recognized when the
     * original lemma ends with consonant vocal consonant vocal, the last
     * consonant should be stripped before calling this function
     *
     * @param adaptedStam the adapted stam (root) of a lemma, used to construct
     * forms in the paradigm of that lemma.
     * @return
     */
    public String doubleVocal(String adaptedStam, boolean longVocal) {
        if (adaptedStam.length() < 2) {
            return adaptedStam;
        }
        char beforeLast = adaptedStam.charAt(adaptedStam.length() - 2);
        boolean nodup = beforeLast == 'ê' || beforeLast == 'â' || beforeLast == 'û' || beforeLast == 'ô' || beforeLast == 'y'
                || adaptedStam.endsWith("lik") || adaptedStam.endsWith("v") || adaptedStam.endsWith("z");
        boolean mayDuplicate = longVocal && !nodup && !GenerateParadigm.isVocal(adaptedStam, 1)
                && GenerateParadigm.isVocal(beforeLast)
                && (adaptedStam.length() == 2 || !GenerateParadigm.isVocal(adaptedStam, 3));
        if (beforeLast == 'ú' && mayDuplicate) {
            adaptedStam = adaptedStam.substring(0, adaptedStam.length() - 2) + "u" + adaptedStam.substring(adaptedStam.length() - 1);
        } else if (beforeLast == 'i' && mayDuplicate) {
            adaptedStam = adaptedStam.substring(0, adaptedStam.length() - 2) + "y" + adaptedStam.substring(adaptedStam.length() - 1);
        } else if (mayDuplicate) {
            adaptedStam = adaptedStam.substring(0, adaptedStam.length() - 2) + beforeLast + adaptedStam.substring(adaptedStam.length() - 2);
        }
        return adaptedStam;
    }

    /**
     * This method should be called when the stam (root) of a word will get a
     * suffix starting with a consonant. A double consonant at the end will be
     * dedoubled, after this {@link #doubleVocal(String,boolean)} will be called
     * when the vocal is long. A last z will become s, a last v will become f.
     *
     * @param stam the stam (root) of a lemma, used to construct forms in the
     * paradigm of that lemma.
     * @return
     */
    public String singleConsonantDoubleVocal(String stam, boolean longVocal) {
        String stamMinus1 = stam.substring(0, stam.length() - 1);
        if (stam.length() > 1 && !GenerateParadigm.isVocal(stam, 1)) {
            if (stam.charAt(stam.length() - 1) == stam.charAt(stam.length() - 2)) {
                // ontdubbelen laatste medeklinker
                stam = stamMinus1;
            }
            if (longVocal) {
                stam = doubleVocal(stam, true);
            }
        }
        return stam.endsWith("z") ? stamMinus1 + "s" : stam.endsWith("v") ? stamMinus1 + "f" : stam;
    }

    @Override
    public List<Paradigma> generateParadigma(Lemma lemma, Lemma base) {
        List<Paradigma> paradigmas = ipaService.find("Paradigma.findByLemma", Param.one("lemma", base), 0, null, Paradigma.class);
        if (!paradigmas.isEmpty()) {
            final StringBuilder prefix = new StringBuilder();
            if (lemma.getSoart().contains("haadtw")) {
                /*
                - prefix leeg, vorm eindigt met vorm uit base => prefix vorm - basevorm
                - prefix gevuld, vormen gelijk => vorm = prefix + vorm
                 */
                if (Util.nullOrEmpty(lemma.getPrefix()) && lemma.getFoarm().endsWith(base.getFoarm())) {
                    prefix.append(lemma.getFoarm().replace(base.getFoarm(), ""));
                    lemma.setPrefix(prefix.toString());
                } else if (!Util.nullOrEmpty(lemma.getPrefix()) && lemma.getFoarm().equals(base.getFoarm())) {
                    lemma.setFoarm(lemma.getPrefix() + lemma.getFoarm());
                }
            }
            if (lemma.getFoarm().endsWith(base.getFoarm())) {
                return paradigmas.stream().map((t) -> {
                    boolean split = lemma.getSoart().contains("haadtw") && t.getSoarten().stream().anyMatch((s) -> s.getSoartlabel().contains("1") || s.getSoartlabel().contains("2") || s.getSoartlabel().contains("3"));
                    Paradigma p = new Paradigma();
                    p.setLemmaId(lemma);
                    p.setFoarm(prefix + t.getFoarm());
                    p.setOfbrekking(prefix + "." + t.getOfbrekking());
                    p.setStd(t.getStd());
                    List<Soartlist> soarten = new ArrayList<>(5);
                    soarten.addAll(t.getSoarten());
                    p.setSoarten(soarten);
                    return p;
                }).collect(Collectors.toList());
            } else if (lemma.getSoart().contains("subst")) {
                return generateParadigma(lemma, generateNoun(lemma.getFoarm()));
            } else if (lemma.getSoart().contains("haadtw") || lemma.getSoart().contains("keppel")) {
                return generateParadigma(lemma, generateVerb(lemma.getFoarm()));
            } else if (lemma.getSoart().contains("adjektyf")) {
                return generateParadigma(lemma, generateAdjective(lemma.getFoarm()));
            }

        }
        return Collections.emptyList();
    }
}
