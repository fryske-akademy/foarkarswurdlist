/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import jakarta.annotation.security.RolesAllowed;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.NotFound;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.AbstractCrudServiceEnvers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author eduard
 */
@Local({StdwCrudService.class})
@Stateless
public class CrudServiceBean extends AbstractCrudServiceEnvers implements StdwCrudService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrudServiceBean.class);
    @PersistenceContext(unitName = "standertwurdlist_unit")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    @Override
    public List<Lemma> findUnusedStandert() {
        return find("Lemma.unusedStandert", null, 0, 7000, Lemma.class);
    }

    @RolesAllowed(EDITORROLE)
    @Transactional
    @Override
    public void copyHyphenation(Lemma lemma, String hyphenation) {
        stream("Paradigma.findByLemma",Param.one("lemma",lemma),0,-1,Paradigma.class)
                .forEach(p -> p.setOfbrekking(hyphenation));
    }

    @RolesAllowed(EDITORROLE)
    @Transactional
    @Override
    public List<NotFound> findNotFound() {
        List<NotFound> notFound = findAll(NotFound.class);
        for (Iterator<NotFound> it = notFound.iterator();it.hasNext();) {
            NotFound next = it.next();
            if (!findDynamic(0, 1, null, Param.one("foarm", next.getFoarm()), Lemma.class).isEmpty()) {
                it.remove();
                delete(next);
            }
        }
        return notFound;
    }
    
    @Override
    @Transactional
    public void updatePron(List<Paradigma> noPron, AtomicInteger PROGRESS, AtomicInteger LASTID, IpaService ipaService) {
        final List<Paradigma> toSave = new ArrayList<>(noPron.size());
        noPron.forEach(i -> {
            PROGRESS.incrementAndGet();
            LASTID.set(i.getId());
            String form = i.getFoarm();
            String lemma = i.getLemmaId().getFoarm();
            String pos = i.getLemmaId().getPos().toLowerCase().substring(4);
            String ipa = String.join(" ", ipaService.toIpa(form, lemma, Pc.Pos.valueOf(pos))).trim();
            if (!form.contains(" ") && ipa.contains(" ")) {
                LOGGER.warn(String.format("wrong ipa \"%s\" for \"%s\"", ipa,form));
            } else {
                if (!ipa.isEmpty()) {
                    if (!ipa.equals(i.getPron())) {
                        i.setPron(ipa);
                        toSave.add(i);
                    }
                } else {
                    LOGGER.warn("ipa failed for: " + form);
                }
            }
        });
        if (!toSave.isEmpty()) {
            batchSave(toSave, null);
        }
    }


}
