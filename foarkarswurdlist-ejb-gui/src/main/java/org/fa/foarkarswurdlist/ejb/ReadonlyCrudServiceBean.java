/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import com.vectorprint.RequestHelper;
import com.vectorprint.configuration.cdi.Property;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.services.AbstractCrudServiceEnvers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author eduardmm
 */
@Local({IpaService.class})
@Stateless
@StdwCrudBean
public class ReadonlyCrudServiceBean extends AbstractCrudServiceEnvers implements IpaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadonlyCrudServiceBean.class);

    private static final RequestHelper rh = new RequestHelper();

    @Inject
    @Property()
    private String g2pUrl;

    @Inject
    @Property()
    private String g2pPosUrl;

    @Inject
    @Property(defaultValue = "30")
    private int timeoutSeconds;

    @PersistenceContext(unitName = "standertwurdlist_unit_ro")
    private transient EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    private String enc(String s) {
        return s==null?null:URLEncoder.encode(s, StandardCharsets.UTF_8);
    }

    @Override
    public List<String> toIpa(String form, String lemma, Pc.Pos pos) {
        /*
"graphemic"	"phonemic"	"xsampa"
"leauwe"	"ljoˑu̯ə"	"xxxxxxxx"
         */
        final List<String> ipa = new ArrayList<>();
        try {
            
            HttpRequest.Builder RequestBuilder = HttpRequest.newBuilder()
                    .uri(URI.create(g2pPosUrl + "/" + enc(form) + "/" + enc(lemma) + "/" + pos.name()))
                    .setHeader("Content-Type", "text/plain").GET();

            scanIpa(RequestBuilder, ipa);
            if (LOGGER.isDebugEnabled()) {
                long numWords =  new Scanner(form.replaceAll(" +", " ")).tokens().count();
                if (numWords != ipa.size()) {
                    LOGGER.debug(String.format("ipa for %d words (%s) sought, %d found",
                            numWords,
                            form,
                             ipa.size()));
                }
            }
        } catch (NoSuchElementException | TimeoutException | ExecutionException | InterruptedException e) {
            LOGGER.warn(String.format("failure during fetching ipa for %s %s %s",form,lemma,pos.name()), e);
        }
        return ipa;
    }

    @Override
    public List<String> toIpa(String... words) {
/*
"graphemic"	"phonemic"	"xsampa"
"leauwe"	"ljoˑu̯ə"	"xxxxxxxx"
 */
        final List<String> ipa = new ArrayList<>();
        try {
            HttpRequest.Builder POST = HttpRequest.newBuilder()
                    .uri(URI.create(g2pUrl))
                    .setHeader("Content-Type", "text/plain")
                    .POST(HttpRequest.BodyPublishers.ofString(String.join(" ", words))
                    );
            scanIpa(POST, ipa);
            if (LOGGER.isDebugEnabled()) {
                long numWords = Arrays.stream(words).mapToLong(w ->
                        new Scanner(w.replaceAll(" +", " ")).tokens().count()).sum();
                if (numWords!=ipa.size()) {
                    LOGGER.debug(String.format("ipa for %d words (%s) sought, %d found",
                            numWords,
                            Arrays.asList(words)
                            ,
                            ipa.size()));
                }
            }
        } catch (NoSuchElementException | TimeoutException | ExecutionException | InterruptedException e) {
            LOGGER.warn("failure during fetching ipa",e);
        }
        return ipa;
    }

    private void scanIpa(HttpRequest.Builder RequestBuilder, final List<String> ipa) throws InterruptedException, ExecutionException, TimeoutException {
        String result = rh.request(RequestBuilder.build(), timeoutSeconds);
        Scanner scanner = new Scanner(result);
        if (scanner.hasNextLine()) {
            scanner.nextLine();
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                Scanner ipaS = new Scanner(s).useDelimiter("\t");
                ipaS.next();
                ipa.add(ipaS.next().replace("\"", ""));
            }
        }
    }
}
