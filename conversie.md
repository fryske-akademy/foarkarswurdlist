upgrade to 2007 format => gjin oplossing  
use unc path instead of drive letter looks promising but no, still problems

migration is the best option:

- move mdb to a safe place
- on windows and using access (readme for 2010 version)
    - prepare the database by showing system tables (file, options, current database, navigation options, show hidden objects and show system objects)
    - allow read access to MSysObjects, MSysQueries & MSysRelationships (file, users and permissions, user and group permissions)
    - prepare a odbc connection to this mdb
    - start mysql server
    - migrate access database using mysql workbench wizard
        - use prepared odbc and mysql connection
        - choose to create script and not to apply to db and adapt it
            - add auto_increment to primary key id columns
            - add "character set utf8 collate utf8_bin not null" to column foarm in Lemma, Paradigma, HolLemma, HolParadigma
            - not null for foarm columns and reference columns
            - unique indexes on foarm/lemma(id), foarm/hollemma
            - [EXECUTE RESULT SCRIPT](migration_script.sql)
        - apply script to mysql db (outside of wizard)
        - don't include table Morfo, change target schema name to standertwurdlist (look for these options in the following steps in the wizard)
        - for the rest all defaults
        - [EXECUTE VERSION SCRIPT](version.sql)
        - [EXECUTE LING SCRIPT](ling.sql)
- install glassfish from https://javaee.github.io/glassfish/download (tested using latest)
- create a jdbc connection pool and connect it to the database, add a property driverClass=com.mysql.jdbc.Driver
- create a jdbc resource using the pool
- [SEE GLASSISH README](glassfish.txt)
- install netbeans with base-ee and primefaces plugin and mysql driver
- register glassfish as server in netbeans
- create a new maven web project, use glassfish as server, java ee 7
- If you have your application with JPA ready you can:
    - put ``` <property name="hibernate.hbm2ddl.auto" value="update"/>``` in persistence.xml (or set this elsewhere
    - deploy your application
    - audit tables will be created for you
- If you already have your application your done now
- For all tables that become entities: alter table xyz add column version int(11) not null default 0;
- run the "entity classes from database", point to the jdbc resource, choose tables that should become entities, separate package for classes, no jaxb
- add @Version to version fields in entity classes
- run the "jsf pages from entity classes", separate packages (default jsf pages folder), choose primefaces template, check primefaces in components
- remove or make readonly id and version fields in pages
- further customize: inline editing of tables, (envers)auditing, search functionality, converters, large dropdowns
