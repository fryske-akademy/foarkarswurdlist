/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.CrudReadService;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Lemma.class, managed = true)
@Named
@ApplicationScoped
public class LemmaConverter extends AbstractConverter implements Converter<Lemma>, Serializable {

    public static Lemma findLemma(String foarm, String sense, CrudReadService crudReadService) {
        if (foarm == null) {
            return null;
        }
        if (foarm.matches("^[0-9]+$")) {
            return crudReadService.find(Integer.valueOf(foarm), Lemma.class);
        } else if (sense == null) {
            return crudReadService.findOne("Lemma.byFoarm", Param.one("foarm", foarm), Lemma.class);
        } else {
            return crudReadService.findOne("Lemma.byFoarmAndSense", new Param.Builder().add("foarm", foarm).add("sense", Short.valueOf(sense)).build(), Lemma.class);
        }
    }

    @Override
    public Lemma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return findLemma(Util.split(value, 0), Util.split(value, 1), crudReadService);
    }

    public static String getKey(Lemma lemma) {
        return lemma.getFoarm() + ((lemma.getHomnum() == null) ? "" : ": " + lemma.getHomnum());
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Lemma o) {
        if (o == null) {
            return null;
        }
        return getKey(o);
    }

}
