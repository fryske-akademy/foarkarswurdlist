package org.fa.foarkarswurdlist.jsf;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.LemmaPart;
import org.fa.foarkarswurdlist.jsf.converters.LemmaConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazyLemmaPart;
import org.fa.tei.jaxb.facustomization.Pc;

import java.util.List;

@SessionScoped
@Named
public class LemmaPartController extends StdwController<LemmaPart, LazyLemmaPart> {
    
    public static final List<Pc.Pos> POS = List.of(Pc.Pos.values());

    public List<Pc.Pos> getPOS() {
        return POS;
    }

    @Inject
    @Named("lemmaController")
    private LemmaController lemmaController;

    public String showLemma(LemmaPart l) {
        return filterAndRedirect(lemmaController.getFiltering(), "foarm",getSelected().getLemma().getFoarm(),"/lemma/List");
    }

    @Override
    protected void fillCopy(LemmaPart newEntity, LemmaPart selected) {
        newEntity.setLemma(selected.getLemma());
        newEntity.setPart(selected.getPart());
        newEntity.setPos(selected.getPos());
    }

    @Inject
    @Named("lemmaConverter")
    private LemmaConverter lemmaConverter;

    @Override
    protected void fillNew(LemmaPart entity) {
        if (getLazyModel().hasFilter("lemma")) {
            Lemma l = lemmaConverter.getAsObject(null, null, getFilterString("lemma"));
            entity.setLemma( l);
            entity.setPart(l.getFoarm());
            String pos = l.getPos();
            if (pos!=null) {
                entity.setPos(Pc.Pos.valueOf(pos.substring(pos.indexOf(".")+1).toLowerCase()));
            }
        }
    }

    @Override
    public List<String> complete(String query) {
        throw new UnsupportedOperationException("complete method not implemented");
    }


}
