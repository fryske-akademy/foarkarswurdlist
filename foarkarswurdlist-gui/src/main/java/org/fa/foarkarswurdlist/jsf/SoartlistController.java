package org.fa.foarkarswurdlist.jsf;

import org.fa.foarkarswurdlist.jpa.Soartlist;
import org.fa.foarkarswurdlist.jsf.lazy.LazySoartlist;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

@SessionScoped
@Named
public class SoartlistController extends StdwController<Soartlist, LazySoartlist> {

}
