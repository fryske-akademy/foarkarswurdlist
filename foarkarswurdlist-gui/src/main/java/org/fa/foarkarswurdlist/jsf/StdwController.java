package org.fa.foarkarswurdlist.jsf;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;
import org.fa.foarkarswurdlist.ejb.StdwCrudService;
import org.fa.foarkarswurdlist.jpa.listeners.CrudAware;
import org.fryske_akademy.jpa.AbstractEntity;
import org.fryske_akademy.jsf.NewSupportingLazyController;
import org.fryske_akademy.jsf.lazy.NewSupportingLazyModel;
import org.fryske_akademy.services.Auditing;
import org.fryske_akademy.services.CrudReadService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;
import java.util.List;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.primefaces.event.RowEditEvent;

public abstract class StdwController<T extends AbstractEntity, R extends NewSupportingLazyModel<T>> extends NewSupportingLazyController<T, R> {

    @Inject
    private transient StdwCrudService crudWriteService;
    @Inject
    @StdwCrudBean
    private transient Auditing auditing;
    

    @Serial
    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        crudWriteService = CDI.current().select(StdwCrudService.class).get();
        auditing = CDI.current().select(Auditing.class, CrudAware.STDW_ANNOTATION).get();
    }

    @Override
    public CrudReadService getCrudReadService() {
        return auditing;
    }

    @Override
    public StdwCrudService getCrudWriteService() {
        return crudWriteService;
    }

    @Override
    public void destroy(T entity) {
        try {
            super.destroy(entity);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
    }

    @Override
    public T save(RowEditEvent event) {
        try {
            return super.save(event);
        } catch (Exception e) {
            // handled insuper
        }
        return (T) event.getObject();
    }

    public boolean isRememberTableState() {
        return super.isRememberTableState() && !FILTERING.equals(
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(STATE));
    }

    @Override
    protected String newRowOnLastPage(String table) {
        return "lastPage('" + table + "');scrollDown('.ui-layout-unit-content');";
    }

    @Override
    protected String newRowOnCurrentPage(String table) {
        return "";
    }

    @Override
    public String gotoPageContaining(T entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void fillCopy(T newEntity, T selected) {
    }

    @Override
    protected boolean useFilterMechanism() {
        return true;
    }

    @Override
    public List<String> complete(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void redirectToServletPath(String servletPath) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + servletPath);
    }
}
