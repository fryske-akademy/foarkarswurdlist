/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;


import com.vectorprint.StringConverter;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import org.fa.foarkarswurdlist.jpa.LemmaPart;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazyLemmaPart extends LazyStdw<LemmaPart> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        String v = String.valueOf(value);
        if (key.equals("lemma") && !OPERATOR.valueIsOperator(String.valueOf(value), builder.isSyntaxInValue())) {
            String foarm = Util.split(v, 0);
            String sense = Util.split(v, 1);
            builder.add(key + ".foarm", "stdfoarm", "like", foarm, isUseOr(),null);
            if (sense != null) {
                builder.add(key + ".homnum", "stdsense", OPERATOR.EQ.getToken(), sense, isUseOr(), StringConverter.SHORT_PARSER);
            }
        } else if (key.equals("pos") ) {
            builder.add("pos", Pc.Pos.valueOf(v));
        } else if (key.equals("base") ) {
            if (v != null && !"0".equals(v)) {
                builder.add(key, "1".equals(v), isUseOr());
            }
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }


}
