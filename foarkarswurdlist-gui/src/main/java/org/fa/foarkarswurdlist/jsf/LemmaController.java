package org.fa.foarkarswurdlist.jsf;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Lemma.SOART;
import org.fa.foarkarswurdlist.jpa.NotFound;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fa.foarkarswurdlist.jpa.Synoniem;
import org.fa.foarkarswurdlist.jsf.converters.LemmaConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazyLemma;
import org.fa.foarkarswurdlist.paradigm.GenerateParadigm;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.util.JsfUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fa.foarkarswurdlist.ejb.JsonbService;
import org.fryske_akademy.Util;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

@SessionScoped
@Named
public class LemmaController extends StdwController<Lemma, LazyLemma> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LemmaController.class.getName());

    @Inject
    @ManagedProperty("#{paradigmaController}")
    private ParadigmaController paradigmaController;

    @Inject
    @ManagedProperty("#{lemmaPartController}")
    private LemmaPartController lemmaPartController;

    @Inject
    @ManagedProperty("#{synoniemController}")
    private SynoniemController synoniemController;

    @Inject
    @ManagedProperty("#{holLemmaController}")
    private HolLemmaController holLemmaController;

    @Inject
    private GenerateParadigm generateParadigm;

    @EJB
    private JsonbService jsonbService;

    public List<String> getSoarten() {
        return SOART.DBVALUES;
    }

    private List<Lemma> unused;

    public List<Lemma> findUnusedStandert() {
        if (unused == null) {
            unused = getCrudWriteService().findUnusedStandert();
        }
        return unused;
    }

    public void refreshUnused() {
        unused = null;
    }

    private List<NotFound> notFound;

    public List<NotFound> findNotFound() {
        if (notFound == null) {
            notFound = getCrudWriteService().findNotFound();
        }
        return notFound;
    }

    public void refreshNotFound() {
        notFound = null;
    }

    @Override
    public Lemma create(Lemma l) {
        Lemma persist = super.create(l);
        try {
            if (copy != null) {
                Lemma c = copy;
                copy = null;
                List<Paradigma> generateParadigma = generateParadigm.generateParadigma(persist, c);
                getCrudWriteService().batchSave(generateParadigma, null);
                getCrudWriteService().update(persist);
                redirectToParadigm(persist);
            } else {
                generateParadigmNoHandle(persist);
            }
        } catch (Exception e) {
            JsfUtil.handleException(new RuntimeException(e), "failure generating paradigm");
        }
        return persist;
    }

    /**
     * Calls {@link #prepareCreate() } and {@link #executOnClient(java.lang.String)
     * }.
     *
     * @param table
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void editNew(String table, String foarm) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        prepareCreate();
        getNewEntity().setFoarm(foarm);
        executOnClient(table);
        refreshNotFound();
    }

    @Override
    protected void fillNew(Lemma entity) {
        copy = null;
        setSelected(entity);
    }

    private void redirectToParadigm(Lemma persist) throws IOException {
        paradigmaController.getLazyModel().clear().add("lemmaId", lemmaConverter.getAsString(null, null, persist));
        redirectToServletPath("/paradigma/List.xhtml?" + STATE + "=" + FILTERING);
    }

    private void generateParadigmNoHandle(Lemma lemma) throws IOException {
        if (lemma.getSoart().contains("subst")) {
            removeCurrentParadigm(lemma);
            generate(lemma, generateParadigm.generateNoun(lemma.getFoarm()));
        } else if (lemma.getSoart().contains("haadtw") || lemma.getSoart().contains("keppel")) {
            removeCurrentParadigm(lemma);
            generate(lemma, generateParadigm.generateVerb(lemma.getFoarm()));
        } else if (lemma.getSoart().contains("adjektyf")) {
            removeCurrentParadigm(lemma);
            generate(lemma, generateParadigm.generateAdjective(lemma.getFoarm()));
        }
    }

    public void generateParadigm(Lemma lemma) {
        try {
            generateParadigmNoHandle(lemma);
        } catch (Exception exception) {
            JsfUtil.handleException(new RuntimeException(exception), "");
        }
    }

    private void removeCurrentParadigm(Lemma lemma) {
        List<Paradigma> paradigmas = getCrudReadService().find("Paradigma.findByLemma", Param.one("lemma", lemma), 0, null, Paradigma.class);
        if (!paradigmas.isEmpty()) {
            getCrudWriteService().batchDelete(paradigmas, null);
        }
    }

    private void generate(Lemma lemma, List<GenerateParadigm.Form> forms) throws IOException {
        if (forms.isEmpty()) {
            throw new IllegalArgumentException("generating paradigm for " + lemma.getFoarm() + " not supported");
        }
        List<Paradigma> generateParadigma = generateParadigm.generateParadigma(lemma, forms);
        getCrudWriteService().batchSave(generateParadigma, null);
        redirectToParadigm(lemma);
    }

    public String showHollemmas(Lemma l) {
        return filterAndRedirect(holLemmaController.getFiltering(), "lemma", lemmaConverter.getAsString(null, null, l), "/holLemma/List");
    }

    public String showParadigmas(Lemma l) {
        return filterAndRedirect(paradigmaController.getFiltering(), "lemmaId", lemmaConverter.getAsString(null, null, l), "/paradigma/List");
    }

    public String showParts(Lemma l) {
        return filterAndRedirect(lemmaPartController.getFiltering(), "lemma", lemmaConverter.getAsString(null, null, l), "/parts/List");
    }

    public String showSynoniemen(Lemma l) {
        return filterAndRedirect(synoniemController.getFiltering(), "lemma", lemmaConverter.getAsString(null, null, l), "/synoniem/List");
    }

    private Lemma copy;

    @Override
    protected void fillCopy(Lemma newEntity, Lemma l) {
        copy = l;
        Lemma selected = getCrudReadService().find("Lemma.expand", Param.one("id", l.getId()), 0, 1, Lemma.class).get(0);
        selected = getCrudReadService().find("Lemma.expandSyns", Param.one("l", selected), 0, 1, Lemma.class).get(0);
        newEntity.setFoarm(l.getFoarm());
        newEntity.setSoart(l.getSoart());
        newEntity.setStandert(l.getStandert());
        newEntity.setStdlemId(l.getStdlemId());
        setSelected(newEntity);
        newEntity.setSynoniemen(new ArrayList<>(selected.getSynoniemen().size()));
        selected.getSynoniemen().forEach((p) -> {
            Synoniem np = new Synoniem();
            newEntity.getSynoniemen().add(np);
            np.setVorm(p.getVorm());
            np.setLemma(newEntity);
            np.setBetekenis(p.getBetekenis());
            np.setOpmerking(p.getOpmerking());
        });

    }

    private String idxFilter;

    public String getIdxFilter() {
        return idxFilter;
    }

    public void setIdxFilter(String idxFilter) {
        this.idxFilter = idxFilter;
    }

    @PostConstruct
    private void init() {
        jsonbService.index(null);
    }

    private final LazyDataModel<String> idx = new LazyDataModel<String>() {
        @Override
        public int count(Map<String, FilterMeta> map) {
            return (int) jsonbService.index(null).stream().filter(s -> Util.nullOrEmpty(idxFilter) || s.contains(idxFilter)).count();
        }

        @Override
        public List<String> load(int i, int i1, Map<String, SortMeta> map, Map<String, FilterMeta> map1) {
            return jsonbService.index(null).stream().filter(s -> Util.nullOrEmpty(idxFilter) || s.contains(idxFilter)).skip(i).limit(i1).toList();
        }
    };

    public LazyDataModel<String> index() {
        return idx;
    }

    @Inject
    @Named("lemmaConverter")
    private LemmaConverter lemmaConverter;

    @Override
    public List<String> complete(String query) {
        return getCrudReadService().stream("Lemma.likeFoarm", Param.one("foarm", query), null, null, Lemma.class)
                .map((h) -> lemmaConverter.getAsString(null, null, h)).toList();
    }

}
