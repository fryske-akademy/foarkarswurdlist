/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Paradigma.class, managed = true)
@ApplicationScoped
@Named
public class ParadigmaConverter extends AbstractConverter implements Converter<Paradigma>, Serializable {

    @Override
    public Paradigma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Lemma lemma = LemmaConverter.findLemma(Util.split(value, 1), Util.split(value, 2), crudReadService);

        if (lemma == null) {
            return crudReadService.findOne("Paradigma.findByFoarm", Param.one("foarm", Util.split(value, 0)), Paradigma.class);
        } else {
            return crudReadService.findOne("Paradigma.findByFoarmAndLemma", new Param.Builder()
                    .add("foarm", Util.split(value, 0))
                    .add("lemma", lemma)
                    .build(), Paradigma.class);

        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Paradigma o) {
        if (o == null) {
            return null;
        }
        return o.getFoarm() + ((o.getLemmaId() == null) ? null : ": " + LemmaConverter.getKey(o.getLemmaId()));
    }

}
