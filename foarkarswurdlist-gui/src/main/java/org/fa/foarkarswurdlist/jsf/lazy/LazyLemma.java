/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import com.vectorprint.StringConverter;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author eduard
 */
@Named
@SessionScoped
public class LazyLemma extends LazyStdw<Lemma> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        String v = String.valueOf(value);
        if (key.equals("stdlemId") && !OPERATOR.valueIsOperator(v, builder.isSyntaxInValue())) {
            String foarm = Util.split(v, 0);
            String sense = Util.split(v, 1);
            builder.add(key + ".foarm", "stdfoarm", "like", foarm, isUseOr(),null);
            if (sense != null) {
                builder.add(key + ".homnum", "stdsense", "=", sense, isUseOr(), StringConverter.SHORT_PARSER);
            }
        } else if (key.equals("frekw")||key.equals("id")) {
            builder.add(key, v, StringConverter.INT_PARSER);
        } else if (key.equals("soart")) {
            super.addToParamBuilder(builder, key, Util.wrapStringInWildCards(v,isSyntaxInValue()));
            String s = v.toUpperCase();
            builder.setGroupStartsLastParam(1)
                    .add("pos", Util.wrapStringInWildCards(s.startsWith("POS.")?s.substring(s.indexOf("POS.")+1):s, true), isSyntaxInValue())
                    .setGroupEndsLastParam(1);
        } else if (key.equals("neisjoen") || key.equals("hatfariant") || key.equals("standert") || key.equals("aksept") || key.equals("publish")) {
            if (v != null && !"0".equals(v)) {
                builder.add(key, "1".equals(v), isUseOr());
            }
        } else if (key.equals("homnum")) {
            builder.add(key, v, isUseOr(),StringConverter.SHORT_PARSER);
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }

}
