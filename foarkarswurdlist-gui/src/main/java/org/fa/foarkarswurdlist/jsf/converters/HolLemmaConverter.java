/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.HolLemma;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = HolLemma.class,managed = true)
@ApplicationScoped
@Named
public class HolLemmaConverter extends AbstractConverter implements Converter<HolLemma>, Serializable {

    
    @Override
    public HolLemma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Lemma lemma = LemmaConverter.findLemma(Util.split(value, 1), Util.split(value, 2),crudReadService);
        if (lemma==null) {
            return crudReadService.findOne("HolLemma.findByFoarm",Param.one("foarm", Util.split(value, 0)),HolLemma.class);
        } else {
            return crudReadService.findOne("HolLemma.findByFoarmAndLemma",new Param.Builder()
                    .add("foarm",  Util.split(value, 0))
                    .add("lemma", lemma)
                    .build(),HolLemma.class);
                    
        }
    }


    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, HolLemma o) {
        if (o == null) {
            return null;
        }
        return o.getFoarm() + ((o.getLemma() == null) ? "" : ": " + LemmaConverter.getKey(o.getLemma()));
    }
    
}
