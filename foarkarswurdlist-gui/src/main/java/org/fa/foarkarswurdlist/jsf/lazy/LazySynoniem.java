/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import com.vectorprint.StringConverter;
import org.fa.foarkarswurdlist.jpa.Synoniem;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazySynoniem extends LazyStdw<Synoniem> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        String v = String.valueOf(value);
        if (key.equals("lemma") && !OPERATOR.valueIsOperator(v, builder.isSyntaxInValue())) {
            String foarm = Util.split(v, 0);
            String sense = Util.split(v, 1);
            builder.add(key + ".foarm", "foarm", "like", foarm, isUseOr(), null);
            if (sense != null) {
                builder.add(key + ".homnum", "stdsense", "=", sense, isUseOr(), StringConverter.SHORT_PARSER);
            }
        } else if (key.equals("ok")) {
            if (v != null && !"0".equals(v)) {
                builder.add(key, "1".equals(v), isUseOr());
            }
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }
}
