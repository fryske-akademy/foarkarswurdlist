/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.Soartlist;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Soartlist.class, managed = true)
@ApplicationScoped
@Named
public class SoartlistConverter extends AbstractConverter implements Converter<Soartlist>, Serializable {

    @Override
    @NotNull
    public Soartlist getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return crudReadService.findOne("Soartlist.byLabel", Param.one("label", "label", value.replace("&#x3D;","="), false, null, true), Soartlist.class);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Soartlist o) {
        if (o == null) {
            return null;
        }
        return o.getSoartlabel();
    }

}
