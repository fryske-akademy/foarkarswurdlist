package org.fa.foarkarswurdlist.jsf;

import jakarta.persistence.Transient;
import org.fa.foarkarswurdlist.jpa.FACodeToUniversal;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fa.foarkarswurdlist.jpa.Soartlist;
import org.fa.foarkarswurdlist.jsf.converters.LemmaConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazyParadigma;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@SessionScoped
@Named
public class ParadigmaController extends StdwController<Paradigma, LazyParadigma> {

    @Inject
    @Named("lemmaController")
    private LemmaController lemmaController;

    private DualListModel<Soartlist> pickSoarten;

    public DualListModel<Soartlist> getPickSoarten() {
        return pickSoarten;
    }
    
    public void setPickSoarten(DualListModel<Soartlist> pickSoarten) {
        this.pickSoarten = pickSoarten;
    }
    
    public void onTransfer(TransferEvent event) {
        if (event.isAdd()) {
            getSelected().getSoarten().addAll((Collection<? extends Soartlist>) event.getItems());
        } else {
            getSelected().getSoarten().removeAll(event.getItems());
        }
        setSelected(update(getSelected()));
    }

    public boolean filterLinguistics(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? "" : filter.toString().trim();
        return filterText.length() > 2;
    }

    public void setSelected(Paradigma selected) {
        super.setSelected(selected);
        pickSoarten = null;
        if (selected != null) {
            List<Soartlist> all = getCrudReadService().findAll(Soartlist.class);
            List<Soartlist> par = selected.getSoarten();
            pickSoarten = new DualListModel<>(all.stream().filter(s -> !par.contains(s)).sorted().collect(Collectors.toList()), par);
        }
    }

    public String showLemma(Paradigma l) {
        return filterAndRedirect(lemmaController.getFiltering(), "foarm",l.getLemmaId().getFoarm(),"/lemma/List");
    }

    @Override
    protected void fillCopy(Paradigma newEntity, Paradigma selected) {
        newEntity.setLemmaId(selected.getLemmaId());
        newEntity.setFoarm(selected.getFoarm());
        newEntity.setOfbrekking(selected.getOfbrekking());
    }

    @Inject
    @Named("lemmaConverter")
    private LemmaConverter lemmaConverter;

    @Override
    protected void fillNew(Paradigma entity) {
        if (getLazyModel().hasFilter("lemmaId")) {
            entity.setLemmaId( lemmaConverter.getAsObject(null, null, getFilterString("lemmaId")));
            entity.setFoarm(entity.getLemmaId().getFoarm());
            entity.setOfbrekking(entity.getLemmaId().getFoarm());
        }
    }

    @Override
    public List<String> complete(String query) {
        throw new UnsupportedOperationException("complete method not implemented");
    }
    
    @Override
    public Paradigma create(Paradigma e) {
        Paradigma h = super.create(e);
        getLazyModel().getFilters().put("foarm", h.getFoarm());
        return h;
    }

    public void copyHyphenation(Lemma lemma, String hyphenation) {
        getCrudWriteService().copyHyphenation(lemma,hyphenation);
    }

    /**
     * @see FACodeToUniversal#shortUniversalCode(String)
     */
    @Transient
    public Collection<String> showUniversal(Paradigma paradigma) {
        List<Soartlist> soarten = paradigma.getSoarten();
        if (soarten==null||soarten.isEmpty()) {
            return Collections.emptyList();
        }
        return soarten.stream().map(Soartlist::getUniversalString).collect(Collectors.toList());
    }

}
