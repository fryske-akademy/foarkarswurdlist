package org.fa.foarkarswurdlist.jsf;

import org.fa.foarkarswurdlist.jpa.Synoniem;
import org.fa.foarkarswurdlist.jsf.converters.LemmaConverter;
import org.fa.foarkarswurdlist.jsf.converters.SynoniemConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazySynoniem;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;

@SessionScoped
@Named
public class SynoniemController extends StdwController<Synoniem, LazySynoniem> {

    private Boolean del;

    public Boolean getDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }

    public void jaNee(Synoniem s) {
        if (del) {
            destroy(s);
        } else {
            s.setOk(true);
            update(s);
        }
        del = null;
    }
    
    @Override
    protected void fillCopy(Synoniem newEntity, Synoniem selected) {
        newEntity.setVorm(selected.getVorm());
        newEntity.setLemma(selected.getLemma());
    }

    @Override
    protected void fillNew(Synoniem entity) {
        if (getFiltering().hasFilter("lemma")) {
            entity.setLemma(lemmaConverter.getAsObject(null, null, getFilterString("lemma")));
        }
    }

    @Inject
    @Named("lemmaConverter")
    private LemmaConverter lemmaConverter;

    @Inject
    @Named("synoniemConverter")
    private SynoniemConverter converter;

    @Override
    public List<String> complete(String query) {
        return getCrudReadService().stream("Synoniem.likeVorm", Param.one("vorm", query), null, null, Synoniem.class)
                .map((h) -> converter.getAsString(null, null, h)).toList();
    }

    @Inject
    @Named("lemmaController")
    private LemmaController lemmaController;

    public String showLemma(Synoniem l) {
        return filterAndRedirect(lemmaController.getFiltering(), "foarm",getSelected().getLemma().getFoarm(),"/lemma/List");
    }

}
