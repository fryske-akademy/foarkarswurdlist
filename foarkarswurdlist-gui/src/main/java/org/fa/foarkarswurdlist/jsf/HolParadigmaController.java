package org.fa.foarkarswurdlist.jsf;

import org.fa.foarkarswurdlist.jpa.HolParadigma;
import org.fa.foarkarswurdlist.jpa.Soartlist;
import org.fa.foarkarswurdlist.jsf.converters.HolLemmaConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazyHolParadigma;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;

@SessionScoped
@Named
public class HolParadigmaController extends StdwController<HolParadigma, LazyHolParadigma> {

    @Override
    public HolParadigma create(HolParadigma e) {
        HolParadigma h = super.create(e);
        getLazyModel().getFilters().put("foarm", h.getFoarm());
        return h;
    }

    
    @Override
    protected void fillCopy(HolParadigma newEntity, HolParadigma selected) {
        newEntity.setFoarm(selected.getFoarm());
        newEntity.setHollemma(selected.getHollemma());
        newEntity.setSoart(selected.getSoart());
    }
    
    @Inject
    @Named("holLemmaConverter")
    private HolLemmaConverter holLemmaConverter;
    

    @Override
    protected void fillNew(HolParadigma entity) {
        if (getLazyModel().hasFilter("hollemma")) {
            entity.setHollemma( holLemmaConverter.getAsObject(null,null,getFilterString("hollemma")));
            entity.setSoart(getCrudReadService().findOne("Soartlist.byLabel", Param.one("label","nulfoarm"), Soartlist.class));
        }
    }

    @Override
    public List<String> complete(String query) {
        throw new UnsupportedOperationException("complete method not implemented");
    }

}
