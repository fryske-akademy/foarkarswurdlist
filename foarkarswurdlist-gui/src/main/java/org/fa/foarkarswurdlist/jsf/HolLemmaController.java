package org.fa.foarkarswurdlist.jsf;

import org.fa.foarkarswurdlist.jpa.HolLemma;
import org.fa.foarkarswurdlist.jpa.HolParadigma;
import org.fa.foarkarswurdlist.jsf.converters.HolLemmaConverter;
import org.fa.foarkarswurdlist.jsf.converters.LemmaConverter;
import org.fa.foarkarswurdlist.jsf.lazy.LazyHolLemma;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.util.JsfUtil;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.List;

@SessionScoped
@Named
public class HolLemmaController extends StdwController<HolLemma, LazyHolLemma> {

    @Inject
    @ManagedProperty("#{holParadigmaController}")
    private HolParadigmaController holParadigmaController;

    @Override
    public HolLemma create(HolLemma e) {
        HolLemma persist = super.create(e);
        try {
            holParadigmaController.getLazyModel().clear().add("hollemma", converter.getAsString(null, null, persist));
            holParadigmaController.prepareCreate();
            HolParadigma p = holParadigmaController.getNewEntity();
            p.setFoarm(persist.getFoarm());
            p.setHollemma(persist);
            holParadigmaController.create();
            holParadigmaController.getLazyModel().clear().add("hollemma", converter.getAsString(null, null, persist));
            redirectToServletPath("/holParadigma/List.xhtml?"+STATE + "=" + FILTERING);
        } catch (Exception ex) {
            JsfUtil.handleExceptionRethrow(new RuntimeException(ex), "paradigma niet gemaakt");
        }
        return persist;
    }

    public String showHolParadigmas(HolLemma l) {
        return filterAndRedirect(holParadigmaController.getFiltering(), "hollemma", converter.getAsString(null, null, l), "/holParadigma/List");
    }

    @Override
    protected void fillCopy(HolLemma newEntity, HolLemma selected) {
        newEntity.setFoarm(selected.getFoarm());
        newEntity.setLemma(selected.getLemma());
    }

    @Override
    protected void fillNew(HolLemma entity) {
        if (getFiltering().hasFilter("lemma")) {
            entity.setLemma(lemmaConverter.getAsObject(null, null, getFilterString("lemma")));
        }
    }

    @Inject
    @Named("lemmaConverter")
    private LemmaConverter lemmaConverter;
    
    @Inject
    @Named("holLemmaConverter")
    private HolLemmaConverter converter;

    @Override
    public List<String> complete(String query) {
        return getCrudReadService().stream("HolLemma.likeFoarm", Param.one("foarm", query), null, null, HolLemma.class)
                .map(h -> converter.getAsString(null, null, h)).toList();
    }

}
