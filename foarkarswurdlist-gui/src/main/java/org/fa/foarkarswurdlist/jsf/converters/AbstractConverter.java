/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;
import org.fa.foarkarswurdlist.jpa.listeners.CrudAware;
import org.fryske_akademy.services.Auditing;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author eduard
 */
public class AbstractConverter {
    
    @Inject
    @StdwCrudBean
    protected transient Auditing crudReadService;
    
    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        crudReadService = CDI.current().select(Auditing.class, CrudAware.STDW_ANNOTATION).get();
    }
}
