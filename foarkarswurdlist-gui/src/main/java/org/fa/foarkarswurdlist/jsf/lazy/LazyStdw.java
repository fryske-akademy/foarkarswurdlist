/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import org.fa.foarkarswurdlist.ejb.StdwCrudBean;
import org.fa.foarkarswurdlist.jpa.listeners.CrudAware;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.lazy.NewSupportingLazyModel;
import org.fryske_akademy.services.Auditing;
import org.fryske_akademy.services.CrudReadService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serial;

/**
 *
 * @author eduard
 */
public abstract class LazyStdw<T extends EntityInterface> extends NewSupportingLazyModel<T> {

    @Inject
    @StdwCrudBean
    private transient Auditing auditing;
    
    @Serial
    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        auditing = CDI.current().select(Auditing.class, CrudAware.STDW_ANNOTATION).get();
    }

    @Override
    public CrudReadService getCrudReadService() {
        return auditing;
    }

    @Override
    protected void addToParamBuilder(Param.Builder builder, String s, Object o) {
        builder.add(s,o,isUseOr());
    }

}
