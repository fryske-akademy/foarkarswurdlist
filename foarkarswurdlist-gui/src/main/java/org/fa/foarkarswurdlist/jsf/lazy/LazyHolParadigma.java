/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import com.vectorprint.StringConverter;
import org.fa.foarkarswurdlist.jpa.HolParadigma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazyHolParadigma extends LazyStdw<HolParadigma> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        if (key.equals("hollemma") && !OPERATOR.valueIsOperator(String.valueOf(value), builder.isSyntaxInValue())) {
            String foarm = Util.split((String) value, 0);
            String lfoarm = Util.split((String) value, 1);
            String lsense = Util.split((String) value, 2);
            builder.add(key + ".foarm", key, "like", foarm,isUseOr(),null);
            if (lfoarm != null) {
                builder.add(key + ".lemma.foarm", "foarm", "like", lfoarm, isUseOr(),null);
            }
            if (lsense != null) {
                builder.add(key + ".lemma.homnum", "sense", "=", lsense,isUseOr(),StringConverter.SHORT_PARSER);
            }
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }

}
