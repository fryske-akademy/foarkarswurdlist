/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.Synoniem;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Synoniem.class,managed = true)
@ApplicationScoped
@Named
public class SynoniemConverter extends AbstractConverter implements Converter<Synoniem>, Serializable {

    
    @Override
    public Synoniem getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Lemma lemma = LemmaConverter.findLemma(Util.split(value, 1), Util.split(value, 2),crudReadService);
        if (lemma==null) {
            return crudReadService.findOne("Synoniem.findByVorm",Param.one("vorm", Util.split(value, 0)),Synoniem.class);
        } else {
            return crudReadService.findOne("Synoniem.findByVormAndLemma",new Param.Builder()
                    .add("vorm",  Util.split(value, 0))
                    .add("lemma", lemma)
                    .build(),Synoniem.class);
                    
        }
    }


    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Synoniem o) {
        if (o == null) {
            return null;
        }
        return o.getVorm() + ((o.getLemma() == null) ? "" : ": " + LemmaConverter.getKey(o.getLemma()));
    }
    
}
