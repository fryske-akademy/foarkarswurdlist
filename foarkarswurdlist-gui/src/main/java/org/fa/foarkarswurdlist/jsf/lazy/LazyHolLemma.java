/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import com.vectorprint.StringConverter;
import org.fa.foarkarswurdlist.jpa.HolLemma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazyHolLemma extends LazyStdw<HolLemma> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        if (key.equals("lemma") && !OPERATOR.valueIsOperator(String.valueOf(value), builder.isSyntaxInValue())) {
            String foarm = Util.split((String) value, 0);
            String sense = Util.split((String) value, 1);
            builder.add(key + ".foarm", "foarm", "like", foarm, isUseOr(),null);
            if (sense != null) {
                builder.add(key + ".homnum", "sense", "=", sense,isUseOr(), StringConverter.SHORT_PARSER);
            }
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }

}
