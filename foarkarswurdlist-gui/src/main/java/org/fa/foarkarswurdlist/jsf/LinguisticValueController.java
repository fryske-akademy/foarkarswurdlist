package org.fa.foarkarswurdlist.jsf;

import org.fa.foarkarswurdlist.jpa.LinguisticValue;
import org.fa.foarkarswurdlist.jsf.lazy.LazyLinguisticValue;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

@SessionScoped
@Named
public class LinguisticValueController extends StdwController<LinguisticValue, LazyLinguisticValue> {

}
