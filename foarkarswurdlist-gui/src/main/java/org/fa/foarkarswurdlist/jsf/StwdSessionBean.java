package org.fa.foarkarswurdlist.jsf;

import org.fryske_akademy.jsf.SessionBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

@Named(value = "sessionBean")
@SessionScoped
public class StwdSessionBean extends SessionBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(StwdSessionBean.class);    

    @Override
    public void setCurrentTheme(String currentTheme) {
        if (currentTheme == null) return;
        try {
            super.setCurrentTheme(currentTheme);
        } catch (Exception e) {
            if (LOGGER.isWarnEnabled()) LOGGER.warn("failed to set theme", e);
            if (!"nova-light".equals(currentTheme)) {
                setCurrentTheme("nova-light");
                setCookie(THEME_COOKIE, "nova-light");
            }
        }
    }

    public void theme(String theme) {
        if (!getCurrentTheme().equals(theme)) {
            setCurrentTheme(theme);
            setCookie(THEME_COOKIE, getCurrentTheme());
        }
    }

    public void lang(String l) {
        if (!getLanguage().equals(l)) {
            setLanguage(l);
            setCookie(LANGUAGE_COOKIE, getLanguage());
        }
    }

    @Override
    public void setLanguage(String language) {
        if (language == null) return;
        try {
            super.setLanguage(language);
        } catch (Exception e) {
            if (LOGGER.isWarnEnabled()) LOGGER.warn("failed to set language", e);
            if (!"Dutch".equals(language)) {
                setLanguage("Dutch");
                setCookie(LANGUAGE_COOKIE, "Dutch");
            }
        }
    }

    public String versionInfo() throws URISyntaxException, IOException {
        URL resource = StwdSessionBean.class.getResource("/build.properties");
        return new String(Files.readAllBytes(Paths.get(resource.toURI()))).replace("\n", ", ");
    }
}
