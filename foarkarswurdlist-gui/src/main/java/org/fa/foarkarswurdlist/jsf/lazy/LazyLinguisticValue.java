/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import org.fa.foarkarswurdlist.jpa.LinguisticValue;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author eduard
 */
@Named
@SessionScoped
public class LazyLinguisticValue extends LazyStdw<LinguisticValue> {

}
