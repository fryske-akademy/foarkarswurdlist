/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.lazy;

import com.vectorprint.StringConverter;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.CrudReadService;
import org.primefaces.model.SortMeta;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import java.util.List;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazyParadigma extends LazyStdw<Paradigma> {

    @Override
    protected CrudReadService.SORTORDER.Builder convertSortMeta(List<SortMeta> multiSortMeta) {
        CrudReadService.SORTORDER.Builder sort = super.convertSortMeta(multiSortMeta);
        if (multiSortMeta==null || multiSortMeta.stream().noneMatch((s) -> s.getField().equals("id"))) {
            sort.add("id", CrudReadService.SORTORDER.ASC);
        }
        return sort;
    }

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        String v = String.valueOf(value);
        if (key.equals("lemmaId") && !OPERATOR.valueIsOperator(String.valueOf(value), builder.isSyntaxInValue())) {
            String foarm = Util.split(v, 0);
            String sense = Util.split(v, 1);
            builder.add(key + ".foarm", "stdfoarm", "like", foarm, isUseOr(),null);
            if (sense != null) {
                builder.add(key + ".homnum", "stdsense", OPERATOR.EQ.getToken(), sense, isUseOr(), StringConverter.SHORT_PARSER);
            }
        } else if (key.equals("splitfoarm")) {
            switch (v) {
                case "0":
                    break;
                case "1":
                    builder
                    .add("lemmaId.prefix","prefnull", OPERATOR.ISNOTNULL.getUserInput(), isUseOr())
                    .setGroupStartsLastParam(1)
                    .add("lemmaId.prefix","prefnull2", OPERATOR.ISNOTBLANK.getUserInput(), false)
                    .setGroupEndsLastParam(1)
                    .add("linguistics","ling","like", "%Person.%",isUseOr(),null);
                    break;
                case "2":
                    builder
                    .add("lemmaId.prefix","prefblank", OPERATOR.ISNULL.getUserInput(), isUseOr())
                    .setGroupStartsLastParam(1)
                    .add("lemmaId.prefix","prefblank2", OPERATOR.ISBLANK.getUserInput(), true)
                    .setGroupEndsLastParam(1);
                    break;
            }
        } else if (key.equals("frekw")) {
            builder.add(key, v, StringConverter.INT_PARSER);
        } else if (key.equals("linguistics")) {
            if (!v.contains(".") && !v.equalsIgnoreCase("abbr") && ! v.equalsIgnoreCase("islemma")) v = "." + v;
            List<Param> params = new Param.Builder(true, Param.Builder.DEFAULT_MAPPING, true).add(key, "*" + v + "*").build();
            builder.addParam(params);
        } else if (key.equals("std")||key.equals("ofbneisjoen")) {
            if (v != null && !"0".equals(v)) {
                builder.add(key, "1".equals(v), isUseOr());
            }
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }

}
