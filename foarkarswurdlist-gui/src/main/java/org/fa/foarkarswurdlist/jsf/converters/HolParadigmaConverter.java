/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.jsf.converters;

import org.fa.foarkarswurdlist.jpa.HolParadigma;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import java.io.Serializable;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = HolParadigma.class, managed = true)
@ApplicationScoped
@Named
public class HolParadigmaConverter extends AbstractConverter implements Converter<HolParadigma>, Serializable {

    @Override
    public HolParadigma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return crudReadService.find(getKey(value), HolParadigma.class);
    }

    java.lang.Integer getKey(String value) {
        return Integer.valueOf(value);
    }

    String getStringKey(java.lang.Integer value) {
        return String.valueOf(value);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, HolParadigma o) {
        if (o == null) {
            return null;
        }
        return getStringKey(o.getId());
    }

}
