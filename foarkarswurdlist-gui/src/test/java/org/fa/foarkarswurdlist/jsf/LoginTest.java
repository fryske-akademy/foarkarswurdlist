package org.fa.foarkarswurdlist.jsf;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = new HtmlUnitDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testLogin() {
        driver.get("http://localhost:8080/foarkarswurdlist/");
        if ("about:blank".equals(driver.getCurrentUrl())) {
            // server not running
            System.out.println("http://localhost:8080/foarkarswurdlist/ not up and running");
            return;
        }
//        assertTrue(driver.getCurrentUrl().startsWith("https://"));
//        assertTrue(isElementPresent(By.name("j_username")));
//        assertTrue(isElementPresent(By.name("j_password")));
//        driver.findElement(By.name("j_username")).click();
//        driver.findElement(By.name("j_username")).clear();
//        driver.findElement(By.name("j_username")).sendKeys("gast");
//        driver.findElement(By.name("j_password")).clear();
//        driver.findElement(By.name("j_password")).sendKeys("gast");
//        driver.findElement(By.xpath("//input[@type='submit']")).click();
//        assertTrue(isElementPresent(By.id("LemmaListForm:logout")));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

}
