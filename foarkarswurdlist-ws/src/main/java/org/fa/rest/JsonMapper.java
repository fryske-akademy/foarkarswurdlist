package org.fa.rest;

import org.fa.foarkarswurdlist.ejb.IpaResults;
import org.fa.foarkarswurdlist.jpa.Paradigma;
import org.fa.foarkarswurdlist.jpa.jsonb.Dutchism;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Synonym;

import jakarta.json.Json;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;
import org.fa.foarkarswurdlist.ejb.FormIpa;

public interface JsonMapper<T> {
    String LEMMA = "lemma";
    String POS = "pos";

    String MEANING = "meaning";
    JsonMapper<Synonym> synonymJsonMapper = (JsonObjectBuilder builder, Synonym s) -> builder
            .add(LEMMA, s.getLemma().getForm())
            .add(POS, s.getLemma().getPos())
            .add("synonym", s.getForm())
            .add("meaning", (s.getMeaning()==null?JsonValue.NULL:Json.createValue(s.getMeaning())))
            .build();
    JsonMapper<Dutchism> dutchismJsonMapper = (JsonObjectBuilder builder, Dutchism s) -> builder
            .add(LEMMA, s.getLemma().getForm())
            .add(POS, s.getLemma().getPos())
            .add("dutchism", s.getForm())
            .build();
    JsonMapper<Object[]> posMapper = (JsonObjectBuilder builder, Object[] o) -> builder
            .add(LEMMA, (String) o[0])
            .add(POS, (String) o[1])
            .build();
    String FORM = "form";
    JsonMapper<LemmaParadigm> paradigmMapper = (JsonObjectBuilder builder, LemmaParadigm o) -> {
        JsonArrayBuilder parArray = Json.createArrayBuilder();
        o.getParadigms().forEach(p -> {
            String splitForm = Paradigma.getSplitForm(
                    p.getForm(), o.getPrefix(), p.getLinguistics()
            );
            JsonObjectBuilder parBuilder = Json.createObjectBuilder();
            parBuilder
                    .add(FORM, Json.createValue(p.getForm()))
                    .add("splitform", (splitForm==null?JsonValue.NULL:Json.createValue(splitForm)))
                    .add("preferred", p.getPreferred())
                    .add("hyphenation", (p.getHyphenation()==null?JsonValue.NULL:Json.createValue(p.getHyphenation())))
                    .add("linguistics", (p.getLinguistics()==null?JsonValue.NULL:Json.createValue(p.getLinguistics())))
                    .add("ipa", (p.getIpa()==null?JsonValue.NULL:Json.createValue(p.getIpa())))
                    ;
            parArray.add(parBuilder.build());
        });
        return builder
                .add(LEMMA, Json.createValue(o.getForm()))
                .add(POS, Json.createValue(o.getPos()))
                .add(MEANING, (o.getMeaning()==null?JsonValue.NULL:Json.createValue(o.getMeaning())))
                .add("preferred", o.getPreferred())
                .add("article", (o.getArticle()==null?JsonValue.NULL:Json.createValue(o.getArticle())))
                .add("paradigm",parArray.build())
                .build();
    };
    JsonMapper<Lemma> lemmaMapper = (JsonObjectBuilder builder, Lemma o) -> builder
            .add(LEMMA, o.getForm())
            .add(POS, o.getPos())
            .add(MEANING, (o.getMeaning()==null?JsonValue.NULL:Json.createValue(o.getMeaning())))
            .build();
    String HYPHENATION = "hyphenation";
    JsonMapper<String> hyphMapper = (JsonObjectBuilder builder, String s) -> builder
            .add(HYPHENATION, s)
            .build();
    JsonMapper<Object[]> artMapper = (JsonObjectBuilder builder, Object[] o) -> builder
            .add(LEMMA, (String) o[0])
            .add("article", (o[1]==null?JsonValue.NULL:Json.createValue((String)o[1])))
            .build();
    JsonMapper<Object[]> variantMapper = (JsonObjectBuilder builder, Object[] o) -> builder
            .add(LEMMA, (String) o[2])
            .add(POS, (String) o[1])
            .add("variant", (o[0]==null?JsonValue.NULL:Json.createValue((String)o[0])))
            .build();

    String IPA = "ipa";
    JsonMapper<FormIpa> ipaMapper = (JsonObjectBuilder builder, FormIpa o) -> builder
            .add(FORM, o.getForm())
            .add(IPA, o.getIpa())
            .add(HYPHENATION,o.getHyphenation())
            .add(LEMMA, o.getLemma())
            .add(POS, o.getPos().name())
            .add("isVariant", o.isVariant())
            .build();
    JsonMapper<IpaResults> ipaResultMapper = (JsonObjectBuilder builder, IpaResults o) -> {
        JsonObjectBuilder jb = builder
                .add("offset", o.offset())
                .add("max", o.max())
                .add("total", o.total());
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        o.formIpas().stream().map(formIpa -> ipaMapper.map(builder,formIpa))
                .forEach(arrayBuilder::add);
        return jb.add(FORM+"s", arrayBuilder.build()).build();
    };

    JsonObject map(JsonObjectBuilder builder, T toMap);
}
