/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.rest.export;

import org.fa.foarkarswurdlist.ejb.JsonbService;
import org.fryske_akademy.Util;

import jakarta.ejb.EJB;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;

import java.nio.charset.StandardCharsets;

/**
 * @author eduard
 */
@Produces("text/tab-separated-values")
@Path("nonpreferred")
public class NonPreferredService {

    public static final String CRLF = "\r\n";

    @EJB
    private JsonbService jsonbService;

    @GET
    public Response standard() {
        StreamingOutput output = output1 -> {
            try {
                jsonbService.exportNonPreferred().forEach(nonPreferred -> {
                    try {
                        output1.write((
                                nonPreferred.getNonstandard() + "\t" +
                                nonPreferred.getStandard() + "\t" +
                                nonPreferred.getForm() + "\t" +
                                nonPreferred.getLinguistics() + CRLF).getBytes(StandardCharsets.UTF_8));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (Exception ex) {
                throw new WebApplicationException(
                        Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), fault(ex)).build());
            }
        };

        return Response.ok(output).build();
    }

    private String fault(Exception ex) {
        return Util.deepestCause(ex).getMessage();
    }

}
