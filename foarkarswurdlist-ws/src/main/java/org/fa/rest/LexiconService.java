/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.rest;

import com.vectorprint.configuration.cdi.Property;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonBuilderFactory;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.bind.JsonbConfig;
import jakarta.json.stream.JsonGenerator;
import jakarta.json.stream.JsonGeneratorFactory;
import jakarta.persistence.NoResultException;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;
import org.fa.foarkarswurdlist.ejb.JsonbService;
import org.fa.foarkarswurdlist.jpa.jsonb.Dutchism;
import org.fa.foarkarswurdlist.jpa.jsonb.Lemma;
import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Synonym;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author eduard
 */
@Stateless
@Path("lexicon")
@Produces("application/json")
public class LexiconService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LexiconService.class);

    private static final JsonbConfig jc = new JsonbConfig()
            .withStrictIJSON(true);

    private static final JsonGeneratorFactory FACTORY = Json.createGeneratorFactory(jc.getAsMap());

    private static final JsonBuilderFactory BUILDER_FACTORY = Json.createBuilderFactory(jc.getAsMap());

    @EJB
    private JsonbService jsonbService;

    @Inject
    @Property(defaultValue = "true")
    private boolean onlyChecked;

    @Inject
    @Property
    private boolean extendedLemmaSearch;

    @GET
    @Path("/info")
    public Response info() {
        Timestamp timestamp = jsonbService.findOne(Lemma.LASTCHANGED, null, Timestamp.class);
        String info;
        try {
            info = jsonbService.versionInfo() + ", data_last_changed=" + timestamp;
            JsonObjectBuilder builder = BUILDER_FACTORY.createObjectBuilder();
            new Scanner(info).useDelimiter(", ").forEachRemaining(
                    s -> {
                        String[] split = s.split("=");
                        builder.add(split[0], split[1]);
                    }
            );
            return Response.ok(builder.build()).build();
        } catch (URISyntaxException | IOException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), fault(e)).build());
        }
    }

    private StreamingOutput streamArray(String query, String paramValue) {
        return outputStream -> {
            List<Param> params = Param.one(JsonMapper.FORM, paramValue + "%");
            params.addAll(Param.one("checked", onlyChecked));
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                generator.writeStartArray();
                jsonbService.find(
                        query,
                        params, 0, 30, String.class).forEach(
                        generator::write
                        );
                generator.writeEnd();
            } catch (Exception ex) {
                handle(ex);
            }
        };

    }

    private void handle(Exception ex) {
        if (Util.deepestCause(ex) instanceof NoResultException) {
            throw new WebApplicationException(
                    Response.status(Response.Status.NOT_FOUND.getStatusCode(), LexiconService.this.fault(ex)).build());
        } else {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), LexiconService.this.fault(ex)).build());
        }
    }

    private StreamingOutput stream(String query, String paramName, String paramValue, Class outClass, JsonMapper mapper) {
        return stream(query, paramName, paramValue, "", outClass, mapper);
    }

    private StreamingOutput stream(String query, String paramName, String paramValue, String pos, Class outClass, JsonMapper mapper) {
        return outputStream -> {
            List<Param> params = new Param.Builder(false, Param.Builder.DEFAULT_MAPPING, false)
                    .add(paramName, paramValue)
                    .add("checked", onlyChecked ? Boolean.TRUE : Boolean.FALSE).build();
            if (pos != null) {
                params.addAll(Param.one(JsonMapper.POS, pos));
            }
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                generator.writeStartObject().writeStartArray("results");
                JsonObjectBuilder objectBuilder = BUILDER_FACTORY.createObjectBuilder();
                jsonbService.find(
                        query,
                        params, 0, -1, outClass)
                        .forEach(
                                o -> generator.write(mapper.map(objectBuilder, o))
                        );
                generator.writeEnd().writeEnd();
            } catch (Exception ex) {
                handle(ex);
            }
        };

    }

    @GET
    @Path("/autocomplete/{form}")
    public Response autocomplete(@NotNull @NotEmpty @PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput stream = streamArray(insensitive ? Paradigm.FORM : Paradigm.FORM_SENSITIVE,
                insensitive ? form.toLowerCase(Locale.ROOT) : form);
        return Response.ok(stream).build();
    }



    @GET
    @Path("/index")
    public Response index() {
        return Response.ok(getIndexOutput(null)).build();
    }

    @GET
    @Path("/index/{lemma}")
    public Response index(@NotEmpty @NotNull @PathParam(JsonMapper.LEMMA) String lemma) {
        return Response.ok(getIndexOutput(lemma)).build();
    }

    private StreamingOutput getIndexOutput(String lemma) {
        return output -> {
            try (JsonGenerator generator = FACTORY.createGenerator(output)) {
                generator.writeStartArray();
                jsonbService.index(lemma).forEach(generator::write);
                generator.writeEnd();
            } catch (Exception ex) {
                handle(ex);
            }
        };
    }

    @GET
    @Path("/synonyms/{form}")
    public Response findSynonyms(@NotNull @NotEmpty @PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput stream = stream(insensitive ? Synonym.BYFORM : Synonym.BY_FORM_SENSITIVE,
                JsonMapper.FORM, insensitive ? form.toLowerCase(Locale.ROOT) : form,
                Synonym.class, JsonMapper.synonymJsonMapper
        );
        return Response.ok(stream).build();
    }

    @GET
    @Path("/dutchisms/{form}")
    public Response findDutchsims(@PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput stream = stream(insensitive ? Dutchism.BYFORM : Dutchism.BY_FORM_SENSITIVE,
                JsonMapper.FORM, insensitive ? form.toLowerCase(Locale.ROOT) : form,
                Dutchism.class, JsonMapper.dutchismJsonMapper
        );
        return Response.ok(stream).build();
    }

    @GET
    @Path("/paradigm/{lemma}")
    public Response findParadigm(@PathParam("lemma") String lemma,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive,
            @DefaultValue("") @QueryParam("pos") String pos
    ) {
        StreamingOutput out = outputStream -> {
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                generator.writeStartObject().writeStartArray("results");
                JsonObjectBuilder objectBuilder = BUILDER_FACTORY.createObjectBuilder();
                jsonbService.findParadigm(lemma, pos.isEmpty() ? null : Pc.Pos.valueOf(pos), !insensitive)
                        .forEach(
                                o -> generator.write(JsonMapper.paradigmMapper.map(objectBuilder, o))
                        );
                generator.writeEnd().writeEnd();
            } catch (Exception ex) {
                handle(ex);
            }
        };
        return Response.ok(out).build();
    }

    @GET
    @Path("/variants/{lemma}")
    public Response findVariants(@PathParam(JsonMapper.LEMMA) String lemma,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput out = stream(
                insensitive ? Lemma.VARBY_LEMMA : Lemma.VARBY_LEMMA_SENSITIVE,
                JsonMapper.LEMMA,
                insensitive ? lemma.toLowerCase(Locale.ROOT) : lemma,
                Object[].class, JsonMapper.variantMapper);
        return Response.ok(out).build();
    }

    @GET
    @Path("/hyphenation/{form}")
    public Response findHyphenation(@PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput out = stream(
                insensitive ? Paradigm.HYPHBYFORM : Paradigm.HYPHBY_FORM_SENSITIVE,
                JsonMapper.FORM,
                insensitive ? form.toLowerCase(Locale.ROOT) : form,
                null,
                String.class,
                JsonMapper.hyphMapper);
        return Response.ok(out).build();
    }

    @GET
    @Path("/byipa/{ipa}")
    public Response findFormsByIpa(@PathParam("ipa") String ipa,
               @DefaultValue("0") @QueryParam("offset") int offset,
               @DefaultValue("30") @QueryParam("max") int max
               ) {
        StreamingOutput out = outputStream -> {
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                JsonObjectBuilder objectBuilder = BUILDER_FACTORY.createObjectBuilder();
                generator.write(
                        JsonMapper.ipaResultMapper.map(objectBuilder,
                            jsonbService.findFormsByIpa( offset, max, ipa))
                );
            } catch (Exception ex) {
                handle(ex);

            }
        };
        return Response.ok(out).build();
    }

//    @GET
//    @Path("/availablePos")
//    public Response findPos() {
//        StreamingOutput out = new StreamingOutput() {
//            @Override
//            public void write(OutputStream output) throws IOException, WebApplicationException {
//                JsonArray array = Json.createArrayBuilder(Arrays.asList(
//                        Arrays.stream(Pos.values()).map(p->p.name()).toArray())).build();
//                Json.createWriter(output).write(array);
//            }
//        };
//        return Response.ok(out).build();
//    }
    @GET
    @Path("/article/{lemma}")
    public Response findArticle(@PathParam(JsonMapper.LEMMA) String lemma,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput out = stream(
                insensitive ? Lemma.ARTICLE : Lemma.ARTICLE_SENSITIVE,
                JsonMapper.LEMMA,
                insensitive ? lemma.toLowerCase(Locale.ROOT) : lemma,
                null,
                Object[].class,
                JsonMapper.artMapper);
        return Response.ok(out).build();
    }

    @GET
    @Path("/pos/{form}")
    public Response findPos(@PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput out = stream(
                insensitive ? Lemma.POSBYFORM : Lemma.POSBY_FORM_SENSITIVE,
                JsonMapper.FORM,
                insensitive ? form.toLowerCase(Locale.ROOT) : form,
                Object[].class, JsonMapper.posMapper);
        return Response.ok(out).build();

    }

    @GET
    @Path("/lemma/{form}")
    public Response findLemma(@PathParam(JsonMapper.FORM) String form,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive
    ) {
        StreamingOutput out = outputStream -> {
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                generator.writeStartObject().writeStartArray("results");
                JsonObjectBuilder objectBuilder = BUILDER_FACTORY.createObjectBuilder();
                jsonbService.findLemma(form, null, !insensitive, null, extendedLemmaSearch).forEach(
                        o -> generator.write(JsonMapper.lemmaMapper.map(objectBuilder, o))
                );
                generator.writeEnd().writeEnd();

            } catch (Exception ex) {
                handle(ex);

            }
        };

        return Response.ok(out).build();
    }

    @GET
    @Path("/compositions/{part}")
    public Response findCompositions(@PathParam("part") String part,
            @DefaultValue("") @QueryParam("pos") String pos,
            @DefaultValue("true") @QueryParam("insensitive") boolean insensitive,
            @QueryParam("onlyBase") Boolean onlyBase
    ) {
        StreamingOutput out = outputStream -> {
            try (JsonGenerator generator = FACTORY.createGenerator(outputStream)) {
                generator.writeStartObject().writeStartArray("results");
                JsonObjectBuilder objectBuilder = BUILDER_FACTORY.createObjectBuilder();
                jsonbService.lemmasForPart(part, pos.isEmpty() ? null : Pc.Pos.valueOf(pos.toLowerCase()), insensitive, onlyBase).forEach(
                        o -> generator.write(JsonMapper.lemmaMapper.map(objectBuilder, o))
                );
                generator.writeEnd().writeEnd();

            } catch (Exception ex) {
                handle(ex);

            }
        };

        return Response.ok(out).build();
    }

    private String fault(Exception ex) {
        LOGGER.warn(Util.deepestCause(ex).getMessage(), Util.deepestCause(ex));
        return Util.deepestCause(ex).getMessage();
    }
}
