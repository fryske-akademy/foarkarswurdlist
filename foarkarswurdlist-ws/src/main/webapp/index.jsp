<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <title>Lexicon service Service</title>
    <style type="text/css">
        ul {line-height: 1.5}
    </style>
</head>
<body>
<h1>Fryske Akademy lexicon service</h1>
<a href="corpora_linguistics.html">documentation of used linguistic codes</a><br/><br/>
<p>
    Available paths:
    <ul>
        <li><a href="rest/lexicon/info">service info</a> </li>
        <li><a href="rest/lexicon/autocomplete/aai">autocomplete forms</a> </li>
        <li><a href="rest/lexicon/index/boek">show lemmas around a given lemma in the frisian index</a> </li>
        <li><a href="rest/lexicon/synonyms/aai">find synonyms for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/variants/aanst">find variants for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/dutchisms/efter">find dutchisms for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/paradigm/kinne">find the paradigm for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/byipa/ẽːstə">find forms ending with the input ipa</a> </li>
        <li><a href="rest/lexicon/pos/kinne">find the part of speech for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/hyphenation/kinsto">find the hyphenation for one or more forms</a> </li>
        <li><a href="rest/lexicon/article/hus">find the article for one or more lemmas</a> </li>
        <li><a href="rest/lexicon/lemma/kinne">find the lemma for one or more forms</a> </li>
        <li><a href="rest/lexicon/compositions/komme">find the composite lemmas for one or more parts</a>, use a query parameter onlyBase=true|false to only search parts that are the "base" part of a lemma</li>
    </ul>

    <ul>
        <li>only verified lemmas are searched</li>
        <li>Path parameters lemma and form support wildcards: * for multiple characters and _ for one.</li>
        <li>By default searches are case and diacritics insensitive</li>
        <li>You can use a query parameter insensitive=false to search case and diacritics sensitive</li>
    </ul>

</p>

<p>Contact: e drenth at fryske-akademy dot nl</p>
</body>
</html>
