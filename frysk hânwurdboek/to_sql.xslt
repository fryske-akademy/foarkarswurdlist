<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xs"
                version="3.0">
    
    <xsl:output method="text" encoding="UTF-8"/>
    <!-- select id from lemma where foarm = 'aadskulp' and soart in (select facode from code_convert where universalcode ilike 'Pos.noun%'); -->
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="apos2">''</xsl:variable>
    
    <xsl:template match="form[@type='synonym']">
        <xsl:value-of select="'insert into synoniem (version,vorm, betekenis,lemma_id) values (0,'"/>
        <xsl:text>'</xsl:text>
        <xsl:value-of select="replace(orth/text(),$apos,$apos2)"/>
        <xsl:text>','</xsl:text>
        <xsl:value-of select="replace(note/text(),$apos,$apos2)"/>
        <xsl:text>',(</xsl:text>
        <xsl:value-of select="'select id from lemma where neisjoen=true and foarm ='"/>
        <xsl:text>'</xsl:text>
        <xsl:value-of select="replace(parent::form/orth/text(),$apos,$apos2)"/>
        <xsl:text>'</xsl:text>
        <xsl:value-of select="' and soart in (select facode from code_convert where universalcode ilike '"/>
        <xsl:text>'Pos.</xsl:text>
        <xsl:value-of select="parent::form/@pos"/>
        <xsl:text>%'</xsl:text>
        <xsl:value-of select="')));'"/>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="text()"/>


</xsl:stylesheet>