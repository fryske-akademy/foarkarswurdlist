BEGIN {
    FS="\t"
}
{gsub(/'/, "''")}
{
    print "insert into synoniem (version, lemma_id, vorm, betekenis, opmerking) "
    print "  select 0, (select id from lemma where foarm='" $4 "' and pos='" $2 "'), vorm, betekenis, opmerking from synoniem "
    print "  where lemma_id = (select id from lemma where foarm='" $1 "' and pos='" $2 "');"
}