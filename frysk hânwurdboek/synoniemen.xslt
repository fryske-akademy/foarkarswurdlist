<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:local="http://syno"
    exclude-result-prefixes="xs"
    version="3.0">
    
    <xsl:output method="xml" indent="yes"/>
    <!-- sed -n '/\(.*\)\t\1\t/p' forms.csv|sed -n '/Islemma/s/^\([^\t]*\)\t.*Pos\.\([a-zA-Z]*\).*/\1,\L\2/p' > lemmas -->
    <xsl:variable name="lemmas" as="xs:string*" select="unparsed-text-lines('file:lemmas')"/>
    
    <xsl:function name="local:isWord" as="xs:boolean">
        <xsl:param name="word" as="xs:string"/>
        <xsl:sequence select='not(contains($word," ")) and matches($word,"[a-zA-Zéúâêôûà-]")'/>
    </xsl:function>

    <xsl:function name="local:pos" as="xs:string">
        <xsl:param name="pos" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="$pos='bynwurd'">cconj</xsl:when>
            <xsl:when test="$pos='bywurd'">adv</xsl:when>
            <xsl:when test="$pos='eigennamme'">propn</xsl:when>
            <xsl:when test="$pos='eigenskipswurd'">adj</xsl:when>
            <xsl:when test="$pos='ferhâldingswurd'">adp</xsl:when>
            <xsl:when test="$pos='foarnamwurd'">pron</xsl:when>
            <xsl:when test="$pos='gemeentenamme'">propn</xsl:when>
            <xsl:when test="$pos='haadwurd'">noun</xsl:when>
            <xsl:when test="$pos='lânnamme'">propn</xsl:when>
            <xsl:when test="$pos='lidwurd'">det</xsl:when>
            <xsl:when test="$pos='ôfkoarting'">abbr</xsl:when>
            <xsl:when test="$pos='plaknamme'">propn</xsl:when>
            <xsl:when test="$pos='provinsjenamme'">propn</xsl:when>
            <xsl:when test="$pos='telwurd'">num</xsl:when>
            <xsl:when test="$pos='tiidwurd'">verb</xsl:when>
            <xsl:when test="$pos='útropwurd'">intj</xsl:when>
            <xsl:when test="$pos='weromwurkjend tiidwurd'">verb</xsl:when>
            <xsl:when test="$pos='wrâlddielnamme'">propn</xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('unknown: ',$pos)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:template match="/">
        <TEI>
            <text>
                <body>
                    <xsl:apply-templates select="wb/art"/>
                </body>
            </text>
        </TEI>
    </xsl:template>

    <xsl:template match="art[count(lem)=1]">
        <xsl:choose>
            <xsl:when test="contains(replace(lem/kop/ws[1]/text(),',.*',''),'rheaksel')">
                <xsl:message>Skip: foar- of efterheaksel: <xsl:value-of select="lem/w/text()"/> </xsl:message>                                
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="lem" select="lem/w/text()[1]"/>
                <xsl:variable name="syns" as="item()*">
                    <xsl:apply-templates select="sem/ssem"/>            
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="not(empty($syns))">
                        <xsl:variable name="pos" select="local:pos(replace(lem/kop/ws[1]/text(),',.*',''))"/>
                        <xsl:variable name="fkw" select="count(index-of($lemmas,concat($lem,',',$pos)))"/>
                        <xsl:choose>
                            <xsl:when test="$pos='abbr'">
                                <xsl:message>Skip: abbreveation: <xsl:value-of select="concat($lem,',',$pos)"/> </xsl:message>
                            </xsl:when>
                            <xsl:when test="$fkw=1">
                                <entry>
                                    <form pos="{$pos}">
                                        <orth><xsl:value-of select="$lem"/> </orth>
                                        <xsl:copy-of select="$syns"/>
                                    </form>
                                </entry>
                            </xsl:when>
                            <xsl:when test="count(index-of($lemmas,concat($lem,',')))=1">
                                <!-- ok if one match without pos -->
                                <xsl:message>Pos in fhwb differs from fkw: <xsl:value-of select="$pos"/>, <xsl:value-of select="index-of($lemmas,concat($lem,','))"/></xsl:message>
                                <entry>
                                    <form pos="{$pos}">
                                        <orth><xsl:value-of select="$lem"/> </orth>
                                        <xsl:copy-of select="$syns"/>
                                    </form>
                                </entry>
                            </xsl:when>
                            <xsl:when test="$fkw=0">
                                <xsl:message>Not in fkw: <xsl:value-of select="concat($lem,',',$pos)"/> </xsl:message>                
                            </xsl:when>
                            <xsl:when test="$fkw>1">
                                <xsl:message>More in fkw: <xsl:value-of select="concat($lem,',',$pos)"/> </xsl:message>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>Skip: no synonyms found for: <xsl:value-of select="$lem"/> </xsl:message>                
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="ssem">
        <xsl:variable name="word" select="normalize-space(string-join(text()))"/>
        <!--
        <x> tussen haakjes in note, KAN GENESTE <i> bevatten
        <i> vaak voorbeelden, GEEN synoniemen!
        <a> bevat synoniem

        opmerking/uitleg:

        - Alle tekst onder <ssem> als er een <a> is
        -->
        <xsl:choose>
            <xsl:when test="local:isWord($word)">
                <form type="synonym">
                    <orth><xsl:value-of select="$word"/></orth>
                </form>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="tokenize($word,',')">
                    <xsl:if test="local:isWord(normalize-space(.))">
                        <form type="synonym">
                            <orth><xsl:value-of select="normalize-space(.)"/></orth>
                        </form>
                    </xsl:if>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="a"/>
    </xsl:template>

    <xsl:template match="a">
        <form type="synonym">
            <note>
                <xsl:choose>
                    <xsl:when test="../x">
                        <xsl:apply-templates select="..//text()"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="string-join(..//text()[not(parent::a)])"/></xsl:otherwise>
                </xsl:choose>
            </note>
            <orth><xsl:value-of select="normalize-space(text())"/></orth>
        </form>
    </xsl:template>

    <xsl:template match="text()[parent::ssem]"><xsl:value-of select="."/></xsl:template>
    <xsl:template match="text()[ancestor::x]"><xsl:value-of select="concat(' (',.,') ')"/></xsl:template>

    <xsl:template match="art[count(lem)>1]">
        <xsl:message>Skip: art with more lem: <xsl:value-of select="string-join(lem/w/text(),', ')"/> </xsl:message>
    </xsl:template>

</xsl:stylesheet>