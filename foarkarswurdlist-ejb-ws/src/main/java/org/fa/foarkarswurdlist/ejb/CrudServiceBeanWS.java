/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import com.vectorprint.configuration.cdi.Property;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaQuery;
import org.fa.foarkarswurdlist.jpa.export.Dutchism;
import org.fa.foarkarswurdlist.jpa.export.Form;
import org.fa.foarkarswurdlist.jpa.export.NonPreferred;
import org.fa.foarkarswurdlist.jpa.export.SpellChecker;
import org.fa.foarkarswurdlist.jpa.export.Synonym;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaParadigm;
import org.fa.foarkarswurdlist.jpa.jsonb.LemmaPart;
import org.fa.foarkarswurdlist.jpa.jsonb.Paradigm;
import org.fa.foarkarswurdlist.jpa.jsonb.Variant;
import org.fa.foarkarswurdlist.jpa.listeners.CrudAware;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.AbstractCrudService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.fa.tei.jaxb.facustomization.Pc;

/**
 * @author eduard
 */
@Local({JsonbService.class})
@Stateless
public class CrudServiceBeanWS extends AbstractCrudService implements JsonbService {


    @Property(required = false, defaultValue = "true")
    @Inject
    private Boolean cache;

    private EntityManagerFactory emf;

    @PersistenceContext(unitName = "standertwurdlist_unit_export")
    private EntityManager exportEntityManager;

    private static List<String> index;

    @PostConstruct
    private void init() {
        emf = cache ?
                Persistence.createEntityManagerFactory("standertwurdlist_unit_fast") :
                Persistence.createEntityManagerFactory("standertwurdlist_unit_fast", new HashMap() {{
                    put(
                            "jakarta.persistence.sharedCache.mode", "ENABLE_SELECTIVE");
                }});
        if (index==null||index.isEmpty()) {
            try {
                index = emf.createEntityManager().createQuery("select l.form from Lemma l")
                        .getResultStream()
                        .sorted(new FrisianComparator<>(JsonbService.FRY_COLLATION, (s) -> (String) s))
                        .toList();
            } catch (ParseException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    private Object getValue(String word, boolean sensitive) {
        return sensitive ? word :
                CrudAware.removeDiacrits(word).toLowerCase(Locale.ROOT);
    }

    @Inject
    private NotFoundService notfoundService;

    @Property(defaultValue = "false")
    @Inject
    private boolean logNotFound;

    @Override
    public Stream<SpellChecker> exportSpelling() {
        CriteriaQuery cq = exportEntityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SpellChecker.class));
        return exportEntityManager.createQuery(cq).getResultStream();
    }

    @Override
    public Stream<NonPreferred> exportNonPreferred() {
        CriteriaQuery cq = exportEntityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(NonPreferred.class));
        return exportEntityManager.createQuery(cq).getResultStream();
    }

    @Override
    public Stream<Form> exportForms() {
        CriteriaQuery cq = exportEntityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Form.class));
        return exportEntityManager.createQuery(cq).getResultStream();
    }

    @Override
    public Stream<Dutchism> exportDutchisms() {
        CriteriaQuery cq = exportEntityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Dutchism.class));
        return exportEntityManager.createQuery(cq).getResultStream();
    }

    @Override
    public Stream<Synonym> exportSynonyms() {
        CriteriaQuery cq = exportEntityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Synonym.class));
        return exportEntityManager.createQuery(cq).getResultStream();
    }

    @Override
    public List<org.fa.foarkarswurdlist.jpa.jsonb.Lemma> findLemma(String form, Pc.Pos pos, boolean sensitive, Integer maxResults, boolean searchEverywhere) {
        List<Param> params = getParams(form, pos, sensitive, "form", true);
        List<org.fa.foarkarswurdlist.jpa.jsonb.Lemma> lemmata
                = find(sensitive
                        ? org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.FULL_LEMMA_BY_FORM_SENSITIVE
                        : org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.FULL_LEMMA_BYFORM,
                        params, 0, maxResults == null ? -1 : maxResults, org.fa.foarkarswurdlist.jpa.jsonb.Lemma.class
                );
        if (searchEverywhere||lemmata.isEmpty()) {
            lemmata.addAll(find(sensitive
                    ? org.fa.foarkarswurdlist.jpa.jsonb.Lemma.BY_SYN_FORM_SENSITIVE
                    : org.fa.foarkarswurdlist.jpa.jsonb.Lemma.BY_SYN_FORM,
                    params, 0, maxResults == null ? -1 : maxResults, org.fa.foarkarswurdlist.jpa.jsonb.Lemma.class
            ));
        }
        if (searchEverywhere||lemmata.isEmpty()) {
            lemmata.addAll(find(sensitive
                    ? org.fa.foarkarswurdlist.jpa.jsonb.Lemma.BY_VAR_FORM_SENSITIVE
                    : org.fa.foarkarswurdlist.jpa.jsonb.Lemma.BY_VAR_FORM,
                    params, 0, maxResults == null ? -1 : maxResults, org.fa.foarkarswurdlist.jpa.jsonb.Lemma.class
            ));
        }
        if (searchEverywhere||lemmata.isEmpty()) {
            lemmata.addAll(find(sensitive
                        ? org.fa.foarkarswurdlist.jpa.jsonb.Dutchism.FULL_LEMMA_BY_FORM_SENSITIVE
                        : org.fa.foarkarswurdlist.jpa.jsonb.Dutchism.FULL_LEMMA_BYFORM,
                        params, 0, maxResults == null ? -1 : maxResults, org.fa.foarkarswurdlist.jpa.jsonb.Lemma.class
                ));
        }
        return lemmata;
    }

    public String versionInfo() throws URISyntaxException, IOException {
        URL resource = CrudServiceBeanWS.class.getResource("/build.properties");
        return new String(Files.readAllBytes(Paths.get(resource.toURI()))).replace("\n", ", ");
    }

    private static final int IDXCONTEXT = 50;

    @Override
    public List<String> index(String lemma) {
        if (Util.nullOrEmpty(lemma)) {
            return index;
        }
        int found = IntStream.range(0, index.size()).filter(i -> index.get(i).equals(lemma)).findFirst().orElse(-1);
        if (found != -1) {
            int start = found - IDXCONTEXT;
            int end = found + IDXCONTEXT;
            if (start < 0) {start=0;}
            if (end > index.size()) {end=index.size();}
            return IntStream.range(start, end).mapToObj(i -> index.get(i)).toList();
        }
        return Collections.emptyList();
    }

    @Override
    public List<LemmaParadigm> findParadigm(String lemma, Pc.Pos pos, boolean sensitive) {
        List<Param> params = getParams(lemma, pos, sensitive, "lemma", false);

        List<LemmaParadigm> rv = find(sensitive
                ? LemmaParadigm.BY_LEMMA_SENSITIVE : LemmaParadigm.BY_LEMMA,
                params, 0, -1, LemmaParadigm.class
        );
        if (logNotFound && rv.isEmpty()) {
            CompletableFuture.runAsync(() -> notfoundService.logNotFound(lemma, pos == null ? "" : pos.name()));
        }
        return rv;
    }

    @Override
    public List<org.fa.foarkarswurdlist.jpa.jsonb.Synonym> findSynonyms(String form, Pc.Pos pos, boolean sensitive) {
        List<Param> params = getParams(form, pos, sensitive, "form", false);

        return find(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Synonym.BY_FORM_SENSITIVE : org.fa.foarkarswurdlist.jpa.jsonb.Synonym.BYFORM,
                params, 0, -1, org.fa.foarkarswurdlist.jpa.jsonb.Synonym.class
        );
    }

    private List<Param> getParams(String form, Pc.Pos pos, boolean sensitive, String key, boolean wildcards) {

        return new Param.Builder(true, wildcards ? Param.Builder.DEFAULT_MAPPING : null, false)
                .add(key, getValue(form, sensitive))
                .add("checked", Boolean.TRUE)
                .add("pos", pos == null ? "" : "Pos." + pos.name().toUpperCase(Locale.ROOT)).build();
    }

    @Override
    public List<org.fa.foarkarswurdlist.jpa.jsonb.Dutchism> findDutchisms(String form, Pc.Pos pos, boolean sensitive) {
        List<Param> params = getParams(form, pos, sensitive, "form", false);

        return find(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Dutchism.BY_FORM_SENSITIVE : org.fa.foarkarswurdlist.jpa.jsonb.Dutchism.BYFORM,
                params, 0, -1, org.fa.foarkarswurdlist.jpa.jsonb.Dutchism.class
        );
    }

    @Override
    public List<Variant> findVariants(String lemma, Pc.Pos pos, boolean sensitive) {
        List<Param> params = getParams(lemma, pos, sensitive, "lemma", false);

        return stream(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Lemma.VARBY_LEMMA_SENSITIVE : org.fa.foarkarswurdlist.jpa.jsonb.Lemma.VARBY_LEMMA,
                params, 0, -1, Object[].class
        ).map(o
                -> new Variant((String) o[0], (String) o[2], (String) o[1])
        ).collect(Collectors.toList());
    }

    @Override
    public String getHyphenation(String form, boolean sensitive) {
        List<Param> params = Param.one("form", getValue(form, sensitive));
        params.addAll(Param.one("checked", Boolean.TRUE));
        return stream(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.HYPHBY_FORM_SENSITIVE
                : org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.HYPHBYFORM,
                params, 0, 1, String.class
        ).map(Optional::ofNullable).findFirst().flatMap(Function.identity())
                .orElse(null);
    }

    @Override
    public String getIpa(String form, boolean sensitive) {
        List<Param> params = Param.one("form", getValue(form, sensitive));
        params.addAll(Param.one("checked", Boolean.TRUE));
        return stream(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.PRONBY_FORM_SENSITIVE
                : org.fa.foarkarswurdlist.jpa.jsonb.Paradigm.PRONBYFORM,
                params, 0, 1, String.class
        ).map(Optional::ofNullable).findFirst().flatMap(Function.identity())
                .orElse(null);
    }

    @Override
    public String getArticle(String form, boolean sensitive) {
        List<Param> params = Param.one("lemma", getValue(form, sensitive));
        params.addAll(Param.one("checked", Boolean.TRUE));
        return stream(sensitive
                ? org.fa.foarkarswurdlist.jpa.jsonb.Lemma.ARTICLE_SENSITIVE
                : org.fa.foarkarswurdlist.jpa.jsonb.Lemma.ARTICLE,
                params, 0, 1, Object[].class
        ).map(o -> (String) o[1]).map(Optional::ofNullable).findFirst().flatMap(Function.identity()).orElse(null);
    }

    @Override
    public List<org.fa.foarkarswurdlist.jpa.jsonb.Lemma> lemmasForPart(String lemmaPart, Pc.Pos pos, boolean sensitive, Boolean onlyBase) {
        String query = onlyBase==null ?
                sensitive?LemmaPart.BYPARTSENSITIVE:LemmaPart.BYPART :
                sensitive?LemmaPart.BYBASESENSITIVE:LemmaPart.BYBASE;
        Param.Builder builder = new Param.Builder(true, Param.Builder.DEFAULT_MAPPING, false)
                .add("part",lemmaPart)
                .add("pos", pos==null?"":pos.name())
                .add("checked", Boolean.TRUE);
        if (onlyBase!=null) builder.add("base", onlyBase);
        return find(query,
                builder.build()
                , 0, -1, org.fa.foarkarswurdlist.jpa.jsonb.Lemma.class);
    }
    
    @Inject
    @Property(defaultValue = "1500")
    private int maxFormsByIpa;

    @Override
    public IpaResults findFormsByIpa(int offset, int max, String... ipas) {
        if (ipas==null||ipas.length==0) return null;
        boolean vowel;
        switch(ipas.length) {
            case 1 -> vowel = !IPCONSONANTS.contains(ipas[0].substring(0,1));
            case 2, 3, 4 -> {
                vowel = !IPCONSONANTS.contains(ipas[0].substring(0,1));
                for (short s = 1; s < ipas.length; s++) {
                    if (vowel == IPCONSONANTS.contains(ipas[s].substring(0, 1))) {
                        throw new IllegalArgumentException("provided ipa must all start with either vowel or consonant");
                    }                    
                }
            }
            default -> throw new IllegalArgumentException("max 4 ipa arguments supported, found " + ipas.length);
        }
        String qName = vowel ? Paradigm.IPAVOWEL : Paradigm.IPA;
        if (ipas.length > 1) qName += ipas.length;
        
        String qCountName = vowel ? Paradigm.IPAVOWELCOUNT : Paradigm.IPACOUNT;
        if (ipas.length > 1) qCountName += ipas.length;
        
        Param.Builder builder = new Param.Builder().add("1", "%"+ipas[0]);
        if (vowel) {
            builder.add("2", "%["+IPCONSONANTS+"]"+ipas[0]);
            builder.add("3", ipas[0]);
        }
        
        for (short s = 1; s < ipas.length; s++) {
            if (vowel) {
                builder
                        .add(""+(s*3+1), "%"+ipas[s])
                        .add(""+(s*3+2), "%["+IPCONSONANTS+"]"+ipas[s])
                        .add(""+(s*3+3), ipas[s]);
            } else {
                builder.add(""+(s+1), "%"+ipas[s]);
            }
        }
        

        Long total = findNative(qCountName, builder.build(),
                0, 1, Long.class).get(0);
        
        List<FormIpa> l = streamNative(qName, builder.build(),
                Math.max(0,offset), Math.min(maxFormsByIpa,max), Object[].class)
                .map(o -> {
                    String p = (String) ((Object[]) o)[5];
                    Pc.Pos pos = p.startsWith("abbr") ? Pc.Pos.x :
                            Pc.Pos.valueOf(p.substring(4).toLowerCase());
                    return new FormIpa((String) ((Object[]) o)[0], (String) ((Object[]) o)[1], (String) ((Object[]) o)[2], (String) ((Object[]) o)[3], ((Object[]) o)[4]!=null, pos);
                }).toList();
        return new IpaResults(Math.max(0,offset), Math.min(maxFormsByIpa,max), total.intValue(),l);

    }
}
