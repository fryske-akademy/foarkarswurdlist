/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.foarkarswurdlist.ejb;

import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceException;
import jakarta.transaction.Transactional;
import org.fa.foarkarswurdlist.jpa.FACodeToUniversal;
import org.fa.foarkarswurdlist.jpa.Lemma;
import org.fa.foarkarswurdlist.jpa.NotFound;
import org.fa.foarkarswurdlist.paradigm.GenerateParadigm;
import org.fa.tei.jaxb.facustomization.Pc;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.AbstractCrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author eduard
 */
@Local(NotFoundService.class)
@Stateless
public class NotFoundServiceImpl extends AbstractCrudService implements NotFoundService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotFoundServiceImpl.class.getName());

    @PersistenceContext(unitName = "standertwurdlist_unit")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    private static final Pattern noWildcards = Pattern.compile("[^*%?_]{2,}");

    @Inject
    private GenerateParadigm generateParadigm;

    @Transactional
    public void logNotFound(String word, String pos) {
        if (noWildcards.matcher(word).matches()) {
            try {
                if (find("Lemma.byFoarm",
                        Param.one("foarm",word),
                         0, 1, Lemma.class).stream().findFirst().isPresent()) {
                    return;
                }
                if (!Util.nullOrEmpty(pos)&&!"x".equalsIgnoreCase(pos)) {
                    Lemma l = new Lemma();
                    l.setFoarm(word);
                    l.setSoart(FACodeToUniversal.faPos(pos));
                    create(l);
                    List<GenerateParadigm.Form> forms;
                    switch (Pc.Pos.valueOf(pos)) {
                        case verb:
                        case aux:
                            forms = generateParadigm.generateVerb(word);
                            batchSave(generateParadigm.generateParadigma(l, forms), null);
                            break;
                        case noun:
                            forms = generateParadigm.generateNoun(word);
                            batchSave(generateParadigm.generateParadigma(l, forms), null);
                            break;
                        case adj:
                            forms = generateParadigm.generateAdjective(word);
                            batchSave(generateParadigm.generateParadigma(l, forms), null);
                            break;
                    }
                }
                NotFound notFound = new NotFound();
                notFound.setFoarm(word);
                create(notFound);
            } catch (PersistenceException e) {
                LOGGER.warn(String.format("problem storing %s, %s",word,pos), Util.deepestCause(e));
            }
        }
    }

}
