# foarkarswurdlist application

The foarkarswurdlist contains frisian lemmas and their paradigm, dutchisms and their paradigm together with linguistic information for lemmas and word forms. It is a EE 9 / jpa 3, jsf 2.3 application that uses primefaces.

Based on [CrudApi](https://bitbucket.org/fryske-akademy/crudapi)

Follows [this architecture](https://bitbucket.org/fryske-akademy/java-architecture)